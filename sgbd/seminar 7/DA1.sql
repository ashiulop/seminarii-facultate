--blocuri alocate in BD: proceduri, functii, pachete, triggeri

--PROCEDURI
/*
CREATE OR REPLACE PROCEDURE numeproc (parametru TIP_PARAMETRU TIP_DATA) AS
--sectiune declarativa
    BEGIN
    ...
    
    EXCEPTION
    END;
    /


--TIPURI PARAMETRI:
--  - IN
--  - OUT
--  - IN OUT

--APEL PROCEDURA:
EXECUTE numeproc(parametri);
--sau
BEGIN
    ...
    numeproc(parametri);
    ...
END;
/
*/



--sa se creeze o procedura prin care sa se mareasca cu o valoare salariul angajatilor care si-au schimbat functia de x ori
CREATE OR REPLACE PROCEDURE marire_salariu(p_procent IN NUMBER, p_nr NUMBER) AS
    fara_schimbari EXCEPTION;
    BEGIN
        UPDATE angajati a
            SET salariul = salariul + salariul * p_procent/100
            WHERE (SELECT COUNT(id_functie) FROM istoric_functii f  WHERE f.id_angajat = a.id_angajat) = p_nr;
            
        IF SQL%NOTFOUND THEN 
            RAISE fara_schimbari;
        ELSE
            DBMS_OUTPUT.PUT_LINE('S-au produs ' || SQL%ROWCOUNT || ' modificari.');
        END IF;
    EXCEPTION
        WHEN fara_schimbari THEN
            DBMS_OUTPUT.PUT_LINE('Nu s-au efectuat modificari!');
    END;
    /

SET SERVEROUTPUT ON;
EXECUTE marire_salariu(20, 3);

BEGIN
    marire_salariu(20, 3);
END;
/
ROLLBACK;    

/*
--FUNCTII:
 CREATE OR REPLACE FUNCTION nume_functie (parametri TIP_PARAM TIP_DATA) RETURN TIP_DATA AS
    --sectiune declarativa
    BEGIN
        ...
        RETURN valoare_returnata;
        ...
    EXCEPTION
        WHEN exceptie THEN 
            RETURN 0;
    END;
    /
    
--APEL:
DECLARE
    variabila TIP_DATA;
BEGIN 
    variabila := nume_functie(parametri);
END;
/
*/



--sa se creeze o functie care sa returneze varsta clientului pentru care id-ul e dat ca parametru. Tratare caz in care nu exista

CREATE OR REPLACE FUNCTION varsta_client(p_id clienti.id_client%TYPE) RETURN NUMBER AS
    v_varsta NUMBER;
    BEGIN
        SELECT ROUND((SYSDATE - data_nastere)/365) INTO v_varsta
            FROM clienti 
            WHERE id_client = p_id;
        RETURN v_varsta;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;
    /
    

DECLARE
    v_varsta NUMBER;
BEGIN
    v_varsta := varsta_client(20);
    IF v_varsta = -1 THEN
        DBMS_OUTPUT.PUT_LINE('Nu exista clientul!');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Clientul are ' || v_varsta || ' de ani.');
    END IF;
END;
/

--sa se creeze o procedura in interiorul careia sa declaram un cursor pentru clienti, sa se afiseze numele, prenumele si varsta 

CREATE OR REPLACE PROCEDURE afisare_clienti AS
    CURSOR c IS SELECT nume_client, prenume_client, id_client FROM clienti;
    v_varsta NUMBER;
    BEGIN 
        FOR rec_clienti IN c LOOP
            v_varsta := varsta_client(rec_clienti.id_client);
            DBMS_OUTPUT.PUT_LINE(rec_clienti.id_client || ' ' || rec_clienti.nume_client || ' ' || rec_clienti.prenume_client || ' ' || v_varsta);
        END LOOP;
    END;
    /

EXECUTE afisare_clienti;