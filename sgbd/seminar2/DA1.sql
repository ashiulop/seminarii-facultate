SET SERVEROUTPUT ON


--1
VARIABLE g_venit NUMBER
ACCEPT s PROMPT 'ID Salariat:'

DECLARE 
    p1_id NUMBER := &s;
BEGIN
    SELECT salariul + salariul * NVL(comision, 0) INTO :g_venit
        FROM angajati
        WHERE id_angajat - p1_id;
    DBMS_OUTPUT.PUT_LINE('Angajatul ' || p1_id \\ ' are venitul ' || :g_venit);
    
END
/
PRINT g_venit

--2

VARIABLE g_sal NUMBER
ACCEPT d PROMPT 'Departament:'

DECLARE 
    v_dep NUMBER := &d;
BEGIN
    SELECT AVG(salariul) INTO :g_sal
        FROM angajati
        WHERE id_departament = v_dep;
    DBMS_OUTPUT.PUT_LINE('Salariul angajatilor din dep ' || v_dep || ' este ' || ROUND(:g_sal));
    
END;
/

--3
SELECT *
    FROM angajati
    WHERE salariul < :g_sal;
    
    
--4

DECLARE
    v_cas NUMBER;
    v_cass NUMBER;
    v_impozit NUMBER;
    v_sal NUMBER;
    v_sal_net NUMBER;
BEGIN
    SELECT salariul, salariul * 0.25, salariul * 0.1, (salariul - salariul*0.25 - salariul *0.1)*0.1 INTO v_sal, v_cas, v_cass, v_impozit
    FROM angajati
    WHERE id_angajat = 100;
    
    v_sal_net := v_sal - v_cas - V_cass - v_impozit;
    
    DBMS_OUTPUT.PUT_LINE(v_sal_net);
END;

--5

ACCEPT den PROMPT 'Denumire produs:'
ACCEPT descriere PROMPT 'Descriere:'

DECLARE
    v_id_produs NUMBER;
BEGIN
    SELECT MAX(id_produs)+1 INTO v_id_produs
    FROM produse;
    
    INSERT INTO produse(id_produs, denumire_produs, descriere) VALUES(v_id_produs, '&den', '&descriere');
END;
/

SELECT * 
    FROM produse
    WHERE id_produs = (SELECT MAX(id_produs) FROM produse);