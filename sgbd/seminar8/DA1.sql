--1
CREATE OR REPLACE PROCEDURE new_job(p_id functii.id_functie%TYPE, p_den functii.denumire_functie%TYPE, p_sal_min functii.salariu_min%TYPE) AS
    BEGIN
        INSERT INTO functii VALUES(
            p_id, p_den, p_sal_min, 2*p_sal_min);
    END;
    /

DECLARE
    este_deja EXCEPTION;
BEGIN
    new_job('SY_ANAL', 'System Analyst', 6000);
    IF SQL%NOTFOUND THEN
        RAISE este_deja;
    ELSE
        DBMS_OUTPUT.PUT_LINE('Modificari efectuate cu succes!');
    END IF;
    
    EXCEPTION
        WHEN este_deja THEN
            DBMS_OUTPUT.PUT_LINE('Functia exista deja!');
END;
/


--2

CREATE OR REPLACE PROCEDURE add_job_hist(p_id istoric_functii.id_angajat%TYPE, p_id_func istoric_functii.id_functie%TYPE) AS
    v_data_angajare angajati.data_angajare%TYPE;
    v_id_dep angajati.id_departament%TYPE;
    BEGIN
        SELECT data_angajare, id_departament INTO v_data_angajare, v_id_dep
            FROM angajati 
            WHERE id_angajat = p_id;
        INSERT INTO istoric_functii VALUES(p_id, v_data_angajare, SYSDATE, p_id_func, v_id_dep);
        UPDATE angajati
            SET data_angajare = SYSDATE, id_functie = p_id_func, salariul = (SELECT salariu_min FROM functii WHERE id_functie = p_id_func) + 500
            WHERE id_angajat = p_id;
        
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('Angajatul nu exista!');
    END;
    
EXECUTE add_job_hist(106, 'SY_ANAL');

SELECT * FROM angajati a JOIN istoric_functii if ON a.id_angajat = if.id_angajat WHERE a.id_angajat = 106;


--3
CREATE OR REPLACE PROCEDURE upd_jobsal(p_id functii.id_functie%TYPE, p_sal_min functii.salariu_min%TYPE, p_sal_max functii.salariu_max%TYPE) AS
    v_id functii.id_functie%TYPE;
    salarii_invalide EXCEPTION;
    BEGIN
        SELECT id_functie INTO v_id
            FROM functii 
            WHERE id_functie = p_id;
            
        IF p_sal_min > p_sal_max THEN
            RAISE salarii_invalide;
        END IF;
        
        UPDATE functii
            SET salariu_min = p_sal_min, salariu_max = p_sal_max
            WHERE id_functie = p_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('Functia nu exista!');
        WHEN salarii_invalide THEN
            DBMS_OUTPUT.PUT_LINE('Salariul minim este mai mare decat salariul maxim!');
    END;
    /
    
EXECUTE upd_jobsal('SY_ANAL', 7000, 140);

EXECUTE upd_jobsal('SY_ANAL', 7000, 14000);

SELECT * FROM functii WHERE id_functie = 'SY_ANAL';

--4
