SET SERVEROUTPUT ON;

--1
CREATE TABLE mesaje(
    id NUMBER,
    nume VARCHAR2(30)
    );
    
--2
DECLARE 
    CURSOR c IS SELECT * FROM angajati ORDER BY salariul DESC;
    rec_ang c%ROWTYPE;
BEGIN
    OPEN c;
    LOOP
        FETCH c INTO rec_ang;
        EXIT WHEN c%ROWCOUNT > 5;
        INSERT INTO mesaje VALUES(rec_ang.id_angajat, rec_ang.nume);
        DBMS_OUTPUT.PUT_LINE(rec_ang.nume || ' ' || rec_ang.salariul);
    END LOOP;
END;
/

--sau
DECLARE 
    CURSOR c IS SELECT * FROM angajati ORDER BY salariul DESC;
BEGIN
    FOR rec_ang in c LOOP
        EXIT WHEN c%ROWCOUNT > 5;
        INSERT INTO mesaje VALUES(rec_ang.id_angajat, rec_ang.nume);
        DBMS_OUTPUT.PUT_LINE(rec_ang.nume || ' ' || rec_ang.salariul);
    END LOOP;
END;
/

--sau
DECLARE 
    CURSOR c IS SELECT *
        FROM ( SELECT id_angajat, nume, salariul
        FROM angajati
        ORDER BY salariul DESC)
        WHERE ROWNUM <= 5;
BEGIN
    FOR rec_ang in c LOOP
        INSERT INTO mesaje VALUES(rec_ang.id_angajat, rec_ang.nume);
        DBMS_OUTPUT.PUT_LINE(rec_ang.nume || ' ' || rec_ang.salariul);
    END LOOP;
END;
/


--3
DECLARE 
    CURSOR c IS (SELECT id_comanda, numarProduse
    FROM (SELECT id_comanda, COUNT(id_produs) AS numarProduse FROM rand_comenzi
    GROUP BY id_comanda
    ORDER BY count(id_produs) DESC)
    WHERE ROWNUM <= 3);
BEGIN
    FOR rec_com in c LOOP
        DBMS_OUTPUT.PUT_LINE(rec_com.id_comanda || ' ' || rec_com.numarProduse);
    END LOOP;
END;
/

--4

DECLARE
    CURSOR c1 IS SELECT id_departament, denumire_departament FROM departamente WHERE id_departament IN (SELECT id_departament FROM angajati);
    CURSOR c2 IS SELECT id_angajat, nume, salariul, id_departament FROM angajati;
BEGIN
    FOR rec_dep IN c1 LOOP
        DBMS_OUTPUT.PUT_LINE('-' || rec_dep.id_departament || ' ' || rec_dep.denumire_departament);
        FOR rec_ang IN c2 LOOP
            IF rec_ang.id_departament = rec_dep.id_departament THEN
                DBMS_OUTPUT.PUT_LINE('----' || rec_ang.id_angajat || ' ' || rec_ang.nume || ' ' || rec_ang.salariul);
            END IF;
        END LOOP;
    END LOOP;
END;
/

--sau
DECLARE
    CURSOR c1 IS SELECT id_departament, denumire_departament FROM departamente WHERE id_departament IN (SELECT id_departament FROM angajati);
    CURSOR c2(x angajati.id_departament%TYPE) IS SELECT id_angajat, nume, salariul, id_departament FROM angajati WHERE id_departament = x;
BEGIN
    FOR rec_dep IN c1 LOOP
        DBMS_OUTPUT.PUT_LINE('-' || rec_dep.id_departament || ' ' || rec_dep.denumire_departament);
        FOR rec_ang IN c2 (rec_dep.id_departament) LOOP
            DBMS_OUTPUT.PUT_LINE('----' || rec_ang.id_angajat || ' ' || rec_ang.nume || ' ' || rec_ang.salariul);
        END LOOP;
    END LOOP;
END;
/
SELECT * FROM mesaje;

-- 5. Sa se creeze un bloc PL/SQL prin care sa se afiseze salariile totale pnetru fiecare departament identificat prin denumire_departament

DECLARE 
    CURSOR c IS SELECT denumire_departament, SUM(salariul) sal_total 
                    FROM departamente d JOIN angajati a 
                    ON a.id_departament = d.id_departament 
                    GROUP BY denumire_departament;
BEGIN
    FOR rec_dep IN c LOOP
        DBMS_OUTPUT.PUT_LINE(rec_dep.denumire_departament || ' ' || rec_dep.sal_total);
    END LOOP;
END;
/

--6. Sa se completeze blocul de la ex. 4 a.i. sa apara si salariul toatal / dep
    
DECLARE
    CURSOR c1 IS SELECT id_departament, denumire_departament FROM departamente WHERE id_departament IN (SELECT id_departament FROM angajati);
    CURSOR c2(x angajati.id_departament%TYPE) IS SELECT id_angajat, nume, salariul, id_departament
                                                    FROM angajati 
                                                    WHERE id_departament = x;
    sal_total NUMBER;
BEGIN
    FOR rec_dep IN c1 LOOP
        sal_total := 0;
        DBMS_OUTPUT.PUT_LINE(' - ' || rec_dep.id_departament || ' ' || rec_dep.denumire_departament);
        FOR rec_ang IN c2 (rec_dep.id_departament) LOOP
            DBMS_OUTPUT.PUT_LINE(' ---- ' || rec_ang.id_angajat || ' ' || rec_ang.nume || ' ' || rec_ang.salariul);
            sal_total := sal_total + rec_ang.salariul;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(' ---- SALARIUL TOTAL: ' || sal_total);
    END LOOP;
END;


