#include <stdio.h>
#include <iostream>
using namespace std;

struct student
{
	int cod;
	char *nume;
	float medie;
};

struct nodarb
{
	int BF;
	student inf;
	nodarb *left, *right;
};

struct nodListaSimpla {
	student inf;
	nodListaSimpla *next;
};

struct nodListaDubla {
	student inf;
	nodListaDubla *next, *prev;
};

nodarb *creare(student s, nodarb *st, nodarb *dr)
{
	nodarb *nou = (nodarb*)malloc(sizeof(nodarb));
	nou->inf.cod = s.cod;
	nou->inf.nume = (char*)malloc((strlen(s.nume) + 1) * sizeof(char));
	strcpy(nou->inf.nume, s.nume);
	nou->inf.medie = s.medie;
	nou->left = st;
	nou->right = dr;
	return nou;
}

nodarb *inserare(student s, nodarb *rad)
{
	nodarb *aux = rad;
	if (rad == NULL)
	{
		aux = creare(s, NULL, NULL);
		return aux;
	}
	else
		while (true)
		{
			if (s.cod < aux->inf.cod)
				if (aux->left != NULL)
					aux = aux->left;
				else
				{
					aux->left = creare(s, NULL, NULL);
					return rad;
				}
			else
				if (s.cod > aux->inf.cod)
					if (aux->right != NULL)
						aux = aux->right;
					else
					{
						aux->right = creare(s, NULL, NULL);
						return rad;
					}
				else
					return rad;
		}
}

void preordine(nodarb *rad)
{
	if (rad != NULL)
	{
		printf("\nCod=%d, Nume=%s, Medie=%5.2f, BF = %d", rad->inf.cod, rad->inf.nume, rad->inf.medie, rad->BF);
		preordine(rad->left);
		preordine(rad->right);
	}
}

void inordine(nodarb *rad)
{
	if (rad != NULL)
	{
		inordine(rad->left);
		printf("\nCod=%d, Nume=%s, Medie=%5.2f, BF = %d", rad->inf.cod, rad->inf.nume, rad->inf.medie, rad->BF);
		inordine(rad->right);
	}
}

void postordine(nodarb *rad)
{
	if (rad != NULL)
	{
		postordine(rad->left);
		postordine(rad->right);
		printf("\nCod=%d, Nume=%s, Medie=%5.2f, BF = %d", rad->inf.cod, rad->inf.nume, rad->inf.medie, rad->BF);
	}
}

void dezalocare(nodarb *rad)
{
	if (rad != NULL)
	{
		nodarb *st = rad->left;
		nodarb *dr = rad->right;
		free(rad->inf.nume);
		free(rad);
		dezalocare(st);
		dezalocare(dr);
	}
}

void nrStudentiPesteOpt(nodarb *rad, int *nr) {
	if (rad != NULL) {
		nrStudentiPesteOpt(rad->left, nr);
		if (rad->inf.medie > 8)
			(*nr)++;
		nrStudentiPesteOpt(rad->right, nr);
	}
}

nodarb *cautare(nodarb *rad, int cheie)
{
	if (rad != NULL)
	{
		if (cheie == rad->inf.cod)
			return rad;
		else
			if (cheie < rad->inf.cod)
				return cautare(rad->left, cheie);
			else
				return cautare(rad->right, cheie);
	}
	else
		return NULL;
}

int maxim(int a, int b)
{
	int max = a;
	if (max < b)
		max = b;
	return max;
}

int nrNiveluri(nodarb *rad)
{
	if (rad != NULL)
		return 1 + maxim(nrNiveluri(rad->left), nrNiveluri(rad->right));
	else
		return 0;
}

void vectorG(nodarb *rad, student *vector, int *nr) {
	if (rad != NULL) {
		if (rad->inf.nume[0] == 'M') {
			vector[*nr].cod = rad->inf.cod;
			vector[*nr].nume = (char*)malloc((strlen(rad->inf.nume) + 1) * sizeof(char));
			strcpy(vector[*nr].nume, rad->inf.nume);
			vector[*nr].medie = rad->inf.medie;
			(*nr)++;
		}
		vectorG(rad->left, vector, nr);
		vectorG(rad->right, vector, nr);
	}
}

void conversieArboreVector(nodarb *rad, student *vect, int *nr)
{
	if (rad != NULL)
	{
		/*vect[*nr] = rad->inf;
		(*nr)++;
		conversieArboreVector(rad->left, vect, nr);
		conversieArboreVector(rad->right, vect, nr);*/

		vect[*nr].cod = rad->inf.cod;
		vect[*nr].nume = (char*)malloc((strlen(rad->inf.nume) + 1) * sizeof(char));
		strcpy(vect[*nr].nume, rad->inf.nume);
		vect[*nr].medie = rad->inf.medie;
		(*nr)++;

		nodarb *st = rad->left;
		nodarb *dr = rad->right;
		free(rad->inf.nume);
		free(rad);
		conversieArboreVector(st, vect, nr);
		conversieArboreVector(dr, vect, nr);
	}
}

nodarb *stergeRad(nodarb * rad) {
	nodarb *aux = rad;
	if (aux->left != NULL) {
		rad = aux->left;
		if (aux->right != NULL) {
			nodarb *temp = aux->left;
			while (temp->right)
			{
				temp = temp->right;
			}
			temp->right = aux->right;
		}
	}
	else
	{
		if (aux->right != NULL) {
			rad = aux->right;
		}
		else
		{
			rad = NULL;
		}
	}
	free(aux->inf.nume);
	free(aux);
	return rad;
}

nodarb *stergeNod(nodarb *rad, int cheie) {
	if (rad == NULL)
		return NULL;
	else {
		if (rad->inf.cod == cheie) {
			rad = stergeRad(rad);
			return rad;
		}
		else
		{
			nodarb *aux = rad;
			while (true)
			{
				if (cheie < aux->inf.cod) {
					if (aux->left == NULL)
						break;
					else
						if (aux->left->inf.cod == cheie)
							aux->left = stergeRad(aux->left);
						else
							aux = aux->left;
				}
				else {
					if (cheie > aux->inf.cod) {
						if (aux->right == NULL)
							break;
						else
							if (aux->right->inf.cod == cheie)
								aux->right = stergeRad(aux->right);
							else
								aux = aux->right;
					}
				}
			}
			return rad;
		}
	}
}

void calculBF(nodarb *rad) {
	if (rad != NULL) {
		rad->BF = nrNiveluri(rad->right) - nrNiveluri(rad->left);
		calculBF(rad->left);
		calculBF(rad->right);
	}
}


void conversieArboreListaSimpla(nodarb *rad, nodListaSimpla **cap) {
	if (rad != NULL) {
		nodListaSimpla *nou = (nodListaSimpla*)malloc(sizeof(nodListaSimpla));
		nou->inf.cod = rad->inf.cod;
		nou->inf.nume = (char*)malloc((strlen(rad->inf.nume) + 1) * sizeof(char));
		strcpy(nou->inf.nume, rad->inf.nume);
		nou->inf.medie = rad->inf.medie;
		nou->next = NULL;

		if (*cap == NULL)
			*cap = nou;
		else {
			nodListaSimpla *temp = *cap;
			while (temp->next)
				temp = temp->next;
			temp->next = nou;
		}
		nodarb *st = rad->left;
		nodarb *dr = rad->right;
		free(rad->inf.nume);
		free(rad);
		conversieArboreListaSimpla(st, cap);
		conversieArboreListaSimpla(dr, cap);

	}
}


void traversareListaSimpla(nodListaSimpla *cap) {
	nodListaSimpla *temp = cap;
	while (temp) {
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", temp->inf.cod, temp->inf.nume, temp->inf.medie);
		temp = temp->next;
	}
}

void dezalocareListaSimpla(nodListaSimpla *cap) {
	nodListaSimpla *temp = cap;
	while (temp) {
		nodListaSimpla *temp2 = temp->next;
		free(temp->inf.nume);
		free(temp);
		temp = temp2;
	}
}

void conversieArboreListaDubla(nodarb *rad, nodListaDubla **cap, nodListaDubla **coada) {
	if (rad != NULL) {
		nodListaDubla *nou = (nodListaDubla*)malloc(sizeof(nodListaDubla));
		nou->inf.cod = rad->inf.cod;
		nou->inf.nume = (char*)malloc((strlen(rad->inf.nume) + 1) * sizeof(char));
		strcpy(nou->inf.nume, rad->inf.nume);
		nou->inf.medie = rad->inf.medie;
		nou->next = NULL;
		nou->prev = NULL;

		if (*cap == NULL) {
			*cap = nou;
			*coada = nou;
		}
		else {
			nodListaDubla *temp = *cap;
			while (temp->next)
				temp = temp->next;
			temp->next = nou;
			nou->prev = temp;
			*coada = nou;
		}
		nodarb *st = rad->left;
		nodarb *dr = rad->right;
		free(rad->inf.nume);
		free(rad);
		conversieArboreListaDubla(st, cap, coada);
		conversieArboreListaDubla(dr, cap, coada);

	}
}

void traversareListaDubla(nodListaDubla *cap) {
	nodListaDubla *temp = cap;
	while (temp) {
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", temp->inf.cod, temp->inf.nume, temp->inf.medie);
		temp = temp->next;
	}
}

void traversareInversListaDubla(nodListaDubla *coada) {
	nodListaDubla *temp = coada;
	while (temp) {
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", temp->inf.cod, temp->inf.nume, temp->inf.medie);
		temp = temp->prev;
	}
}

void dezalocareListaDubla(nodListaDubla *cap) {
	nodListaDubla *temp = cap;
	while (temp) {
		nodListaDubla *temp2 = temp->next;
		free(temp->inf.nume);
		free(temp);
		temp = temp2;
	}
}

void main()
{
	int n;

	FILE *f = fopen("fisier.txt", "r");

	//printf("Nr. studenti: ");
	fscanf(f, "%d", &n);

	nodarb *rad = NULL;
	student s;
	char buffer[20];

	for (int i = 0; i < n; i++)
	{
		//printf("\nCod: ");
		fscanf(f, "%d", &s.cod);
		//printf("\nNume: ");
		fscanf(f, "%s", buffer);
		s.nume = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(s.nume, buffer);
		//printf("\nMedie: ");
		fscanf(f, "%f", &s.medie);

		rad = inserare(s, rad);
	}
	fclose(f);
	calculBF(rad);

	preordine(rad);
	printf("\n--------------------\n");
	inordine(rad);
	printf("\n--------------------\n");
	postordine(rad);

	int a = 0;
	nrStudentiPesteOpt(rad, &a);

	printf("\nexista %d studenti cu media peste 8", a);

	nodarb* nodCautat = cautare(rad, 6);
	if (nodCautat != NULL)
		printf("\nStudentul cautat se numeste %s", nodCautat->inf.nume);
	else
		printf("\nNu exista!");

	printf("\nInaltime este %d: ", nrNiveluri(rad));
	printf("\nInaltime subarbore stang este %d: ", nrNiveluri(rad->left));
	printf("\nInaltime subarbore drept este %d: ", nrNiveluri(rad->right));


	/*printf("\n----Vector de studenti cu litera G--------------------\n");

	student* vector = (student*)malloc(n * sizeof(student));
	int x = 0;
	vectorG(rad, vector, &x);
	for (int i = 0; i < x; i++)
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", vector[i].cod, vector[i].nume, vector[i].medie);

	for (int i = 0; i < x; i++)
		free(vector[i].nume);
	free(vector);*/

	/*printf("\n----Vector--------------------\n");

	student* vect = (student*)malloc(n * sizeof(student));
	int nr = 0;
	conversieArboreVector(rad, vect, &nr);
	for (int i = 0; i < nr; i++)
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", vect[i].cod, vect[i].nume, vect[i].medie);

	for (int i = 0; i < nr; i++)
		free(vect[i].nume);
	free(vect);*/

	//rad = stergeNod(rad, 7);
	//calculBF(rad);
	//inordine(rad);
	//printf("\n--------------------\n");
	//inordine(rad->left);
	//printf("\n--------------------\n");
	//inordine(rad->right);


	/*printf("\n----Lista Simpla de Studenti--------------------\n");
	nodListaSimpla *cap = NULL;
	conversieArboreListaSimpla(rad, &cap);
	traversareListaSimpla(cap);
	dezalocareListaSimpla(cap);*/


	printf("\n----Lista Dubla de Studenti--------------------\n");
	nodListaDubla *cap = NULL, *coada = NULL;
	conversieArboreListaDubla(rad, &cap, &coada);
	traversareListaDubla(cap);
	printf("\n--------------------\n");
	traversareInversListaDubla(coada);
	dezalocareListaDubla(cap);

	//dezalocare(rad);



}