#include <iostream>
#include <stdio.h>

using namespace std;

struct task {
	int id;
	char* denumire;
	float durata;
	char* nume_inginer;
	int complexitate;
};

struct nodarb {
	int BF;
	task inf;
	nodarb *left, *right;
};

struct nodld {
	task inf;
	nodld *next, *prev;
};

nodarb* creare(task t, nodarb *st, nodarb *dr) {
	nodarb *nou = (nodarb*)malloc(sizeof(nodarb));
	nou->inf.id = t.id;
	nou->inf.denumire = (char*)malloc((strlen(t.denumire) + 1) * sizeof(char));
	strcpy(nou->inf.denumire, t.denumire);
	nou->inf.durata = t.durata;
	nou->inf.nume_inginer = (char*)malloc((strlen(t.nume_inginer) + 1) * sizeof(char));
	strcpy(nou->inf.nume_inginer, t.nume_inginer);
	nou->inf.complexitate = t.complexitate;
	nou->left = st;
	nou->right = dr;
	return nou;
}

nodarb* inserare(task t, nodarb *rad) {
	nodarb *aux = rad;
	if (rad == NULL)
	{
		aux = creare(t, NULL, NULL);
		return aux;
	}
	else
		while (true)
		{
			if (t.id < aux->inf.id)
				if (aux->left != NULL)
					aux = aux->left;
				else
				{
					aux->left = creare(t, NULL, NULL);
					return rad;
				}
			else
				if (t.id > aux->inf.id)
					if (aux->right != NULL)
						aux = aux->right;
					else
					{
						aux->right = creare(t, NULL, NULL);
						return rad;
					}
			else
				return rad;
		}
}

void inordine(nodarb *rad)
{
	if (rad != NULL)
	{
		inordine(rad->left);
		printf("\nId = %d, Denumire = %s, Durata = %5.2f, Nume inginer = %s, Complexitate = %d, BF = %d", rad->inf.id, rad->inf.denumire, rad->inf.durata, rad->inf.nume_inginer, rad->inf.complexitate, rad->BF);
		inordine(rad->right);
	}
}

void preordine(nodarb *rad) {
	if (rad != NULL) {
		printf("\nId = %d, Denumire = %s, Durata = %5.2f, Nume inginer = %s, Complexitate = %d, BF = %d", rad->inf.id, rad->inf.denumire, rad->inf.durata, rad->inf.nume_inginer, rad->inf.complexitate, rad->BF);
		preordine(rad->left);
		preordine(rad->right);
	}
}

int maxim(int a, int b)
{
	int max = a;
	if (max < b)
		max = b;
	return max;
}

int nrNiveluri(nodarb *rad)
{
	if (rad != NULL)
		return 1 + maxim(nrNiveluri(rad->left), nrNiveluri(rad->right));
	else
		return 0;
}

void calculBF(nodarb *rad) {
	if (rad != NULL) {
		rad->BF = nrNiveluri(rad->right) - nrNiveluri(rad->left);
		calculBF(rad->left);
		calculBF(rad->right);
	}
}
nodarb* rotatie_dreapta(nodarb *rad) {
	printf("\nRotatie dreapta\n");
	nodarb *nod1 = rad->left;
	rad->left = nod1->right;
	nod1->right = rad;
	rad = nod1;
	return rad;
}


nodarb* rotatie_stanga(nodarb *rad) {
	printf("\nRotatie stanga\n");
	nodarb *nod1 = rad->right;
	rad->right = nod1->left;
	nod1->left = rad;
	rad = nod1;
	return rad;
}

nodarb* rotatie_stanga_dreapta(nodarb *rad) {
	printf("\nRotatie stanga-dreapta\n");
	nodarb *nod1 = rad->left;
	nodarb *nod2 = nod1->right;
	nod1->right = nod2->left;
	nod2->left = nod1;
	rad->left = nod2->right;
	nod2->right = rad;
	rad = nod2;
	return rad;
}

nodarb* rotatie_dreapta_stanga(nodarb *rad) {
	printf("\nRotatie dreapta-stanga\n");
	nodarb *nod1 = rad->right;
	nodarb *nod2 = nod1->left;
	nod1->left = nod2->right;
	nod2->right = nod1;
	rad->right = nod2->left;
	nod2->left = rad;
	rad = nod2;
	return rad;
}

nodarb *reechilibrare(nodarb *rad) {
	calculBF(rad);
	if (rad->BF <= -2 && rad->left->BF <= -1) {
		rad = rotatie_dreapta(rad);
		calculBF(rad);
	}
	else if (rad->BF >= 2 && rad->right->BF >= 1) {
		rad = rotatie_stanga(rad);
		calculBF(rad);
	}
	else if (rad->BF <= -2 && rad->left->BF >= 1) {
		rad = rotatie_stanga_dreapta(rad);
		calculBF(rad);
	}
	else if (rad->BF >= 2 && rad->right->BF <= -1) {
		rad = rotatie_dreapta_stanga(rad);
		calculBF(rad);
	}
	return rad;
}

void spargere(nodarb **rad1, nodarb **rad2, float durata) {
	if ((*rad1) != NULL) {
		if ((*rad1)->left != NULL && (*rad1)->left->inf.durata == durata) {
			*rad2 = (*rad1)->left;
			(*rad1)->left = NULL;
			return;
		}
		else if ((*rad1)->right != NULL && (*rad1)->right->inf.durata == durata) {
			*rad2 = (*rad1)->right;
			(*rad1)->right = NULL;
			return;
		}
		spargere(&(*rad1)->left, rad2, durata);
		spargere(&(*rad1)->right, rad2, durata);
	}
}

void inserareLD(nodld **cap, nodld **coada, task t) {
	nodld *nou = (nodld*)malloc(sizeof(nodld));
	nou->inf.id = t.id;
	nou->inf.denumire = (char*)malloc((strlen(t.denumire) + 1) * sizeof(char));
	strcpy(nou->inf.denumire, t.denumire);
	nou->inf.durata = t.durata;
	nou->inf.nume_inginer = (char*)malloc((strlen(t.nume_inginer) + 1) * sizeof(char));
	strcpy(nou->inf.nume_inginer, t.nume_inginer);
	nou->inf.complexitate = t.complexitate;
	nou->next = NULL;
	nou->prev = NULL;

	if ((*cap) == NULL && (*coada) == NULL) {
		*cap = nou;
		*coada = nou;
	}
	else {
		nodld *temp = *cap;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = nou;
		nou->prev = temp;
		*coada = nou;
	}

}

void inserareInLD(nodarb *rad, nodld **cap, nodld **coada) {
	if (rad != NULL) {
		inserareInLD(rad->left, cap, coada);
		inserareLD(cap, coada, rad->inf);
		inserareInLD(rad->right, cap, coada);
	}
}

void parcurgereLD(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			printf("\nId = %d, Denumire = %s, Durata = %5.2f, Nume inginer = %s, Complexitate = %d", temp->inf.id, temp->inf.denumire, temp->inf.durata, temp->inf.nume_inginer, temp->inf.complexitate);
			temp = temp->next;
		}
	}
}

void inserareInVector(nodld *cap, task *vect, int *nr, int complexitate) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			if (temp->inf.complexitate > complexitate) {
				vect[*nr].id = temp->inf.id;
				vect[*nr].denumire = (char*)malloc((strlen(temp->inf.denumire) + 1) * sizeof(char));
				strcpy(vect[*nr].denumire, temp->inf.denumire);
				vect[*nr].durata = temp->inf.durata;
				vect[*nr].nume_inginer = (char*)malloc((strlen(temp->inf.nume_inginer) + 1) * sizeof(char));
				strcpy(vect[*nr].nume_inginer, temp->inf.nume_inginer);
				vect[*nr].complexitate = temp->inf.complexitate;
				(*nr)++;
			}
			temp = temp->next;
		}
	}
}

void dezalocareLD(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			nodld *temp2 = temp->next;
			free(temp->inf.denumire);
			free(temp->inf.nume_inginer);
			free(temp);
			temp = temp2;
		}
	}
}

void dezalocareVector(task *vect, int nr){
	if (vect != NULL) {
		for (int i = 0; i < nr; i++) {
			free(vect[i].denumire);
			free(vect[i].nume_inginer);
		}
		free(vect);
	}
}


void dezalocareArbore(nodarb *rad) {
	if (rad != NULL) {
		dezalocareArbore(rad->left);
		dezalocareArbore(rad->right);
		free(rad->inf.denumire);
		free(rad->inf.nume_inginer);
		free(rad);
	}
}

void main() {
	int n;
	FILE *f = fopen("fisier.txt", "r");
	task t;
	nodarb *rad = NULL;
	char buffer[20];
	fscanf(f, "%d", &n);
	for (int i = 0; i < n; i++) {
		fscanf(f, "%d", &t.id);
		fscanf(f, "%s", buffer);
		t.denumire = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(t.denumire, buffer);
		fscanf(f, "%f", &t.durata);
		fscanf(f, "%s", buffer);
		t.nume_inginer = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(t.nume_inginer, buffer);
		fscanf(f, "%d", &t.complexitate);

		rad = inserare(t, rad);
		rad = reechilibrare(rad);
	}
	fclose(f);
	inordine(rad);
	printf("\n--------------------\n");
	preordine(rad);

	nodarb *rad2 = NULL;
	spargere(&rad, &rad2, 90);

	printf("\n---------------ARBORE 1--------------------\n");

	inordine(rad);
	printf("\ninaltime arbore 1: %d", nrNiveluri(rad));

	printf("\n---------------ARBORE 2--------------------\n");
	inordine(rad2);
	printf("\ninaltime arbore 2: %d", nrNiveluri(rad2));

	printf("\n---------------LISTA DUBLA--------------------\n");
	nodld *cap = NULL;
	nodld *coada = NULL;
	
	inserareInLD(rad, &cap, &coada);
	parcurgereLD(cap);

	printf("\n---------------vector--------------------\n");
	task *vect = (task*)malloc(n * sizeof(task));
	int nr = 0;
	inserareInVector(cap, vect, &nr, 1);
	for (int i = 0; i < nr; i++) {
		printf("\nId = %d, Denumire = %s, Durata = %5.2f, Nume inginer = %s, Complexitate = %d", vect[i].id, vect[i].denumire, vect[i].durata, vect[i].nume_inginer, vect[i].complexitate);
	}

	dezalocareLD(cap);
	dezalocareVector(vect, nr);
	dezalocareArbore(rad);
	dezalocareArbore(rad2);

}