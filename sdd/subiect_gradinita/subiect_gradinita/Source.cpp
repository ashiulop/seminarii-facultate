#include <iostream>
#include <stdio.h>

using namespace std;

struct gradinita {
	int cod;
	char* nume;
	int numar_copii;
	char* tip;
	int numar_educatoare;
};

struct nodls {
	gradinita inf;
	nodls *next;
};

struct hashT {
	int size;
	nodls **vect;
};


int functieHash(int cheie, hashT tabela) {
	return cheie % tabela.size;
}

int inserare(gradinita g, hashT tabela) {
	int pozitie;
	if (tabela.vect != NULL) {
		pozitie = functieHash(g.cod, tabela);
		nodls *nou = (nodls*)malloc(sizeof(nodls));

		nou->inf.cod = g.cod;
		nou->inf.nume = (char*)malloc((strlen(g.nume) + 1) * sizeof(char));
		strcpy(nou->inf.nume, g.nume);
		nou->inf.numar_copii = g.numar_copii;
		nou->inf.numar_educatoare = g.numar_educatoare;
		nou->inf.tip = (char*)malloc((strlen(g.tip) + 1) * sizeof(char));
		strcpy(nou->inf.tip, g.tip);
		nou->next = NULL;

		if (tabela.vect[pozitie] == NULL)
			tabela.vect[pozitie] = nou;
		else {
			nodls *temp = tabela.vect[pozitie];
			while (temp->next != NULL)
				temp = temp->next;
			temp->next = nou;
		}

	}
	return pozitie;
}

int modificare(hashT tabela, int cod) {
	
}

void traversare(hashT tabela) {
	if (tabela.vect != NULL) {
		for (int i = 0; i < tabela.size; i++) {
			if (tabela.vect[i] != NULL) {
				printf("Pozitie = %d\n", i);
				nodls *temp = tabela.vect[i];
				while (temp) {
					printf("Cod = %d, Nume = %s, Numar copii = %d, Tip = %s, Numar educatoare = %d\n", temp->inf.cod, temp->inf.nume, temp->inf.numar_copii, temp->inf.tip, temp->inf.numar_educatoare);
					temp = temp->next;
				}
			}
		}
	}
}

void main() {
	hashT tabela;
	tabela.size = 101;
	tabela.vect = (nodls**)malloc(tabela.size * sizeof(nodls*));
	for (int i = 0; i < tabela.size; i++)
		tabela.vect[i] = NULL;

	int n;
	FILE *f = fopen("fisier.txt", "r");
	fscanf(f, "%d", &n);
	gradinita g;
	char buffer[20];
	for (int i = 0; i < n; i++) {
		fscanf(f, "%d", &g.cod);
		fscanf(f, "%s", buffer);
		g.nume = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(g.nume, buffer);
		fscanf(f, "%d", &g.numar_copii);
		fscanf(f, "%s", buffer);
		g.tip = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(g.tip, buffer);
		fscanf(f, "%d", &g.numar_educatoare);

		inserare(g, tabela);
	}

	traversare(tabela);
}