#include <stdio.h>
#include <iostream>
using namespace std;

struct Apel {
	int prioritate;
	char* nume;
};

struct heap
{
	Apel *vect;
	int nrElem;
};

void filtrare(heap h, int index)
{
	int indexMax = index;
	int indexS = 2 * index + 1;
	int indexD = 2 * index + 2;

	if (indexS < h.nrElem && h.vect[indexS].prioritate > h.vect[indexMax].prioritate)
		indexMax = indexS;
	if (indexD < h.nrElem && h.vect[indexD].prioritate > h.vect[indexMax].prioritate)
		indexMax = indexD;

	if (index != indexMax)
	{
		Apel temp = h.vect[index];
		h.vect[index] = h.vect[indexMax];
		h.vect[indexMax] = temp;

		filtrare(h, indexMax);
	}
}

void inserare(heap *h, Apel elem)
{
	Apel *vect1 = (Apel*)malloc(((*h).nrElem + 1) * sizeof(Apel));
	for (int i = 0; i < (*h).nrElem; i++)
		vect1[i] = (*h).vect[i];

	(*h).nrElem++;
	free((*h).vect);
	(*h).vect = vect1;

	(*h).vect[(*h).nrElem - 1] = elem;

	for (int i = ((*h).nrElem - 1) / 2; i >= 0; i--)
		filtrare((*h), i);
}

void extragere(heap *h, Apel *elem)
{
	Apel *vect1 = (Apel*)malloc(((*h).nrElem - 1) * sizeof(Apel));

	Apel temp = (*h).vect[0];
	(*h).vect[0] = (*h).vect[(*h).nrElem - 1];
	(*h).vect[(*h).nrElem - 1] = temp;

	*elem = (*h).vect[(*h).nrElem - 1];

	for (int i = 0; i < (*h).nrElem - 1; i++)
		vect1[i] = (*h).vect[i];

	(*h).nrElem--;
	free((*h).vect);
	(*h).vect = vect1;

	for (int i = ((*h).nrElem - 1) / 2; i >= 0; i--)
		filtrare((*h), i);
}

void afisare(heap h)
{
	printf("\nApeluri: ");
	for (int i = 0; i < h.nrElem; i++)
		printf("\nNume: %s, Prioritate: %d ", h.vect[i].nume, h.vect[i].prioritate);
}

void main()
{
	heap h;

	printf("Nr elemente: ");
	scanf("%d", &h.nrElem);

	h.vect = (Apel*)malloc(h.nrElem * sizeof(Apel));
	for (int i = 0; i < h.nrElem; i++)
	{
		char buffer[20];
		printf("\nNume: ");
		scanf("%s", buffer);
		h.vect[i].nume = (char*)malloc(strlen(buffer) + 1 * sizeof(char));
		strcpy(h.vect[i].nume, buffer);
		printf("\nPrioritate: ");
		scanf("%d", &h.vect[i].prioritate);
	}

	for (int i = (h.nrElem - 1) / 2; i >= 0; i--)
		filtrare(h, i);

	afisare(h);

	Apel a;

	char buffer[20];
	printf("\nNume: ");
	scanf("%s", buffer);
	a.nume = (char*)malloc(strlen(buffer) + 1 * sizeof(char));
	strcpy(a.nume, buffer);
	printf("\nPrioritate: ");
	scanf("%d", &a.prioritate);
	inserare(&h, a);

	afisare(h);

	Apel elem;
	extragere(&h, &elem);
	printf("\nElement extras: %s %d", elem.nume, elem.prioritate);

	afisare(h);
}