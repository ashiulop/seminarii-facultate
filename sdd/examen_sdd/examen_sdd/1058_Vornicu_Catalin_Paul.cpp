#include <stdio.h>
#include <iostream>
using namespace std;

struct examen {
	int cod;
	char* materie;
	char data_si_ora[11];
	char* nume_supraveghetor;
	int numar_studenti;
};

struct heap {
	examen *vect;
	int nrElem;
};

struct nodld {
	examen inf;
	nodld *next, *prev;
};

void filtrare(heap h, int index)
{
	int indexMin = index;
	int indexS = 2 * index + 1;
	int indexD = 2 * index + 2;

	if (indexS < h.nrElem && strcmp(h.vect[indexMin].data_si_ora, h.vect[indexS].data_si_ora) == 1)
		indexMin = indexS;
	if (indexD < h.nrElem && strcmp(h.vect[indexMin].data_si_ora, h.vect[indexD].data_si_ora) == 1)
		indexMin = indexD;

	if (index != indexMin)
	{
		examen temp = h.vect[index];
		h.vect[index] = h.vect[indexMin];
		h.vect[indexMin] = temp;

		filtrare(h, indexMin);
	}
}

void afisare(heap h)
{
	printf("\nExamene: ");
	for (int i = 0; i < h.nrElem; i++)
		printf("\nCod: %d, Materie: %s, Data si ora: %s, Nume profesor: %s, Numar studenti: %d ", h.vect[i].cod, h.vect[i].materie, h.vect[i].data_si_ora, h.vect[i].nume_supraveghetor, h.vect[i].numar_studenti);
}

void inserare(heap *h, examen elem)
{
	examen *vect1 = (examen*)malloc(((*h).nrElem + 1) * sizeof(examen));
	for (int i = 0; i < (*h).nrElem; i++)
		vect1[i] = (*h).vect[i];

	(*h).nrElem++;
	free((*h).vect);
	(*h).vect = vect1;

	(*h).vect[(*h).nrElem - 1] = elem;

	for (int i = ((*h).nrElem - 1) / 2; i >= 0; i--)
		filtrare((*h), i);
}


void extragere(heap *h, examen *elem)
{
	examen *vect1 = (examen*)malloc(((*h).nrElem - 1) * sizeof(examen));

	examen temp = (*h).vect[0];
	(*h).vect[0] = (*h).vect[(*h).nrElem - 1];
	(*h).vect[(*h).nrElem - 1] = temp;

	*elem = (*h).vect[(*h).nrElem - 1];

	for (int i = 0; i < (*h).nrElem - 1; i++)
		vect1[i] = (*h).vect[i];

	(*h).nrElem--;
	free((*h).vect);
	(*h).vect = vect1;

	for (int i = ((*h).nrElem - 1) / 2; i >= 0; i--)
		filtrare((*h), i);
}

void inserareInLD(nodld **cap, nodld **coada, examen e){
	nodld *nou = (nodld*)malloc(sizeof(nodld));
	nou->inf.cod = e.cod;
	nou->inf.materie = (char*)malloc((strlen(e.materie) + 1) * sizeof(char));
	strcpy(nou->inf.materie, e.materie);
	strcpy(nou->inf.data_si_ora, e.data_si_ora);
	nou->inf.nume_supraveghetor = (char*)malloc((strlen(e.nume_supraveghetor) + 1) * sizeof(char));
	strcpy(nou->inf.nume_supraveghetor, e.nume_supraveghetor);
	nou->inf.numar_studenti = e.numar_studenti;
	nou->next = NULL;
	nou->prev = NULL;
	if ((*cap) == NULL && (*coada) == NULL) {
		*cap = nou;
		*coada = nou;
	}
	else {
		nodld *temp = *cap;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = nou;
		nou->prev = temp;
		*coada = nou;
	}
}

void inserareCoadaLD(nodld **cap, nodld **coada, examen e) {
	nodld *nou = (nodld*)malloc(sizeof(nodld));
	nou->inf.cod = e.cod;
	nou->inf.materie = (char*)malloc((strlen(e.materie) + 1) * sizeof(char));
	strcpy(nou->inf.materie, e.materie);
	strcpy(nou->inf.data_si_ora, e.data_si_ora);
	nou->inf.nume_supraveghetor = (char*)malloc((strlen(e.nume_supraveghetor) + 1) * sizeof(char));
	strcpy(nou->inf.nume_supraveghetor, e.nume_supraveghetor);
	nou->inf.numar_studenti = e.numar_studenti;
	nou->next = NULL;
	nou->prev = NULL;
	if ((*cap) == NULL && (*coada) == NULL) {
		*cap = nou;
		*coada = nou;
	}
	else {
		nodld *temp = *coada;
		while (temp->prev != NULL) {
			temp = temp->prev;
		}
		temp->prev = nou;
		nou->next = temp;
		*cap = nou;
	}
}

nodld* filtrareDupaStudenti(heap h, nodld *cap, nodld **coada, int nr_studenti) {
	for (int i = 0; i < h.nrElem; i++) {
		if (h.vect[i].numar_studenti > nr_studenti)
			inserareInLD(&cap, coada, h.vect[i]);
	}
	return cap;
}

void procesareExamene(heap *h, nodld **cap, nodld **coada) {
	examen temp;
	int nr = h->nrElem;
	for (int i = 0; i < nr; i++) {
		extragere(h, &temp);
		inserareCoadaLD(cap, coada, temp);
	}
}

void afisareLD(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			printf("\nCod: %d, Materie: %s, Data si ora: %s, Nume profesor: %s, Numar studenti: %d ", temp->inf.cod, temp->inf.materie, temp->inf.data_si_ora, temp->inf.nume_supraveghetor, temp->inf.numar_studenti);
			temp = temp->next;
		}
	}
}

void dezalocareHeap(heap h) {
	for (int i = 0; i < h.nrElem; i++) {
		free(h.vect[i].materie);
		free(h.vect[i].nume_supraveghetor);
	}
	free(h.vect);
	printf("\nDezalocare heap");
}

void dezalocareLD(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			nodld *temp2 = temp->next;
			free(temp->inf.materie);
			free(temp->inf.nume_supraveghetor);
			free(temp);
			temp = temp2;
		}
		printf("\nDezalocare lista dubla");
	}
}

void main() {
	printf("\n--------EXERCITIUL 1------\n");
	heap h;
	FILE *f = fopen("fisier.txt", "r");
	fscanf(f, "%d", &h.nrElem);
	h.vect = (examen*)malloc(h.nrElem * sizeof(examen));
	for (int i = 0; i < h.nrElem; i++) {
		char buffer[20];
		fscanf(f, "%d", &h.vect[i].cod);
		fscanf(f, " %[^\n]s", buffer);
		h.vect[i].materie = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(h.vect[i].materie, buffer);
		fscanf(f, " %[^\n]s", h.vect[i].data_si_ora);
		fscanf(f, " %[^\n]s", buffer);
		h.vect[i].nume_supraveghetor = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(h.vect[i].nume_supraveghetor, buffer);
		fscanf(f, "%d", &h.vect[i].numar_studenti);
	}
	fclose(f);
	for (int i = (h.nrElem - 1) / 2; i >= 0; i--)
		filtrare(h, i);
	afisare(h);
	printf("\n--------EXERCITIUL 2------\n");
	examen e;
	extragere(&h, &e);
	printf("\nElement extras: ");
	printf("\nCod: %d, Materie: %s, Data si ora: %s, Nume profesor: %s, Numar studenti: %d ", e.cod, e.materie, e.data_si_ora, e.nume_supraveghetor, e.numar_studenti);

	printf("\n-------EXERCITIUL 4-------\n");

	nodld *cap = NULL;
	nodld *coada = NULL;
	cap = filtrareDupaStudenti(h, cap, &coada, 150);
	afisareLD(cap);

	printf("\n-------EXERCITIUL 5-------\n");
	nodld *cap2 = NULL;
	nodld *coada2 = NULL;
	procesareExamene(&h, &cap2, &coada2);
	afisareLD(cap2);


	printf("\n-------EXERCITIUL 6-------\n");
	dezalocareHeap(h);
	dezalocareLD(cap);
	dezalocareLD(cap2);

}


