#include <stdio.h>
#include <iostream>

using namespace std;

struct nodLp;

struct nodLs {
	nodLp *inf;
	nodLs *next;
};

struct nodLp {
	nodLs *vecini;
	int inf;
	nodLp *next;
};

void inserareLP(nodLp **cap, int cod) {
	nodLp *nou = (nodLp*)malloc(sizeof(nodLp));
	nou->inf = cod;
	nou->vecini = NULL;
	nou->next = NULL;

	if (*cap == NULL)
		*cap = nou;
	else {
		nodLp *temp = *cap;
		while (temp->next != NULL) {
			temp = temp->next;
		}
		temp->next = nou;
	}

}

void inserareLS(nodLs **cap, nodLp *inf) {
	nodLs *nou = (nodLs*)malloc(sizeof(nodLs));
	nou->inf = inf;
	nou->next = NULL;
	if (*cap == NULL)
		*cap = nou;
	else {
		nodLs *temp = *cap;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = nou;
	}
}

void cautare(nodLp *cap, int id, nodLp **out) {
	nodLp *temp = cap;
	while (temp != NULL && temp->inf != id)
		temp = temp->next;
	*out = temp;
}

void inserareArc(nodLp *graf, int start, int stop) {
	if (graf != NULL) {
		nodLp *nodStart, *nodStop;
		cautare(graf, start, &nodStart);
		cautare(graf, stop, &nodStop);
		if (nodStart != NULL && nodStop != NULL) {
			inserareLS(&nodStart->vecini, nodStop);
			inserareLS(&nodStop->vecini, nodStart);

		}
	}
}

void traversareLs(nodLs *cap) {
	nodLs *temp = cap;
	while (temp != NULL) {
		printf("     %d\n", temp->inf->inf + 1);
		temp = temp->next;
	}
}

void traversareLp(nodLp *cap) {
	nodLp *temp = cap;
	while (temp != NULL) {
		printf("%d are urmatorii vecini: \n", temp->inf + 1);
		traversareLs(temp->vecini);
		temp = temp->next;
	}
}

struct nodStiva {
	int inf;
	nodStiva *next;
};

struct nodCoada {
	int inf;
	nodCoada *next;
};

void push(nodStiva **varf, int val) {
	nodStiva *nou = (nodStiva*)malloc(sizeof(nodStiva));
	nou->inf = val;
	nou->next = NULL;

	if (*varf == NULL)
		*varf = nou;
	else {
		nou->next = *varf;
		*varf = nou;
	}
}

void pop(nodStiva **varf, int *val) {
	if(*varf == NULL)
		return;
	else {
		*val = (*varf)->inf;
		nodStiva *aux = *varf;
		*varf = (*varf)->next;
		free(aux);
	}
}

void put(nodCoada **prim, nodCoada **ultim, int val) {
	nodCoada *nou = (nodCoada*)malloc(sizeof(nodCoada));
	nou->inf = val;
	nou->next = NULL;

	if (*prim == NULL && *ultim == NULL) {
		*prim = nou;
		*ultim = nou;
	}
	else {
		(*ultim)->next = nou;
		*ultim = nou;
	}
}

void get(nodCoada **prim, nodCoada **ultim, int *val) {
	if (*prim != NULL && *ultim != NULL) {
		*val = (*prim)->inf;
		nodCoada *aux = *prim;
		*prim = (*prim)->next;
		free(aux);
	}
	if (*prim == NULL)
		*ultim = NULL;
}

int calculareNumarNoduri(nodLp* graf){
	int contor = 0;
	nodLp *temp = graf;
	while (temp)
	{
		contor++;
		temp = temp->next;
	}
	return contor;
}

void parcurgereInAdancime(nodLp* graf, int idNodStart)
{
	if (graf)
	{
		nodStiva* stiva = NULL;
		int nrNoduri = calculareNumarNoduri(graf);
		int* vizitate = (int*)malloc(sizeof(int)*nrNoduri);
		for (int i = 0; i < nrNoduri; i++)
		{
			vizitate[i] = 0;
		}

		push(&stiva, idNodStart);
		vizitate[idNodStart] = 1;

		while (stiva)
		{
			int idNodCurent;
			pop(&stiva, &idNodCurent);

			nodLp *nodCurent;
			cautare(graf, idNodCurent, &nodCurent);
			printf("%d\n", nodCurent->inf);

			nodLs *temp = nodCurent->vecini;
			while (temp != NULL)
			{
				if (vizitate[temp->inf->inf] == 0)
				{
					push(&stiva, temp->inf->inf);
					vizitate[temp->inf->inf] = 1;
				}
				temp = temp->next;
			}
		}
	}
}


void dezalocareLs(nodLs *cap) {
	nodLs *temp = cap;
	while (temp) {
		nodLs *temp2 = temp->next;
		free(temp);
		temp = temp2;
	}
}

void dezalocareLp(nodLp* cap) {
	nodLp *temp = cap;
	while (temp) {
		nodLp *temp2 = temp->next;
		if (temp->vecini != NULL)
			dezalocareLs(temp->vecini);
		printf("\nAm sters %d", temp->inf + 1);
		free(temp);
		temp = temp2;
	}
}

void main() {
	FILE *f = fopen("Text.txt", "r");
	nodLp *graf = NULL;
	int nrNoduri;
	fscanf(f, "%d", &nrNoduri);

	for (int i = 0; i < nrNoduri; i++) {
		inserareLP(&graf, i);
	}

	int nrArce;
	fscanf(f, "%d", &nrArce);

	for (int i = 0; i < nrArce; i++) {
		int start, stop;
		fscanf(f, "%d", &start);
		fscanf(f, "%d", &stop);
		inserareArc(graf, start, stop);
	}

	traversareLp(graf);

	int nod;
	printf("Parcurgere in adancime de la nodul: ");
	scanf("%d", &nod);

	parcurgereInAdancime(graf, nod);

	dezalocareLp(graf);
	/*for (int i = 0; i < nr; i++) {
		mat[i] = (int*)malloc(nr * sizeof(int));
	}

	for (int i = 0; i < nr; i++)
		for (int j = 0; j < nr; j++)
			mat[i][j] = 0;

	int n, ii, jj, greutate;
	fscanf(f, "%d", &n);
	for (int i = 0; i < n; i++) {
		fscanf(f, "%d", &ii);
		fscanf(f, "%d", &jj);
		fscanf(f, "%d", &greutate);
		if (ii <= n && jj <= n) {
			mat[ii - 1][jj - 1] = greutate;
			mat[jj - 1][ii - 1] = greutate;
		}

	}
	fclose(f);

	printf("\n------Matrice--------\n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%d ", mat[i][j]);
		printf("\n");
	}

	int *vizitat = (int*)malloc(nr * sizeof(int));
	for (int i = 0; i < nr; i++)
		vizitat[i] = 0;

	int nod;
	printf("\nParcurgere in adancime de la nodul: ");
	scanf("%d", &nod);

	nodStiva *varf = NULL;
	push(&varf, nod);
	vizitat[nod] = 1;
	while (varf != NULL) {
		pop(&varf, &nod);
		printf("%d-", nod + 1);
		for(int k = 0; k < nr; k++)
			if (mat[nod][k] != 0 && vizitat[k] == 0) {
				push(&varf, k);
				vizitat[k] = 1;
			}
	}

	for (int i = 0; i < nr; i++)
		vizitat[i] = 0;

	printf("\nParcurgere in latime de la nodul: ");
	scanf("%d", &nod);

	nodCoada *prim = NULL, *ultim = NULL;

	put(&prim, &ultim, nod);
	vizitat[nod] = 1;
	while (prim != NULL) {
		get(&prim, &ultim, &nod);
		printf("%d-", nod + 1);
		for (int k = 0; k < nr; k++)
			if (mat[nod][k] != 0 && vizitat[k] == 0) {
				put(&prim, &ultim, k);
				vizitat[k] = 1;
			}
	}*/

	//o structura de tip lista de liste. listele secundare vor contine un nr de noduri coresp numarului de arce care pornesc din nodul respectiv
}