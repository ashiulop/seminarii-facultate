#include <iostream>
#include <stdio.h>
using namespace std;

struct carte
{
	int cod;
	char *titlu;
	float pret;
	int nrAutori;
	char **vectAutori;
};

struct nodStiva
{
	carte inf;
	nodStiva *next;
};

struct nodLista
{
	carte inf;
	nodLista *next;
};

void push(nodStiva **varf, carte c)
{
	nodStiva* nou = (nodStiva*)malloc(sizeof(nodStiva));
	nou->inf.cod = c.cod;
	nou->inf.titlu = (char*)malloc((strlen(c.titlu) + 1) * sizeof(char));
	strcpy(nou->inf.titlu, c.titlu);
	nou->inf.pret = c.pret;
	nou->inf.nrAutori = c.nrAutori;
	nou->inf.vectAutori = (char**)malloc(c.nrAutori * sizeof(char*));
	for (int i = 0; i < c.nrAutori; i++) {
		nou->inf.vectAutori[i] = (char*)malloc((strlen(c.vectAutori[i]) + 1) * sizeof(char));
		strcpy(nou->inf.vectAutori[i], c.vectAutori[i]);
	}
	nou->next = NULL;
	if (*varf == NULL)
	{
		*varf = nou;
	}
	else
	{
		nou->next = *varf;
		*varf = nou;
	}
}

int pop(nodStiva **varf, carte *val)
{
	if (*varf == NULL) return -1;
	else
	{
		(*val).cod = (*varf)->inf.cod;
		(*val).titlu = (char*)malloc((strlen((*varf)->inf.titlu) + 1) * sizeof(char));
		strcpy((*val).titlu, (*varf)->inf.titlu);
		(*val).pret = (*varf)->inf.pret;
		(*val).nrAutori = (*varf)->inf.nrAutori;
		(*val).vectAutori = (char**)malloc((*varf)->inf.nrAutori * sizeof(char*));
		for (int i = 0; i < (*val).nrAutori; i++) {
			(*val).vectAutori[i] = (char*)malloc((strlen((*varf)->inf.vectAutori[i]) + 1) * sizeof(char));
			strcpy((*val).vectAutori[i], (*varf)->inf.vectAutori[i]);
		}
		nodStiva *temp = *varf;
		*varf = (*varf)->next;
		free(temp->inf.titlu);
		for (int i = 0; i < temp->inf.nrAutori; i++) {
			free(temp->inf.vectAutori[i]);
		}
		free(temp->inf.vectAutori);
		free(temp);
		return 0;
	}
}

void traversare(nodStiva *varf)
{
	nodStiva *temp = varf;
	while (temp)
	{
		printf("\nCod = %d, Titlu = %s, Pret = %5.2f, Nr autori = %d", temp->inf.cod, temp->inf.titlu, temp->inf.pret, temp->inf.nrAutori);
		printf("\nAutori: ");
		for (int i = 0; i < temp->inf.nrAutori; i++) {
			printf("%s ", temp->inf.vectAutori[i]);
		}
		temp = temp->next;
	}
}

void traversareLista(nodLista *cap)
{
	nodLista *temp = cap;
	while (temp)
	{
		printf("\nCod = %d, Titlu = %s, Pret = %5.2f, Nr autori = %d", temp->inf.cod, temp->inf.titlu, temp->inf.pret, temp->inf.nrAutori);
		printf("\nAutori: ");
		for (int i = 0; i < temp->inf.nrAutori; i++) {
			printf("%s ", temp->inf.vectAutori[i]);
		}
		temp = temp->next;
	}
}

void conversieStivaVector(nodStiva **varf, carte *vect, int *nr)
{
	carte val;
	while (pop(varf, &val) == 0)
	{
		vect[*nr] = val;
		(*nr)++;
	}
}

void inserareLista(nodLista** cap, carte val)
{
	nodLista* nou = (nodLista*)malloc(sizeof(nodLista));
	nou->inf.cod = val.cod;
	nou->inf.titlu = (char*)malloc((strlen(val.titlu) + 1) * sizeof(char));
	strcpy(nou->inf.titlu, val.titlu);
	nou->inf.pret = val.pret;
	nou->inf.nrAutori = val.nrAutori;
	nou->inf.vectAutori = (char**)malloc(val.nrAutori * sizeof(char*));
	for (int i = 0; i < val.nrAutori; i++) {
		nou->inf.vectAutori[i] = (char*)malloc((strlen(val.vectAutori[i]) + 1) * sizeof(char));
		strcpy(nou->inf.vectAutori[i], val.vectAutori[i]);
	}
	nou->next = NULL;
	if (*cap == NULL)
		*cap = nou;
	else
	{
		nodLista* temp = *cap;
		while (temp->next) temp = temp->next;
		temp->next = nou;
	}
}

void conversieStivaListaSimpla(nodStiva** varf, nodLista**cap)
{
	carte val;
	while (pop(varf, &val) == 0)
	{
		inserareLista(cap, val);
	}
}

void main()
{
	nodStiva *varf = NULL;
	int n;
	carte c;
	printf("Nr. carti = ");
	scanf("%d", &n);
	char buffer[20];
	char buffer2[20];
	for (int i = 0; i < n; i++)
	{
		printf("\nCod = ");
		scanf("%d", &c.cod);
		printf("\nTitlu = ");
		cin.getline(buffer, 20, ';');
		c.titlu = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(c.titlu, buffer);
		printf("\nPret = ");
		scanf("%f", &c.pret);
		printf("\nNr autori = ");
		scanf("%d", &c.nrAutori);
		printf("\nAutori: ");
		c.vectAutori = (char**)malloc(c.nrAutori * sizeof(char*));
		for (int i = 0; i < c.nrAutori; i++) {
			cin.getline(buffer2, 20, ';');
			c.vectAutori[i] = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
			strcpy(c.vectAutori[i], buffer2);
		}
		push(&varf, c);
	}
	traversare(varf);

	carte val;
	pop(&varf, &val);
	printf("\nCartea extrasa are codul %d si titlul %s", val.cod, val.titlu);
	printf("\nAutori: ");
	for (int i = 0; i < val.nrAutori; i++) {
		printf("%s ", val.vectAutori[i]);
	}

	printf("\n----------------\n");
	/*carte *vect = (carte*)malloc(n*sizeof(carte));
	int nr = 0;
	conversieStivaVector(&varf, vect, &nr);
	for(int i=0;i<nr;i++){
		printf("\nCod = %d, Titlu = %s, Pret = %5.2f, Nr autori = %d", vect[i].cod, vect[i].titlu, vect[i].pret, vect[i].nrAutori);
		printf("Autori: ");
		for (int j = 0; j < vect[i].nrAutori; j++){
			printf("%s ", vect[i].vectAutori[j]);
		}
	}
	free(vect);*/

	nodLista* cap = NULL;
	conversieStivaListaSimpla(&varf, &cap);
	traversareLista(cap);
}