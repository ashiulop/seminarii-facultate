#include <iostream>
#include <stdio.h>

using namespace std;

struct angajat {
	int cod;
	char* nume;
	int varsta;
	char* cnp;
	float vechime;
};

struct nodld {
	angajat inf;
	nodld *next, *prev;
};

struct nodarb {
	angajat inf;
	nodarb *left, *right;
};

void inserare(nodld **cap, nodld **coada, angajat a) {
	nodld *nou = (nodld*)malloc(sizeof(nodld));
	nou->inf.cod = a.cod;
	nou->inf.nume = (char*)malloc((strlen(a.nume) + 1) * sizeof(char));
	strcpy(nou->inf.nume, a.nume);
	nou->inf.varsta = a.varsta;
	nou->inf.cnp = (char*)malloc((strlen(a.cnp) + 1) * sizeof(char));
	strcpy(nou->inf.cnp, a.cnp);
	nou->inf.vechime = a.vechime;
	nou->next = NULL;
	nou->prev = NULL;

	if (*cap == NULL && *coada == NULL) {
		*cap = nou;
		*coada = nou;
	}
	else {
		nodld *temp = *cap;
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = nou;
		nou->prev = temp;
		*coada = nou;
	}
}

void afisare(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			printf("Cod = %d, Nume = %s, Varsta = %d, CNP = %s, vechime = %5.2f\n", temp->inf.cod, temp->inf.nume, temp->inf.varsta, temp->inf.cnp, temp->inf.vechime);
			temp = temp->next;
		}
	}
}

void afisareInvers(nodld *coada) {
	if (coada != NULL) {
		nodld *temp = coada;
		while (temp) {
			printf("Cod = %d, Nume = %s, Varsta = %d, CNP = %s, vechime = %5.2f\n", temp->inf.cod, temp->inf.nume, temp->inf.varsta, temp->inf.cnp, temp->inf.vechime);
			temp = temp->prev;
		}
	}
}

void stergere(nodld **cap, nodld **coada, int cod) {
	if (*cap != NULL && *coada != NULL) {
		nodld *temp = *cap;
		while (temp != NULL) {
			if (temp->inf.cod == cod) {
				if (temp == *cap) {
					nodld *temp2 = temp->next;
					free(temp->inf.nume);
					free(temp->inf.cnp);
					free(temp);
					temp = temp2;
					temp->prev = NULL;
					*cap = temp;
				}
				else if (temp == *coada) {
					nodld *temp2 = temp->prev;
					free(temp->inf.nume);
					free(temp->inf.cnp);
					free(temp);
					temp = temp2;
					temp->next = NULL;
					*coada = temp;
				}
				else {
					nodld *anterior = temp->prev;
					nodld *urmator = temp->next;
					free(temp->inf.nume);
					free(temp->inf.cnp);
					free(temp);
					anterior->next = urmator;
					urmator->prev = anterior;
					temp = urmator;
				}
			}
			else {
				temp = temp->next;
			}
		}

		
	}
}

nodarb* creare(angajat a, nodarb *st, nodarb *dr) {
	nodarb *nou = (nodarb*)malloc(sizeof(nodarb));
	nou->inf.cod = a.cod;
	nou->inf.nume = (char*)malloc((strlen(a.nume) + 1) * sizeof(char));
	strcpy(nou->inf.nume, a.nume);
	nou->inf.varsta = a.varsta;
	nou->inf.cnp = (char*)malloc((strlen(a.cnp) + 1) * sizeof(char));
	strcpy(nou->inf.cnp, a.cnp);
	nou->inf.vechime = a.vechime;
	nou->left = st;
	nou->right = dr;
	return nou;
}

nodarb* inserare(angajat a, nodarb *rad) {
	nodarb *aux = rad;
	if (rad == NULL) {
		aux = creare(a, NULL, NULL);
		return aux;
	}
	else {
		while (true) {
			if (a.vechime < aux->inf.vechime)
				if (aux->left != NULL)
					aux = aux->left;
				else {
					aux->left = creare(a, NULL, NULL);
					return rad;
				}
			else
				if (a.vechime > aux->inf.vechime)
					if (aux->right != NULL)
						aux = aux->right;
					else {
						aux->right = creare(a, NULL, NULL);
						return rad;
					}
			else
				return rad;
		}
	}
}

void inserareInArbore(nodarb **rad, nodld **cap, float vechime) {
	if (*cap != NULL) {
		nodld *temp = *cap;
		while (temp) {
			if (temp->inf.vechime > vechime)
				*rad = inserare(temp->inf, *rad);
			temp = temp->next;
		}
	}
}

void inordine(nodarb *rad) {
	if (rad != NULL) {
		inordine(rad->left);
		printf("Cod = %d, Nume = %s, Varsta = %d, CNP = %s, vechime = %5.2f\n", rad->inf.cod, rad->inf.nume, rad->inf.varsta, rad->inf.cnp, rad->inf.vechime);
		inordine(rad->right);
	}
}

void salvareInVector(angajat *vect, nodarb *rad, int *nr, float vechime) {
	if (rad != NULL && rad->inf.vechime != vechime) {
		vect[*nr].cod = rad->inf.cod;
		vect[*nr].nume = (char*)malloc((strlen(rad->inf.nume) + 1) * sizeof(char));
		strcpy(vect[*nr].nume, rad->inf.nume);
		vect[*nr].varsta = rad->inf.varsta;
		vect[*nr].cnp = (char*)malloc((strlen(rad->inf.cnp) + 1) * sizeof(char));
		strcpy(vect[*nr].cnp, rad->inf.cnp);
		vect[*nr].vechime = rad->inf.vechime;
		(*nr)++;
		salvareInVector(vect, rad->left, nr, vechime);
		salvareInVector(vect, rad->right, nr, vechime);
	}
}

void dezalocareLD(nodld *cap) {
	if (cap != NULL) {
		nodld *temp = cap;
		while (temp) {
			nodld *temp2 = temp->next;
			free(temp->inf.nume);
			free(temp->inf.cnp);
			free(temp);
			temp = temp2;

		}
	}
}


void dezalocareVector(angajat *vect, int nr) {
	if (vect != NULL) {
		for (int i = 0; i < nr; i++) {
			free(vect[i].nume);
			free(vect[i].cnp);
		}
		free(vect);
	}
}

void dezalocareArbore(nodarb *rad) {
	if (rad != NULL) {
		dezalocareArbore(rad->left);
		dezalocareArbore(rad->right);
		free(rad->inf.nume);
		free(rad->inf.cnp);
		free(rad);
	}
}

void main() {
	nodld *cap = NULL;
	nodld *coada = NULL;
	angajat a;

	FILE *f = fopen("fisier.txt", "r");
	int n;

	fscanf(f, "%d", &n);
	for (int i = 0; i < n; i++) {
		fscanf(f, "%d", &a.cod);
		char buffer[20];
		fscanf(f, "%s", buffer);
		a.nume = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(a.nume, buffer);
		fscanf(f, "%d", &a.varsta);
		fscanf(f, "%s", buffer);
		a.cnp = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(a.cnp, buffer);
		fscanf(f, "%f", &a.vechime);

		inserare(&cap, &coada, a);
	}

	afisare(cap);

	/*stergere(&cap, &coada, 10);
	printf("\n-------------------\n");
	afisare(cap);
	printf("\n-------------------\n");
	afisareInvers(coada);*/

	printf("\n-------------------\n");
	nodarb *rad = NULL;
	inserareInArbore(&rad, &cap, 2);
	inordine(rad);

	printf("\n-------------------\n");
	angajat *vect = (angajat*)malloc(n * sizeof(angajat));
	int nr = 0;
	salvareInVector(vect, rad, &nr, 4);

	for (int i = 0; i < nr; i++) {
		printf("Cod = %d, Nume = %s, Varsta = %d, CNP = %s, vechime = %5.2f\n", vect[i].cod, vect[i].nume, vect[i].varsta, vect[i].cnp, vect[i].vechime);
	}

	dezalocareLD(cap);
	dezalocareVector(vect, nr);
	dezalocareArbore(rad);
}