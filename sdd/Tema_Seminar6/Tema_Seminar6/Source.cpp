#include<iostream>
#include<stdio.h>

using namespace std;

struct student{
	int cod;
	char *nume;
	float medie;
};

struct nodCoada {
	student inf;
	nodCoada *next;
};

struct nodLS{
	student inf;
	nodLS *next;

};

struct hashT{
	nodLS **vect;
	int size;
};

int functieHash(int cheie, hashT tabela) {

	return cheie % tabela.size;

}

int inserare(hashT tabela, student s)
{
	int pozitie;
	if (tabela.vect != NULL) {
		pozitie = functieHash(s.cod, tabela);
		nodLS *nou = (nodLS*)malloc(sizeof(nodLS));
		nou->inf.cod = s.cod;
		nou->inf.nume = (char*)malloc((strlen(s.nume) + 1) * sizeof(char));
		strcpy(nou->inf.nume, s.nume);
		nou->inf.medie = s.medie;
		nou->next = NULL;
		if (tabela.vect[pozitie] == NULL)
			tabela.vect[pozitie] = nou;
		else
		{
			nodLS* temp = tabela.vect[pozitie];
			while (temp->next)
				temp = temp->next;
			temp->next = nou;
		}
	}

	return pozitie;
}

void traversare(hashT tabela)
{
	if (tabela.vect != NULL) {
		for (int i = 0; i < tabela.size; i++)
			if (tabela.vect[i] != NULL)
			{
				printf("\nPozitie=%d", i);
				nodLS *temp = tabela.vect[i];
				while (temp)
				{
					printf("\nCod=%d, Nume=%s, Medie=%5.2f", temp->inf.cod, temp->inf.nume, temp->inf.medie);
					temp = temp->next;
				}
			}
	}
}

void dezalocare(hashT tabela)
{
	if (tabela.vect != NULL) {
		for (int i = 0; i < tabela.size; i++)
			if (tabela.vect[i] != NULL)
			{ 
				nodLS *temp = tabela.vect[i];
				while (temp)
				{
					nodLS* temp2 = temp->next;
					free(temp->inf.nume);
					free(temp);
					temp = temp2;
				}
			}
		free(tabela.vect);
	}
}

void put(nodCoada **prim, nodCoada **ultim, student s) {
	nodCoada *nou = (nodCoada*)malloc(sizeof(nodCoada));
	nou->inf.cod = s.cod;
	nou->inf.nume = (char*)malloc((strlen(s.nume) + 1) * sizeof(char));
	strcpy(nou->inf.nume, s.nume);
	nou->inf.medie = s.medie;
	nou->next = NULL;

	if (*prim == NULL && *ultim == NULL) {
		*prim = nou;
		*ultim = nou;
	}
	else {
		(*ultim)->next = nou;
		*ultim = nou;
	}
}

int get(nodCoada **prim, nodCoada **ultim, student *s) {
	if (*prim != NULL && *ultim != NULL) {
		(*s).cod = (*prim)->inf.cod;
		(*s).nume = (char*)malloc((strlen((*prim)->inf.nume) + 1) * sizeof(char));
		strcpy((*s).nume, (*prim)->inf.nume);
		(*s).medie = (*prim)->inf.medie;
		nodCoada *temp = *prim;
		*prim = (*prim)->next;
		free(temp->inf.nume);
		free(temp);
		return 0;
	}
	else {
		if (*prim == NULL) {
			*ultim = NULL;
			return -1;
		}
	}
}

void traversareCoada(nodCoada *prim) {
	nodCoada *temp = prim;
	while (temp) {
		printf("\nCod=%d, Nume=%s, Medie=%5.2f", temp->inf.cod, temp->inf.nume, temp->inf.medie);
		temp = temp->next;
	}
}

void conversieHashTableCoada(hashT tabela, nodCoada **prim, nodCoada **ultim) {
	if (tabela.vect != NULL) {
		for (int i = 0; i < tabela.size; i++)
			if (tabela.vect[i] != NULL) {
				nodLS *temp = tabela.vect[i];
				while (temp) {
					put(prim, ultim, temp->inf);
					temp = temp->next;
				}
			}
	}
	dezalocare(tabela);
}

void main()
{
	FILE *f = fopen("fisier.txt", "r");
	hashT tabela;
	tabela.size = 101;
	tabela.vect = (nodLS**)malloc(tabela.size * sizeof(nodLS*));

	for (int i = 0; i < tabela.size; i++)
		tabela.vect[i] = NULL;

	int n;
	fscanf(f, "%d", &n);
	student s;
	char buffer[20];
	for (int i = 0; i < n; i++) {
		fscanf(f, "%d", &s.cod);
		fscanf(f, "%s", buffer);
		s.nume = (char*)malloc((strlen(buffer) + 1) * sizeof(char));
		strcpy(s.nume, buffer);
		fscanf(f, "%f", &s.medie);

		inserare(tabela, s);
	}
	fclose(f);

	traversare(tabela);
	//dezalocare(tabela);

	printf("\n--------------Coada-------------");

	nodCoada *prim = NULL, *ultim = NULL;
	conversieHashTableCoada(tabela, &prim, &ultim);
	traversareCoada(prim);
	student s1;
	//dezalocarea cozii
	while (get(&prim, &ultim, &s1) == 0)
		free(s1.nume);
}