﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace subiect_azi
{
    public partial class Form2 : Form
    {
        FurnizorCurent fclocal;
        FurnizorGaz fglocal;
        public Form2(FurnizorGaz fg_p, FurnizorCurent fc_p)
        {
            InitializeComponent();

            fglocal = fg_p;
            fclocal = fc_p;






            for (int i = 0; i < fclocal.facturi.Length; i++)
            {
                if (fclocal.facturi[i] != null)
                {
                    int luna = Convert.ToInt32(fclocal.facturi[i].Data.Month);
                    TreeNode t = new TreeNode(fclocal.facturi[i].ToString());
                    t.Tag = fclocal.facturi[i];
                    treeView1.Nodes[1].Nodes[luna - 1].Nodes.Add(t);
                }
            }

            for (int i = 0; i < fglocal.facturi.Length; i++)
            {
                if (fglocal.facturi[i] != null)
                {
                    int luna = Convert.ToInt32(fglocal.facturi[i].Data.Month);
                    TreeNode t = new TreeNode(fglocal.facturi[i].ToString());
                    t.Tag = fglocal.facturi[i];
                    treeView1.Nodes[0].Nodes[luna - 1].Nodes.Add(t);
                }
            }
        }

        private void stergereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for(int i = fclocal.facturi.Length - 1; i >= 0; i--)
            {
                if (fclocal.facturi[i] == treeView1.SelectedNode.Tag)
                    fclocal.facturi[i] = null;
            }


            for (int i = fglocal.facturi.Length - 1; i >= 0; i--)
            {
                if (fglocal.facturi[i] == treeView1.SelectedNode.Tag)
                    fglocal.facturi[i] = null;
            }

            treeView1.SelectedNode.Remove();

            
        }

        private void editareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* Factura fac = null;

             for (int i = 0; i < fclocal.facturi.Length; i++)
                 if (fclocal.facturi[i] == treeView1.SelectedNode.Tag)
                     fac = fclocal.facturi[i];

             for (int i = fclocal.facturi.Length - 1; i >= 0; i--)
             {
                 if (fclocal.facturi[i] == treeView1.SelectedNode.Tag)
                     fclocal.facturi[i] = null;
             }

             treeView1.SelectedNode.Remove();


             int luna = 0;
             Form1 f1 = new Form1(fac);
             f1.Show();


             luna = Convert.ToInt32(fac.Data.Month);
             TreeNode t = new TreeNode(fac.ToString());
                 t.Tag = fac;
                 treeView1.Nodes[1].Nodes[luna - 1].Nodes.Add(t);*/

            treeView1.LabelEdit = true;
            int i = treeView1.SelectedNode.Index;
            treeView1.Nodes[0].Nodes[0].Nodes[i].BeginEdit();



            //for(int i = 0; i<treeView1.Nodes.Count; i++)
            //{
            //    for(int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
            //    {
            //        treeView1.Nodes[i].Nodes[j].Nodes.Clear();
            //    }
            //}

            //for (int i = 0; i < fclocal.facturi.Length; i++)
            //{
            //    if (fclocal.facturi[i] != null)
            //    {
            //        int luna = Convert.ToInt32(fclocal.facturi[i].Data.Month);
            //        TreeNode t = new TreeNode(fclocal.facturi[i].ToString());
            //        t.Tag = fclocal.facturi[i];
            //        treeView1.Nodes[1].Nodes[luna - 1].Nodes.Add(t);
            //    }
            //}

            //for (int i = 0; i < fglocal.facturi.Length; i++)
            //{
            //    if (fglocal.facturi[i] != null)
            //    {
            //        int luna = Convert.ToInt32(fglocal.facturi[i].Data.Month);
            //        TreeNode t = new TreeNode(fglocal.facturi[i].ToString());
            //        t.Tag = fglocal.facturi[i];
            //        treeView1.Nodes[0].Nodes[luna - 1].Nodes.Add(t);
            //    }
            //}
        }
    }
}
