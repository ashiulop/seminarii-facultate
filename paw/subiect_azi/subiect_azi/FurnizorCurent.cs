﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace subiect_azi
{
    public class FurnizorCurent
    {
        public Factura[] facturi;

        public FurnizorCurent()
        {
            this.facturi = new Factura[12];
            for (int i = 0; i < 12; i++)
                facturi[i] = null;

        }

        public FurnizorCurent(Factura[] facturi)
        {
            for (int i = 0; i < facturi.Length; i++)
                this.facturi[i] = facturi[i];
        }

        public static FurnizorCurent operator +(FurnizorCurent fg, Factura f )
        {
            int pozitie = f.Data.Month;
            fg.facturi[pozitie] = f;
            return fg;
        }
    }
}
