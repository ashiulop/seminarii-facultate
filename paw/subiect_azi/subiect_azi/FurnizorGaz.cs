﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace subiect_azi
{
    public class FurnizorGaz
    {
        public Factura[] facturi;

        public FurnizorGaz()
        {
            this.facturi = new Factura[12];
            for (int i = 0; i < 12; i++)
                facturi[i] = null;

        }

        public FurnizorGaz(Factura[] facturi)
        {
            for (int i = 0; i < facturi.Length; i++)
                this.facturi[i] = facturi[i];
        }

        public static FurnizorGaz operator+ ( FurnizorGaz fg, Factura f)
        {
            int pozitie = f.Data.Month;
            fg.facturi[pozitie] = f;
            return fg;
        }
    }
}
