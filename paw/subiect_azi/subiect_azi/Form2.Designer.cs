﻿namespace subiect_azi
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("1");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("2");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("3");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("4");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("5");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("6");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("7");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("8");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("9");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("10");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("11");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("12");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Gaz", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("1");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("2");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("3");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("4");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("5");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("6");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("7");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("8");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("9");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("10");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("11");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("12");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Curent", new System.Windows.Forms.TreeNode[] {
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode18,
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stergereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Location = new System.Drawing.Point(55, 12);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node4";
            treeNode1.Text = "1";
            treeNode2.Name = "Node6";
            treeNode2.Text = "2";
            treeNode3.Name = "Node7";
            treeNode3.Text = "3";
            treeNode4.Name = "Node8";
            treeNode4.Text = "4";
            treeNode5.Name = "Node9";
            treeNode5.Text = "5";
            treeNode6.Name = "Node10";
            treeNode6.Text = "6";
            treeNode7.Name = "Node11";
            treeNode7.Text = "7";
            treeNode8.Name = "Node12";
            treeNode8.Text = "8";
            treeNode9.Name = "Node13";
            treeNode9.Text = "9";
            treeNode10.Name = "Node14";
            treeNode10.Text = "10";
            treeNode11.Name = "Node15";
            treeNode11.Text = "11";
            treeNode12.Name = "Node16";
            treeNode12.Text = "12";
            treeNode13.Name = "Node0";
            treeNode13.Text = "Gaz";
            treeNode14.Name = "Node17";
            treeNode14.Text = "1";
            treeNode15.Name = "Node18";
            treeNode15.Text = "2";
            treeNode16.Name = "Node19";
            treeNode16.Text = "3";
            treeNode17.Name = "Node20";
            treeNode17.Text = "4";
            treeNode18.Name = "Node21";
            treeNode18.Text = "5";
            treeNode19.Name = "Node22";
            treeNode19.Text = "6";
            treeNode20.Name = "Node23";
            treeNode20.Text = "7";
            treeNode21.Name = "Node24";
            treeNode21.Text = "8";
            treeNode22.Name = "Node25";
            treeNode22.Text = "9";
            treeNode23.Name = "Node26";
            treeNode23.Text = "10";
            treeNode24.Name = "Node27";
            treeNode24.Text = "11";
            treeNode25.Name = "Node28";
            treeNode25.Text = "12";
            treeNode26.Name = "Node1";
            treeNode26.Text = "Curent";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode26});
            this.treeView1.Size = new System.Drawing.Size(348, 282);
            this.treeView1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editareToolStripMenuItem,
            this.stergereToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // editareToolStripMenuItem
            // 
            this.editareToolStripMenuItem.Name = "editareToolStripMenuItem";
            this.editareToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.editareToolStripMenuItem.Text = "Editare";
            this.editareToolStripMenuItem.Click += new System.EventHandler(this.editareToolStripMenuItem_Click);
            // 
            // stergereToolStripMenuItem
            // 
            this.stergereToolStripMenuItem.Name = "stergereToolStripMenuItem";
            this.stergereToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.stergereToolStripMenuItem.Text = "Stergere";
            this.stergereToolStripMenuItem.Click += new System.EventHandler(this.stergereToolStripMenuItem_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.treeView1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stergereToolStripMenuItem;
    }
}