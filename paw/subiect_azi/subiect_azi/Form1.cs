﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace subiect_azi
{
    public partial class Form1 : Form
    {
        FurnizorCurent fc = new FurnizorCurent();
        FurnizorGaz fg = new FurnizorGaz();
        public Form1( Factura fParam)
        {
            InitializeComponent();
            if (fParam != null)
            {
                this.Text = "Editare";
                cbDenumire.Text = fParam.Denumire;
                tbSuma.Text = fParam.Suma.ToString();
                dtp.Text = fParam.Data.ToString();


              
                button2.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Factura f = new Factura();
            f.Denumire = cbDenumire.SelectedItem.ToString();
            try
            {
                f.Suma = float.Parse(tbSuma.Text);
            }
            catch
            {
                MessageBox.Show("Eroare fa");
            }

            try
            {
                f.Data = Convert.ToDateTime(dtp.Text);
            }
            catch
            {
                MessageBox.Show("Eroare fa");
            }

            if (f.Denumire.Equals("Curent"))
                fc += f;
            else
                fg += f;

            for(int i = 0; i < fc.facturi.Length; i++)
            {
                if(fc.facturi[i] != null)
                    MessageBox.Show(fc.facturi[i].ToString());
            }

            

        }

        private void tbSuma_Validated(object sender, EventArgs e)
        {

        }

        private void tbSuma_Validating(object sender, CancelEventArgs e)
        {
            TextBox tblocal = (TextBox)sender;


            try
            {
                if (tblocal.Text.Length > 0)
                {
                    float x = float.Parse(tblocal.Text);
                    if (x < 0)
                    {
                        errorProvider1.SetError(tblocal, "Suma de plata trebe sa fie pozitiva");
                    }
                    else
                    {
                        errorProvider1.SetError(tblocal, "");
                    }
                }
                else
                {
                    errorProvider1.SetError(tblocal, "Introduceti suma");
                    e.Cancel = true;
                }


            }
            catch
            {

                errorProvider1.SetError(tblocal, "Suma trebuie sa contina cifre ");
                e.Cancel = true;
            }
        }

        private void cbDenumire_Validating(object sender, CancelEventArgs e)
        {
            ComboBox cblocal = (ComboBox)sender;

            if(cblocal.Text!= "")
            {
                errorProvider1.SetError(cblocal, "");
            }
            else
            {
                errorProvider1.SetError(cblocal, "Selectati furnizor");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2(fg, fc);
            f2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Factura f = new Factura();
            f.Denumire = cbDenumire.SelectedItem.ToString();
            try
            {
                f.Suma = float.Parse(tbSuma.Text);
            }
            catch
            {
                MessageBox.Show("Eroare fa");
            }

            try
            {
                f.Data = Convert.ToDateTime(dtp.Text);
            }
            catch
            {
                MessageBox.Show("Eroare fa");
            }

            if (f.Denumire.Equals("Curent"))
                fc += f;
            else
                fg += f;

            for (int i = 0; i < fc.facturi.Length; i++)
            {
                if (fc.facturi[i] != null)
                    MessageBox.Show(fc.facturi[i].ToString());
            }

            this.Close();
        }
    }
}
