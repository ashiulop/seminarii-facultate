﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace subiect_azi
{
    public class Factura
    {
        private string denumire_furnizor;
        private float suma_de_plata;
        private DateTime data_scadenta;

        public Factura() { }

        public Factura(string denumire_furnizor, float suma_de_plata, DateTime data_scadenta)
        {
            this.denumire_furnizor = denumire_furnizor;
            this.suma_de_plata = suma_de_plata;
            this.data_scadenta = data_scadenta;
        }

        public string Denumire
        {
            get
            {
                return denumire_furnizor;
            }
            set
            {
                if (value != null && value.Length > 0)
                    this.denumire_furnizor = value;
            }
        }

        public float Suma
        {
            get
            {
                return suma_de_plata;
            }
            set
            {
                if (value >= 0)
                    this.suma_de_plata = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return data_scadenta;
            }
            set
            {
                if (value != null)
                    this.data_scadenta = value;
            }
        }

        public override string ToString()
        {
            return this.denumire_furnizor + " " + this.suma_de_plata + " " + this.data_scadenta;
        }

        public int nrZile()
        {
            return (data_scadenta - DateTime.Now).Days;
        }
    }
}
