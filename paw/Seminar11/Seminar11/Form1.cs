﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seminar11
{
    public partial class Form1 : Form
    {
        int nrobs;
        float[] x, y;

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Rectangle zonaClient = e.ClipRectangle;
            Brush fundal = new SolidBrush(Color.White);

            g.FillRectangle(fundal, zonaClient);

            zonaClient.X += 20;
            zonaClient.Y += 20;
            zonaClient.Height -= 40;
            zonaClient.Width -= 40;

            int i, vl = zonaClient.Left, vb = zonaClient.Bottom, vr = zonaClient.Right, vt = zonaClient.Top;

            Pen creionRosu = new Pen(Color.Red, 2);
            g.DrawRectangle(creionRosu, zonaClient);

            int latime, distanta;
            float raport_distanta_latime = 0.2f, max;

            SolidBrush[] pensule = new SolidBrush[]
            {
                new SolidBrush(Color.Plum),
                new SolidBrush(Color.PeachPuff),
                new SolidBrush(Color.SlateGray),
                new SolidBrush(Color.Turquoise),
                new SolidBrush(Color.LightPink),
                new SolidBrush(Color.Azure)
            };

            Pen[] creioane = new Pen[]
            {
                new Pen(Color.Coral),
                new Pen(Color.Goldenrod),
                new Pen(Color.Linen),
                new Pen(Color.PowderBlue),
                new Pen(Color.Navy),
                new Pen(Color.LawnGreen)
            };

            SolidBrush pnsCrt;
            Pen penCrt;

            latime = (vr - vl) / (int)((nrobs + 1) * raport_distanta_latime + nrobs);
            distanta = (int)(latime * raport_distanta_latime);

            for(max = y[0], i = 1; i < nrobs; i++)
            {
                if(max < y[i])
                {
                    max = y[i];
                }
            }

            penCrt = creioane[0];
            pnsCrt = pensule[3];

            g.DrawLine(penCrt, vl, vt, vl, vb);
            g.DrawLine(penCrt, vl, vb, vr, vb);

            penCrt = creioane[1];
            for(i = 0; i < nrobs; i++)
            {
                pnsCrt = pensule[i % 6];
                PointF pnt = new PointF(vl + distanta + (latime + distanta) * i, vb - y[i] * (vb - vt) / max);
                SizeF sz = new SizeF(latime, y[i] * (vb - vt) / max);

                g.FillRectangle(pnsCrt, new RectangleF(pnt, sz));
                textBox1.Text += "\r\n(" + Math.Round(pnt.X, 2) + ", " + Math.Round(pnt.Y, 2) + ") --> " + 
                    Math.Round(sz.Width, 2) + ", " + Math.Round(sz.Height, 2);
                string denumire = x[i].ToString();
                g.DrawString(denumire, Font, pnsCrt, vl + distanta + latime / 2 + i * (latime + distanta), vb + 5);
                textBox1.Text += "\r\n";
            }
        }

        private void panel2_Resize(object sender, EventArgs e)
        {
            //panel2.Invalidate();
        }

        public Form1()
        {
            InitializeComponent();
            nrobs = 5;
            x = new float[] { 1980f, 1985f, 1990f, 1995f, 2010f };
            y = new float[] { 32.0f, 94.0f, 68.0f, 115.0f, 95f };
           this.ResizeRedraw = true;
        }
    }
}
