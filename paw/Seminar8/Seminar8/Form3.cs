﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seminar8
{
    public partial class Form3 : Form
    {
        List<Persoana> listaPersoane;
        public Form3(List<Persoana> pLista)
        {
            InitializeComponent();
            listaPersoane = pLista;
        }

        private void AdaugaInControale(Persoana p)
        {
            ListViewItem lvi = new ListViewItem(p.cnp);
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Nume));
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Prenume));
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Varsta.ToString()));

            lvi.Tag = p;
            listView1.Items.Add(lvi);
        }
        public void IncarcaLista(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            foreach(Persoana p in listaPersoane)
            {
                AdaugaInControale(p);
            }
        }
    }
}
