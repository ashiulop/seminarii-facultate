﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminar8
{
    [Serializable]
    public class Persoana
    {
        public string Nume;
        public string Prenume;
        public int Varsta;
        public string cnp;

        public Persoana(string c, string n, string p, int v)
        {
            cnp = c;
            Nume = n;
            Prenume = p;
            Varsta = v;
        }

        public Persoana()
        {
            cnp = "";
            Nume = "";
            Prenume = "";
            Varsta = 0;
        }

        public override string ToString()
        {
            return cnp + " " + Nume + " " + Prenume + " " + Varsta;
        }
    }
}
