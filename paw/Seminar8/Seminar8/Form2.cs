﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seminar8
{
    public partial class Form2 : Form
    {
        Persoana pForm2;
        List<Persoana> listaPersoane;
        public Form2(List<Persoana> listaPersoane)
        {
            pForm2 = new Persoana();
            InitializeComponent();

            this.listaPersoane = listaPersoane;
            this.PersoanaModificata += ActualizeazaFormular;
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            pForm2.cnp = textBoxCNP.Text;
            pForm2.Nume = textBoxNume.Text;
            pForm2.Prenume = textBoxPrenume.Text;
            DialogResult = DialogResult.OK;
            try
            {
                pForm2.Varsta = Convert.ToInt32(textBoxVarsta.Text);
            }
            catch
            {
                MessageBox.Show("Eroare");
                DialogResult = DialogResult.Cancel;

            }
            if (DialogResult == DialogResult.OK)
            {
                MessageBox.Show("Persoana adaugata in lista!");
                listaPersoane.Add(pForm2);
                ((Form1)this.MdiParent).OnListaModificata(EventArgs.Empty);
            }
        }

        public void ActualizeazaFormular(object sender, EventArgs e)
        {
            textBoxCNP.Text = pForm2.cnp;
            textBoxNume.Text = pForm2.Nume;
            textBoxPrenume.Text = pForm2.Prenume;
            textBoxVarsta.Text = pForm2.Varsta.ToString();
        }

        private void salveazaInFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.CheckPathExists = true;
            fd.Filter = "fisiere persoane (*.prs)|*.prs";
            if(fd.ShowDialog() == DialogResult.OK)
            {
                Stream myFile = File.Create(fd.FileName);
                BinaryFormatter serializator = new BinaryFormatter();
                serializator.Serialize(myFile, this.pForm2);
                myFile.Close();
            }
        }

        private void incarcaDinFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.CheckFileExists = true;
            fd.Filter = "fisiere persoane (*.prs)|*.prs";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                Stream myFile = File.OpenRead(fd.FileName);
                BinaryFormatter deserializator = new BinaryFormatter();

                pForm2 = new Persoana();
                pForm2 = (Persoana)deserializator.Deserialize(myFile);

                listaPersoane.Add(pForm2);
                ((Form1)this.MdiParent).OnListaModificata(EventArgs.Empty);
                OnPersoanaModificata(EventArgs.Empty);


            }
        }

        public event EventHandler PersoanaModificata;

        public virtual void OnPersoanaModificata(EventArgs e)
        {
            PersoanaModificata?.Invoke(this, e);
        }

        public delegate void PersoanaModificataEventHandler(object sender, EventArgs e);

        private void salveazaInFisierTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SaveFileDialog fd = new SaveFileDialog();
            //fd.CheckPathExists = true;
            //fd.Filter = "Fisiere text (*.txt)|*.txt";
            //if (fd.ShowDialog() == DialogResult.OK)
            //{
            //    FileStream fs = new FileStream(fd.FileName, FileMode.Create, FileAccess.Write);
            //    StreamWriter sw = new StreamWriter(fs);
            //    sw.WriteLine(pForm2.ToString());
            //    sw.Close();
            //    fs.Close();
            //}
        }

        private void salveazaInFisierTextToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.CheckPathExists = true;
            fd.Filter = "Fisiere text (*.txt)|*.txt";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(fd.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(pForm2.ToString());
                sw.Close();
                fs.Close();
            }
        }

        private void salveazaInFisierBinarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void incarcaDinFisierBinarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void incarcaDinFisierTextToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.CheckPathExists = true;
            fd.Filter = "Fisiere text (*.txt)|*.txt";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(fd.FileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);

                string[] tf = sr.ReadToEnd().Split(' ');
                pForm2 = new Persoana(tf[0], tf[1], tf[2], Convert.ToInt32(tf[3]));

                listaPersoane.Add(pForm2);
                ((Form1)this.MdiParent).OnListaModificata(EventArgs.Empty);
                OnPersoanaModificata(EventArgs.Empty);

                sr.Close();
                fs.Close();
            }
        }
    }
}
