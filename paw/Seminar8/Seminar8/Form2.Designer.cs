﻿namespace Seminar8
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCNP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.textBoxVarsta = new System.Windows.Forms.TextBox();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.labelVarsta = new System.Windows.Forms.Label();
            this.labelPrenume = new System.Windows.Forms.Label();
            this.labelNume = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fisiereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaInFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaDinFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaInFisierTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaInFisierBinarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxCNP
            // 
            this.textBoxCNP.Location = new System.Drawing.Point(200, 60);
            this.textBoxCNP.Name = "textBoxCNP";
            this.textBoxCNP.Size = new System.Drawing.Size(100, 20);
            this.textBoxCNP.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "CNP";
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAdauga.Location = new System.Drawing.Point(144, 220);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(141, 23);
            this.buttonAdauga.TabIndex = 16;
            this.buttonAdauga.Text = "Adauga in lista";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // textBoxVarsta
            // 
            this.textBoxVarsta.Location = new System.Drawing.Point(200, 159);
            this.textBoxVarsta.Name = "textBoxVarsta";
            this.textBoxVarsta.Size = new System.Drawing.Size(100, 20);
            this.textBoxVarsta.TabIndex = 15;
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Location = new System.Drawing.Point(200, 125);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrenume.TabIndex = 14;
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(200, 90);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(100, 20);
            this.textBoxNume.TabIndex = 13;
            // 
            // labelVarsta
            // 
            this.labelVarsta.AutoSize = true;
            this.labelVarsta.Location = new System.Drawing.Point(117, 159);
            this.labelVarsta.Name = "labelVarsta";
            this.labelVarsta.Size = new System.Drawing.Size(37, 13);
            this.labelVarsta.TabIndex = 12;
            this.labelVarsta.Text = "Varsta";
            // 
            // labelPrenume
            // 
            this.labelPrenume.AutoSize = true;
            this.labelPrenume.Location = new System.Drawing.Point(117, 125);
            this.labelPrenume.Name = "labelPrenume";
            this.labelPrenume.Size = new System.Drawing.Size(49, 13);
            this.labelPrenume.TabIndex = 11;
            this.labelPrenume.Text = "Prenume";
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(117, 90);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(35, 13);
            this.labelNume.TabIndex = 10;
            this.labelNume.Text = "Nume";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fisiereToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(441, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fisiereToolStripMenuItem
            // 
            this.fisiereToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salveazaInFisierBinarToolStripMenuItem,
            this.incarcaDinFisierBinarToolStripMenuItem,
            this.salveazaInFisierTextToolStripMenuItem,
            this.salveazaInFisierBinarToolStripMenuItem1});
            this.fisiereToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.fisiereToolStripMenuItem.Name = "fisiereToolStripMenuItem";
            this.fisiereToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.fisiereToolStripMenuItem.Text = "Fisiere";
            // 
            // salveazaInFisierBinarToolStripMenuItem
            // 
            this.salveazaInFisierBinarToolStripMenuItem.Name = "salveazaInFisierBinarToolStripMenuItem";
            this.salveazaInFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.salveazaInFisierBinarToolStripMenuItem.Text = "Salveaza in fisier binar";
            this.salveazaInFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.salveazaInFisierBinarToolStripMenuItem_Click);
            // 
            // incarcaDinFisierBinarToolStripMenuItem
            // 
            this.incarcaDinFisierBinarToolStripMenuItem.Name = "incarcaDinFisierBinarToolStripMenuItem";
            this.incarcaDinFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.incarcaDinFisierBinarToolStripMenuItem.Text = "Incarca din fisier binar";
            this.incarcaDinFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.incarcaDinFisierBinarToolStripMenuItem_Click);
            // 
            // salveazaInFisierTextToolStripMenuItem
            // 
            this.salveazaInFisierTextToolStripMenuItem.Name = "salveazaInFisierTextToolStripMenuItem";
            this.salveazaInFisierTextToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.salveazaInFisierTextToolStripMenuItem.Text = "Salveaza in fisier text";
            this.salveazaInFisierTextToolStripMenuItem.Click += new System.EventHandler(this.salveazaInFisierTextToolStripMenuItem_Click_1);
            // 
            // salveazaInFisierBinarToolStripMenuItem1
            // 
            this.salveazaInFisierBinarToolStripMenuItem1.Name = "salveazaInFisierBinarToolStripMenuItem1";
            this.salveazaInFisierBinarToolStripMenuItem1.Size = new System.Drawing.Size(190, 22);
            this.salveazaInFisierBinarToolStripMenuItem1.Text = "Incarca din fisier text";
            this.salveazaInFisierBinarToolStripMenuItem1.Click += new System.EventHandler(this.incarcaDinFisierTextToolStripMenuItem1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 317);
            this.Controls.Add(this.textBoxCNP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.textBoxVarsta);
            this.Controls.Add(this.textBoxPrenume);
            this.Controls.Add(this.textBoxNume);
            this.Controls.Add(this.labelVarsta);
            this.Controls.Add(this.labelPrenume);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCNP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.TextBox textBoxVarsta;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.Label labelVarsta;
        private System.Windows.Forms.Label labelPrenume;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fisiereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salveazaInFisierBinarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaDinFisierBinarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salveazaInFisierTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salveazaInFisierBinarToolStripMenuItem1;
    }
}