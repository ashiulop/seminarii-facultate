﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Seminar8
{
    public partial class Form1 : Form
    {
        List<Persoana> listaPersoane = new List<Persoana>();
        int nrc = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void fereastraNouaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 fCopil = new Form2(listaPersoane);
            fCopil.MdiParent = this;
            fCopil.Size = new Size(400, 320);
            nrc++;
            fCopil.Text = "Fereastra copil " + nrc;
            fCopil.Show();
        }

        public event EventHandler ListaModificata;

        public virtual void OnListaModificata(EventArgs e)
        {
            ListaModificata?.Invoke(this, e);
        }

        public delegate void ListaModificataEventHandler(object sender, EventArgs e);

        private void fereastraCentralizatoareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 f3Copil = new Form3(listaPersoane);
            f3Copil.MdiParent = this;
            f3Copil.Size = new Size(490, 320);
            f3Copil.Text = "Fereastra centralizatoare";
            f3Copil.Show();
            this.ListaModificata += f3Copil.IncarcaLista;
        }

        private void salveazaXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MemoryStream memStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memStream, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;

            writer.WriteStartDocument();
            writer.WriteStartElement("Lista-persoane");
            foreach (Persoana p in listaPersoane)
            {
                writer.WriteStartElement("persoana");
                writer.WriteElementString("CNP", p.cnp);
                writer.WriteElementString("Nume", p.Nume);
                writer.WriteElementString("Prenume", p.Prenume);
                writer.WriteElementString("Varsta", p.Varsta.ToString());

                writer.WriteEndElement(); //persoana
            }
            writer.WriteEndElement(); //lista-persoane
            writer.WriteEndDocument();
            writer.Close();

            string xmlString = Encoding.UTF8.GetString(memStream.ToArray());

            memStream.Close();
            memStream.Dispose();

            StreamWriter sw = new StreamWriter("C:\\temp\\fisier.xml");
            sw.WriteLine(xmlString);
            sw.Close();

            MessageBox.Show("Fisier generat cu succes!");
        }

        private void incarcaXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("C:\\temp\\fisier.xml");
            string str = sr.ReadToEnd();
            listaPersoane.Clear();

            XmlReader reader = XmlReader.Create(new StringReader(str));
            while (reader.Read())
            {
                if(reader.Name == "persoana" && reader.NodeType == XmlNodeType.Element)
                {
                    Persoana pLocal;
                    while(reader.Read() && reader.Name != "CNP") { }
                    reader.Read();
                    string c = reader.Value;

                    while (reader.Read() && reader.Name != "Nume") { }
                    reader.Read();
                    string n = reader.Value;

                    while (reader.Read() && reader.Name != "Prenume") { }
                    reader.Read();
                    string p = reader.Value;

                    while (reader.Read() && reader.Name != "Varsta") { }
                    reader.Read();
                    int v = Convert.ToInt32(reader.Value);

                    pLocal = new Persoana(c, n, p, v);
                    listaPersoane.Add(pLocal);
                }
            }
            OnListaModificata(EventArgs.Empty);
            sr.Close();
        }
    }
}
