﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen
{
    public class Produs
    {
        private int cod;
        private string denumire;
        private float pret;
        private int cantitate;

        public Produs() { }

        public Produs(int cod, string denumire, float pret, int cantitate)
        {
            this.cod = cod;
            this.denumire = denumire;
            this.pret = pret;
            this.cantitate = cantitate;
        }

        public int Cod
        {
            get
            {
                return this.cod;
            }
            set
            {
                if (value > 0)
                    this.cod = value;
            }
        }
        public string Denumire
        {
            get
            {
                return this.denumire;
            }
            set
            {
                if (value.Length > 0)
                    this.denumire = value;
            }
        }

        public float Pret
        {
            get
            {
                return this.pret;
            }
            set
            {
                if (value > 0)
                    this.pret = value;
            }
        }

        public int Cantitate
        {
            get
            {
                return this.cantitate;
            }
            set
            {
                if (value > 0)
                    this.cantitate = value;
            }
        }

        public float Valoare
        {
            get
            {
                return this.pret * this.cantitate;
            }
        }

        public override string ToString()
        {
            return this.cod + " " + this.denumire + " " + this.pret + " " + this.cantitate;
        }
    }

}
