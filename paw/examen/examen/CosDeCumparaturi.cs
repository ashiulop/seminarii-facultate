﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen
{
    public class CosDeCumparaturi
    {
        public Produs[] vector;

        public CosDeCumparaturi()
        {
            this.vector = new Produs[0];
        }

        public CosDeCumparaturi(Produs[] vector)
        {
            this.vector = vector;
        }

        public static CosDeCumparaturi operator +(CosDeCumparaturi c, Produs p)
        {   

            Produs[] copie = new Produs[c.vector.Length + 1];
            for (int i = 0; i < c.vector.Length; i++)
            {
                copie[i] = c.vector[i];
            }
            copie[c.vector.Length] = p;
            c.vector = copie;
            return c;


        }



    }
}
