﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examen
{
    public partial class Form1 : Form
    {
        CosDeCumparaturi cos = new CosDeCumparaturi();
        public Form1()
        {
            InitializeComponent();
        }

        private void adaugaProdusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produs p = new Produs();
            Form2 f2 = new Form2(p);
            f2.ShowDialog();
            if (f2.DialogResult == DialogResult.OK)
            {
                cos += p;
                adaugaInLVI(cos);
            }

            for(int i = 0; i <cos.vector.Length; i++)
            {
                MessageBox.Show(cos.vector[i].ToString());
            }
        }

        private void adaugaInLVI(CosDeCumparaturi cos)
        {
            listView1.Items.Clear();
            for (int i = 0; i < cos.vector.Length; i++)
            {
                if (cos.vector[i] != null)
                {
                    ListViewItem lvi = new ListViewItem(cos.vector[i].Cod.ToString());
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, cos.vector[i].Denumire));
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, cos.vector[i].Pret.ToString()));
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, cos.vector[i].Cantitate.ToString()));

                    lvi.Tag = cos.vector[i];

                    listView1.Items.Add(lvi);
                }
            }


        }

        private void stergeToolStripMenuItem_Click(object sender, EventArgs e)
        {


            CosDeCumparaturi temp = new CosDeCumparaturi();

            for (int i = 0; i < cos.vector.Length; i++)
            {
                if (cos.vector[i] != listView1.SelectedItems[0].Tag)
                    temp += cos.vector[i];
            }
            cos = temp;
            listView1.SelectedItems[0].Remove();


        }

        private void editeazaToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFileDialog dlg1 = new SaveFileDialog();
            dlg1.Filter = "(.txt)|.txt";
            if (dlg1.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(dlg1.FileName, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                string r = null;
                foreach (ListViewItem itm in listView1.Items)
                {


                    r += "Cod  " + itm.SubItems[0].Text + " nume produs:  " + itm.SubItems[1].Text +
                        " pret:  " + itm.SubItems[2].Text + " cantitate: " + itm.SubItems[3].Text + Environment.NewLine;

                    sw.WriteLine(r);
                }


                sw.Close();
                fs.Close();
            }


        }
    }
}
