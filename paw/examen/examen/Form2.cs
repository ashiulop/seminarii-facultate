﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examen
{
    public partial class Form2 : Form
    {
        Produs p;
        public Form2(Produs param)
        {
            InitializeComponent();
            p = param;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool valid = true;
            try
            {
                p.Cod = int.Parse(tbCod.Text);
            }
            catch
            {
                MessageBox.Show("Codul trebuie sa fie numeric!");
                valid = false;

            }
            p.Denumire = tbDenumire.Text;
            try
            {
                p.Pret = float.Parse(tbPret.Text);
            }
            catch
            {
                MessageBox.Show("Pretul trebuie sa fie numeric!");
                valid = false;
            }
            try
            {
                p.Cantitate = int.Parse(tbCantitate.Text);
            }
            catch
            {
                MessageBox.Show("Cantitatea trebuie sa fie numerica!");
                valid = false;
            }

            if (!valid)
                btnAdd.DialogResult = DialogResult.Cancel;

        }
    }
}
