﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seminar2
{
    class Animal: ICloneable, IComparable
    {
        private int varsta;
        private string nume;
        private float greutate;

        public int Varsta
        {
            get { return varsta; }
            set
            {
                if (value > 0 && value < 500)
                    varsta = value;
            }
        }

        public string Nume
        {
            get { return nume; }
            set
            {
                if (value.Length >= 5)
                    nume = value;
            }
        }

        public float Greutate
        {
            get { return greutate; }
            set
            {
                if (value > 0 && value < 10000)
                    greutate = value;
            }
        }

        public Animal(int v, string n, float g)
        {
            varsta = v;
            nume = n;
            greutate = g;
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        public Animal Clone()
        {
            return (Animal)((ICloneable)this).Clone();
        }

        public int CompareTo(object obj)
        {
            Animal a2 = (Animal)obj;
            if (this.varsta < a2.varsta)
                return 1;
            else if (this.varsta > a2.varsta)
                return -1;
            else
                return string.Compare(this.nume, a2.nume);
        }

        public override string ToString()
        {
            return "Animalul " + nume + " are " + varsta + " ani si greutatea " + greutate + " kg";
        }
    }

    class Pinguin: Animal
    {
        private bool areIgloo;

        public bool AreIgloo
        {
            get { return areIgloo; }
            set { areIgloo = value; }
        }

        public Pinguin(bool a, int v, string n, float g) : base(v, n, g)
        {
            areIgloo = a;
        }

        public override string ToString()
        {
            return base.ToString() + " " + areIgloo;
        }
    }

    class Insecta : Animal
    {
        private int nrPicioare;

        private int NrPicioare
        {
            get { return nrPicioare; }
            set
            {
                if (value > 6 && value < 200)
                    nrPicioare = value;
            }
        }

        public Insecta(int nr, int v, string n, float g) : base(v, n, g)
        {
            nrPicioare = nr;
        }

        public override string ToString()
        {
            return base.ToString() + " si are " + nrPicioare + " picioare";
        }
    }

    class Zoo : ICloneable
    {
        private string denumire;
        private List<Animal> lista = new List<Animal>();

        public string Denumire
        {
            get { return denumire; }
            set { denumire = value; }
        }

        public List<Animal> Lista
        {
            get { return lista; }
            set { lista = value; }
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        public Zoo Clone()
        {
            Zoo nou = (Zoo)((ICloneable)this).Clone();
            List<Animal> listaNoua = new List<Animal>();
            foreach (Animal a in this.lista)
                listaNoua.Add(a.Clone());
            nou.lista = listaNoua;
            return nou;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Animal a1 = new Animal(7, "Azorel", 15);
            Animal a2 = new Animal(9, "Cita", 12);

            Pinguin p1 = new Pinguin(true, 4, "Pinguin 1", 20);

            Insecta i1 = new Insecta(8, 1, "Paianjenel", 0.5f);

            Zoo z1 = new Zoo();
            z1.Denumire = "Baneasa";
            z1.Lista.Add(a1);
            z1.Lista.Add(a2);
            z1.Lista.Add(p1);
            z1.Lista.Add(i1);

            //ArrayList listaobiecte = new ArrayList();

            //listaobiecte.Add(a1);
            //listaobiecte.Add(a2);
            //listaobiecte.Add(p1);
            //listaobiecte.Add(i1);

            //Console.WriteLine(a1);
            //Console.WriteLine(a2.ToString());
            //Console.WriteLine(p1);

            //a1.Nume += " modificat";

            //foreach (Animal a in listaobiecte)
            //    Console.WriteLine(a);

            Console.WriteLine(z1.Denumire);
            foreach(Animal a in z1.Lista)
            {
                Console.WriteLine(a);
            }

            

            Zoo z2 = z1.Clone();

            a1.Nume += " modificat";

            Console.WriteLine(z2.Denumire);
            foreach (Animal a in z2.Lista)
            {
                Console.WriteLine(a);
            }

            Console.ReadLine();
        }
    }
}
