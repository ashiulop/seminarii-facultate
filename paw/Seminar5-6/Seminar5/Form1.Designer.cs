﻿namespace Seminar5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelProdus = new System.Windows.Forms.Label();
            this.labelCantitate = new System.Windows.Forms.Label();
            this.labelPret = new System.Windows.Forms.Label();
            this.labelValoare = new System.Windows.Forms.Label();
            this.bSterge1 = new System.Windows.Forms.Button();
            this.cbProdus1 = new System.Windows.Forms.ComboBox();
            this.tbCantitate1 = new System.Windows.Forms.TextBox();
            this.tbPret1 = new System.Windows.Forms.TextBox();
            this.tbValoare1 = new System.Windows.Forms.TextBox();
            this.bLinieNoua = new System.Windows.Forms.Button();
            this.bCalculeaza = new System.Windows.Forms.Button();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelProdus
            // 
            this.labelProdus.AutoSize = true;
            this.labelProdus.Location = new System.Drawing.Point(97, 52);
            this.labelProdus.Name = "labelProdus";
            this.labelProdus.Size = new System.Drawing.Size(40, 13);
            this.labelProdus.TabIndex = 0;
            this.labelProdus.Text = "Produs";
            // 
            // labelCantitate
            // 
            this.labelCantitate.AutoSize = true;
            this.labelCantitate.Location = new System.Drawing.Point(233, 52);
            this.labelCantitate.Name = "labelCantitate";
            this.labelCantitate.Size = new System.Drawing.Size(49, 13);
            this.labelCantitate.TabIndex = 1;
            this.labelCantitate.Text = "Cantitate";
            // 
            // labelPret
            // 
            this.labelPret.AutoSize = true;
            this.labelPret.Location = new System.Drawing.Point(348, 52);
            this.labelPret.Name = "labelPret";
            this.labelPret.Size = new System.Drawing.Size(26, 13);
            this.labelPret.TabIndex = 2;
            this.labelPret.Text = "Pret";
            // 
            // labelValoare
            // 
            this.labelValoare.AutoSize = true;
            this.labelValoare.Location = new System.Drawing.Point(463, 52);
            this.labelValoare.Name = "labelValoare";
            this.labelValoare.Size = new System.Drawing.Size(43, 13);
            this.labelValoare.TabIndex = 3;
            this.labelValoare.Text = "Valoare";
            // 
            // bSterge1
            // 
            this.bSterge1.Location = new System.Drawing.Point(12, 77);
            this.bSterge1.Name = "bSterge1";
            this.bSterge1.Size = new System.Drawing.Size(75, 23);
            this.bSterge1.TabIndex = 4;
            this.bSterge1.Text = "Sterge";
            this.bSterge1.UseVisualStyleBackColor = true;
            this.bSterge1.Click += new System.EventHandler(this.bSterge1_Click);
            // 
            // cbProdus1
            // 
            this.cbProdus1.FormattingEnabled = true;
            this.cbProdus1.Items.AddRange(new object[] {
            "Laptop",
            "Desktop",
            "Mouse",
            "Tastatura"});
            this.cbProdus1.Location = new System.Drawing.Point(100, 79);
            this.cbProdus1.Name = "cbProdus1";
            this.cbProdus1.Size = new System.Drawing.Size(121, 21);
            this.cbProdus1.TabIndex = 5;
            this.cbProdus1.Validating += new System.ComponentModel.CancelEventHandler(this.cbProdus1_Validating);
            // 
            // tbCantitate1
            // 
            this.tbCantitate1.Location = new System.Drawing.Point(236, 79);
            this.tbCantitate1.Name = "tbCantitate1";
            this.tbCantitate1.Size = new System.Drawing.Size(100, 20);
            this.tbCantitate1.TabIndex = 6;
            this.tbCantitate1.Validating += new System.ComponentModel.CancelEventHandler(this.tbCantitate1_Validating);
            // 
            // tbPret1
            // 
            this.tbPret1.Location = new System.Drawing.Point(351, 79);
            this.tbPret1.Name = "tbPret1";
            this.tbPret1.Size = new System.Drawing.Size(100, 20);
            this.tbPret1.TabIndex = 7;
            // 
            // tbValoare1
            // 
            this.tbValoare1.Enabled = false;
            this.tbValoare1.Location = new System.Drawing.Point(466, 79);
            this.tbValoare1.Name = "tbValoare1";
            this.tbValoare1.Size = new System.Drawing.Size(100, 20);
            this.tbValoare1.TabIndex = 8;
            // 
            // bLinieNoua
            // 
            this.bLinieNoua.Location = new System.Drawing.Point(597, 79);
            this.bLinieNoua.Name = "bLinieNoua";
            this.bLinieNoua.Size = new System.Drawing.Size(75, 23);
            this.bLinieNoua.TabIndex = 9;
            this.bLinieNoua.Text = "Linie noua";
            this.bLinieNoua.UseVisualStyleBackColor = true;
            this.bLinieNoua.Click += new System.EventHandler(this.bLinieNoua_Click);
            // 
            // bCalculeaza
            // 
            this.bCalculeaza.Location = new System.Drawing.Point(713, 29);
            this.bCalculeaza.Name = "bCalculeaza";
            this.bCalculeaza.Size = new System.Drawing.Size(75, 23);
            this.bCalculeaza.TabIndex = 10;
            this.bCalculeaza.Text = "Calculeaza";
            this.bCalculeaza.UseVisualStyleBackColor = true;
            this.bCalculeaza.Click += new System.EventHandler(this.bCalculeaza_Click);
            // 
            // tbTotal
            // 
            this.tbTotal.Enabled = false;
            this.tbTotal.Location = new System.Drawing.Point(816, 29);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.Size = new System.Drawing.Size(100, 20);
            this.tbTotal.TabIndex = 11;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 450);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.bCalculeaza);
            this.Controls.Add(this.bLinieNoua);
            this.Controls.Add(this.tbValoare1);
            this.Controls.Add(this.tbPret1);
            this.Controls.Add(this.tbCantitate1);
            this.Controls.Add(this.cbProdus1);
            this.Controls.Add(this.bSterge1);
            this.Controls.Add(this.labelValoare);
            this.Controls.Add(this.labelPret);
            this.Controls.Add(this.labelCantitate);
            this.Controls.Add(this.labelProdus);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelProdus;
        private System.Windows.Forms.Label labelCantitate;
        private System.Windows.Forms.Label labelPret;
        private System.Windows.Forms.Label labelValoare;
        private System.Windows.Forms.Button bSterge1;
        private System.Windows.Forms.ComboBox cbProdus1;
        private System.Windows.Forms.TextBox tbCantitate1;
        private System.Windows.Forms.TextBox tbPret1;
        private System.Windows.Forms.TextBox tbValoare1;
        private System.Windows.Forms.Button bLinieNoua;
        private System.Windows.Forms.Button bCalculeaza;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

