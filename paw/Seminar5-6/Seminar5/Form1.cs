﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seminar5
{
    public partial class Form1 : Form
    {
        List<object> lista = new List<object>();
        int numarLinii = 1;
        int distantaDintreLinii = 25;

        public Form1()
        {
            InitializeComponent();

            linie l0 = new linie();
            l0.produs = cbProdus1;
            l0.cantitate = tbCantitate1;
            l0.pret = tbPret1;
            l0.valoare = tbValoare1;
            l0.butonSterge = bSterge1;

            lista.Add(l0);
        }

        private void cbProdus1_Validating(object sender, CancelEventArgs e)
        {
            ComboBox cbLocal = (ComboBox)sender;
            if(cbLocal.SelectedIndex < 0)
            {
                errorProvider1.SetError(cbLocal, "Selectati produs!");
            }
            else
            {
                errorProvider1.SetError(cbLocal, "");
            }
        }

        private void tbCantitate1_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            bool ok = true;
            try
            {
                int cant = Convert.ToInt32(tbLocal.Text);
            }
            catch
            {
                errorProvider1.SetError(tbLocal, "Introduceti cantitatea!");
                ok = false;
            }
            if (ok)
            {
                errorProvider1.SetError(tbLocal, "");
            }
           
        }

        private void bCalculeaza_Click(object sender, EventArgs e)
        {
            double valoareTotala = 0;
            foreach(linie l in lista)
            {
                double cant = Convert.ToInt32(((TextBox)l.cantitate).Text);
                double pret = Convert.ToInt32(((TextBox)l.pret).Text);
                valoareTotala += pret * cant;
                ((TextBox)l.valoare).Text = (pret * cant).ToString();
            }
            tbTotal.Text = valoareTotala.ToString();
        }

        private void bLinieNoua_Click(object sender, EventArgs e)
        {
            ComboBox cb1 = new ComboBox();
            TextBox tb1 = new TextBox();
            TextBox tb2 = new TextBox();
            TextBox tb3 = new TextBox();
            Button b2 = new Button();

            //sectiune comboBox
            cb1.Location = new Point(cbProdus1.Location.X, cbProdus1.Location.Y + numarLinii * distantaDintreLinii);
            cb1.Name = "cbProdus" + (numarLinii + 1).ToString();
            cb1.Size = cbProdus1.Size;
            cb1.TabIndex = cbProdus1.TabIndex + 4 * numarLinii;
            cb1.Items.AddRange(new object[]
                {
                    "Laptop",
                    "Desktop",
                    "Mouse",
                    "Tastatura"
                });
            cb1.Validating += new CancelEventHandler(cbProdus1_Validating);

            //sectiune textBoxCantitate
            tb1.Location = new Point(tbCantitate1.Location.X, tbCantitate1.Location.Y + numarLinii * distantaDintreLinii);
            tb1.Name = "tbCantitate" + (numarLinii + 1).ToString();
            tb1.Size = tbCantitate1.Size;
            tb1.TabIndex = tbCantitate1.TabIndex + 4 * numarLinii;

            //sectiune textBoxPret
            tb2.Location = new Point(tbPret1.Location.X, tbPret1.Location.Y + numarLinii * distantaDintreLinii);
            tb2.Name = "tbPret" + (numarLinii + 1).ToString();
            tb2.Size = tbPret1.Size;
            tb2.TabIndex = tbPret1.TabIndex + 4 * numarLinii;

            //sectiune textboxValoare
            tb3.Location = new Point(tbValoare1.Location.X, tbValoare1.Location.Y + numarLinii * distantaDintreLinii);
            tb3.Name = "tbValoare" + (numarLinii + 1).ToString();
            tb3.Size = tbValoare1.Size;
            tb3.Enabled = false;

            //sectiune button
            b2.Location = new Point(bSterge1.Location.X, bSterge1.Location.Y + numarLinii * distantaDintreLinii);
            b2.Name = "buttonSterge" + (numarLinii + 1).ToString();
            b2.Size = bSterge1.Size;
            b2.Text = bSterge1.Text;
            b2.Click += new EventHandler(bSterge1_Click);

            Controls.Add(tb1);
            Controls.Add(tb2);
            Controls.Add(tb3);
            Controls.Add(cb1);
            Controls.Add(b2);

            linie l1 = new linie();
            l1.produs = cb1;
            l1.cantitate = tb1;
            l1.pret = tb2;
            l1.valoare = tb3;
            l1.butonSterge = b2;

            lista.Add(l1);

            bLinieNoua.Location = new Point(bLinieNoua.Location.X, cbProdus1.Location.Y + distantaDintreLinii * numarLinii);
            numarLinii++;

        }

        private void bSterge1_Click(object sender, EventArgs e)
        {
            Button bCurent = (Button)sender;
            linie linieCurenta = (linie)lista.Find(m => ((linie)m).butonSterge == bCurent);
            int indexCurent = lista.IndexOf(linieCurenta);
            for (int i = indexCurent + 1; i < lista.Count; i++)
            {
                ((ComboBox)((linie)lista[i]).produs).Location = new Point(
                    ((ComboBox)((linie)lista[i]).produs).Location.X, 
                    ((ComboBox)((linie)lista[i]).produs).Location.Y - distantaDintreLinii
                    );
                ((TextBox)((linie)lista[i]).cantitate).Location = new Point(
                    ((TextBox)((linie)lista[i]).cantitate).Location.X,
                    ((TextBox)((linie)lista[i]).cantitate).Location.Y - distantaDintreLinii
                    );
                ((TextBox)((linie)lista[i]).pret).Location = new Point(
                    ((TextBox)((linie)lista[i]).pret).Location.X,
                    ((TextBox)((linie)lista[i]).pret).Location.Y - distantaDintreLinii
                    );
                ((TextBox)((linie)lista[i]).valoare).Location = new Point(
                    ((TextBox)((linie)lista[i]).valoare).Location.X,
                    ((TextBox)((linie)lista[i]).valoare).Location.Y - distantaDintreLinii
                    );
                ((Button)((linie)lista[i]).butonSterge).Location = new Point(
                    ((Button)((linie)lista[i]).butonSterge).Location.X,
                    ((Button)((linie)lista[i]).butonSterge).Location.Y - distantaDintreLinii
                    );

            }
            Controls.Remove((Button)linieCurenta.butonSterge);
            Controls.Remove((ComboBox)linieCurenta.produs);
            Controls.Remove((TextBox)linieCurenta.cantitate);
            Controls.Remove((TextBox)linieCurenta.pret);
            Controls.Remove((TextBox)linieCurenta.valoare);

            lista.Remove(linieCurenta);
            numarLinii--;

            bLinieNoua.Location = new Point(bLinieNoua.Location.X, bLinieNoua.Location.Y - distantaDintreLinii);
        }
    }

    public class linie
    {
        public object produs;
        public object cantitate;
        public object pret;
        public object valoare;
        public object butonSterge;
    }
}
