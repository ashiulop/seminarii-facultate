﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seminar_1
{
    class Vehicul:ICloneable
    {
        private string marca;
        private int anFabricatie;
        private float pret;
        private string[] optiuni;
        private float capCilindrica;
        private string[] proprietari;

        public Vehicul()
        {
            marca = "Nedefinita";
            anFabricatie = 0;
            pret = 0.0f;
            optiuni = new string[3];
            capCilindrica = 0;
            proprietari = new string[3];
        }

        public string getMarca()
        {
            return marca;
        }

        public void setMarca(string m)
        {
            marca = m;
        }

        public int AnFabricatie
        {
            get
            {
                return anFabricatie;
            }
            set
            {
                if(value > 2000 && value < 3000)
                    anFabricatie = value;
            }
        }

        public float Pret
        {
            get
            {
                return pret;
            }
            set
            {
                if (value > 0 && value < 50000)
                    pret = value;
            }
        }

        public float CapCilindrica
        {
            get
            {
                return capCilindrica;
            }
            set
            {
                if (value > 0 && value < 10000)
                    capCilindrica = value;
            }
        }

        public void ModificaOptiune(int i, string s)
        {
            optiuni[i] = s;
        }

        public void ModificaProprietar(int i, string s)
        {
            proprietari[i] = s;
        }

        public void Afisare()
        {
            Console.WriteLine("{0} fabricata in anul {1} are pretul {2} euro", marca, anFabricatie, pret);
            Console.WriteLine("{0}, {1}, {2}", optiuni[0], optiuni[1], optiuni[2]);
            Console.WriteLine("Capacitate cilindrica: {0}", capCilindrica);
            Console.WriteLine("{0}, {1}, {2}", proprietari[0], proprietari[1], proprietari[2]);
        }
        
        object ICloneable.Clone()
        {
            return this.MemberwiseClone(); // shallow copy
        }
        public Vehicul Clone()
        {
            Vehicul v = (Vehicul)((ICloneable)this).Clone();   //deep copy
            string[] optiuni2 = new string[3];
            for (int i = 0; i < 3; i++)
                optiuni2[i] = this.optiuni[i];
            v.optiuni = optiuni2;

            string[] proprietari2 = new string[3];
            for (int i = 0; i < 3; i++)
                proprietari2[i] = this.proprietari[i];
            v.proprietari = proprietari2;

            return v;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Vehicul v1 = new Vehicul();
            float a = v1.Pret;


            Vehicul v2 = new Vehicul();
            v2.setMarca("Dacia");
            v2.AnFabricatie = 2018;
            v2.Pret = 500.0f;

            Vehicul v3 = v2.Clone();

            v3.ModificaOptiune(0, "Trapa");

            Vehicul v4 = v2.Clone();
            v4.ModificaProprietar(0, "Andrei");

            v2.Afisare();
            v1.Afisare();
            v3.Afisare();
            v4.Afisare();

            Console.ReadLine();
        }
    }
}
