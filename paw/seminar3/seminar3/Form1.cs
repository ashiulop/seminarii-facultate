﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace seminar3
{
    public partial class Form1 : Form
    {
        List<Persoana> listaPersoane = new List<Persoana>();
        public Form1()
        {
            InitializeComponent();
        }

        private void labelPrenume_Click(object sender, EventArgs e)
        {

        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            Persoana p = new Persoana();
            p.nume = textBoxNume.Text;
            p.prenume = textBoxPrenume.Text;

            bool adaugaok = true;

            try
            {
                p.varsta = Convert.ToInt32(textBoxVarsta.Text);
            }
            catch
            {
                MessageBox.Show("Eroare fam", "Mesaj", MessageBoxButtons.OK);
                adaugaok = false;
            }
            if (adaugaok)
            {
                listBoxPersoane.Items.Add(p.ToString());
                listaPersoane.Add(p);
            }
            
        }

        private void textBoxNume_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonAdaugaInLV_Click(object sender, EventArgs e)
        {
            foreach(Persoana p in listaPersoane)
            {
                ListViewItem lvi = new ListViewItem(p.nume);
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.prenume));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.varsta.ToString()));

                lvi.Tag = p;

                listViewPersoane.Items.Add(lvi);
            }
        }
    }
}
