﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seminar3
{
    class Persoana
    {
        public string nume;
        public string prenume;
        public int varsta;

        public Persoana()
        {

        }

        public override string ToString()
        {
            return nume + " " + prenume + " " + varsta;
        }
    }
}
