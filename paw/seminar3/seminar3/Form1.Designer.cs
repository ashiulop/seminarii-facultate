﻿namespace seminar3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNume = new System.Windows.Forms.Label();
            this.labelPrenume = new System.Windows.Forms.Label();
            this.labelVarsta = new System.Windows.Forms.Label();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.textBoxVarsta = new System.Windows.Forms.TextBox();
            this.listBoxPersoane = new System.Windows.Forms.ListBox();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.listViewPersoane = new System.Windows.Forms.ListView();
            this.buttonAdaugaInLV = new System.Windows.Forms.Button();
            this.columnHeaderNume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPrenume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderVarsta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(71, 35);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(36, 15);
            this.labelNume.TabIndex = 0;
            this.labelNume.Text = "Nume";
            // 
            // labelPrenume
            // 
            this.labelPrenume.AutoSize = true;
            this.labelPrenume.Location = new System.Drawing.Point(71, 94);
            this.labelPrenume.Name = "labelPrenume";
            this.labelPrenume.Size = new System.Drawing.Size(50, 15);
            this.labelPrenume.TabIndex = 1;
            this.labelPrenume.Text = "Prenume";
            this.labelPrenume.Click += new System.EventHandler(this.labelPrenume_Click);
            // 
            // labelVarsta
            // 
            this.labelVarsta.AutoSize = true;
            this.labelVarsta.Location = new System.Drawing.Point(74, 153);
            this.labelVarsta.Name = "labelVarsta";
            this.labelVarsta.Size = new System.Drawing.Size(40, 15);
            this.labelVarsta.TabIndex = 2;
            this.labelVarsta.Text = "Varsta";
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(177, 35);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(137, 23);
            this.textBoxNume.TabIndex = 3;
            this.textBoxNume.TextChanged += new System.EventHandler(this.textBoxNume_TextChanged);
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Location = new System.Drawing.Point(177, 94);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(137, 23);
            this.textBoxPrenume.TabIndex = 4;
            // 
            // textBoxVarsta
            // 
            this.textBoxVarsta.Location = new System.Drawing.Point(177, 153);
            this.textBoxVarsta.Name = "textBoxVarsta";
            this.textBoxVarsta.Size = new System.Drawing.Size(137, 23);
            this.textBoxVarsta.TabIndex = 5;
            // 
            // listBoxPersoane
            // 
            this.listBoxPersoane.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listBoxPersoane.FormattingEnabled = true;
            this.listBoxPersoane.ItemHeight = 15;
            this.listBoxPersoane.Location = new System.Drawing.Point(557, 35);
            this.listBoxPersoane.Name = "listBoxPersoane";
            this.listBoxPersoane.Size = new System.Drawing.Size(223, 139);
            this.listBoxPersoane.TabIndex = 6;
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.Location = new System.Drawing.Point(177, 208);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(137, 23);
            this.buttonAdauga.TabIndex = 7;
            this.buttonAdauga.Text = "Adauga";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // listViewPersoane
            // 
            this.listViewPersoane.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderNume,
            this.columnHeaderPrenume,
            this.columnHeaderVarsta});
            this.listViewPersoane.HideSelection = false;
            this.listViewPersoane.Location = new System.Drawing.Point(557, 230);
            this.listViewPersoane.Name = "listViewPersoane";
            this.listViewPersoane.Size = new System.Drawing.Size(223, 161);
            this.listViewPersoane.TabIndex = 8;
            this.listViewPersoane.UseCompatibleStateImageBehavior = false;
            this.listViewPersoane.View = System.Windows.Forms.View.Details;
            // 
            // buttonAdaugaInLV
            // 
            this.buttonAdaugaInLV.Location = new System.Drawing.Point(439, 300);
            this.buttonAdaugaInLV.Name = "buttonAdaugaInLV";
            this.buttonAdaugaInLV.Size = new System.Drawing.Size(112, 23);
            this.buttonAdaugaInLV.TabIndex = 9;
            this.buttonAdaugaInLV.Text = "Adauga in LV";
            this.buttonAdaugaInLV.UseVisualStyleBackColor = true;
            this.buttonAdaugaInLV.Click += new System.EventHandler(this.buttonAdaugaInLV_Click);
            // 
            // columnHeaderNume
            // 
            this.columnHeaderNume.Text = "Nume";
            // 
            // columnHeaderPrenume
            // 
            this.columnHeaderPrenume.Text = "Prenume";
            // 
            // columnHeaderVarsta
            // 
            this.columnHeaderVarsta.Text = "Varsta";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.buttonAdaugaInLV);
            this.Controls.Add(this.listViewPersoane);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.listBoxPersoane);
            this.Controls.Add(this.textBoxVarsta);
            this.Controls.Add(this.textBoxPrenume);
            this.Controls.Add(this.textBoxNume);
            this.Controls.Add(this.labelVarsta);
            this.Controls.Add(this.labelPrenume);
            this.Controls.Add(this.labelNume);
            this.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.Label labelPrenume;
        private System.Windows.Forms.Label labelVarsta;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.TextBox textBoxVarsta;
        private System.Windows.Forms.ListBox listBoxPersoane;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.ListView listViewPersoane;
        private System.Windows.Forms.Button buttonAdaugaInLV;
        private System.Windows.Forms.ColumnHeader columnHeaderNume;
        private System.Windows.Forms.ColumnHeader columnHeaderPrenume;
        private System.Windows.Forms.ColumnHeader columnHeaderVarsta;
    }
}

