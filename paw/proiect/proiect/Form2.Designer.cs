﻿namespace proiect
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNumeClient = new System.Windows.Forms.TextBox();
            this.textBoxPrenumeClient = new System.Windows.Forms.TextBox();
            this.textBoxCnpClient = new System.Windows.Forms.TextBox();
            this.dateTimePickerClient = new System.Windows.Forms.DateTimePicker();
            this.labelNumeClient = new System.Windows.Forms.Label();
            this.labelPrenumeClient = new System.Windows.Forms.Label();
            this.labelCnpClient = new System.Windows.Forms.Label();
            this.labelDataClient = new System.Windows.Forms.Label();
            this.buttonAdaugaClient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxNumeClient
            // 
            this.textBoxNumeClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumeClient.Location = new System.Drawing.Point(56, 70);
            this.textBoxNumeClient.Name = "textBoxNumeClient";
            this.textBoxNumeClient.Size = new System.Drawing.Size(131, 26);
            this.textBoxNumeClient.TabIndex = 0;
            // 
            // textBoxPrenumeClient
            // 
            this.textBoxPrenumeClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrenumeClient.Location = new System.Drawing.Point(300, 70);
            this.textBoxPrenumeClient.Name = "textBoxPrenumeClient";
            this.textBoxPrenumeClient.Size = new System.Drawing.Size(131, 26);
            this.textBoxPrenumeClient.TabIndex = 1;
            // 
            // textBoxCnpClient
            // 
            this.textBoxCnpClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCnpClient.Location = new System.Drawing.Point(56, 171);
            this.textBoxCnpClient.Name = "textBoxCnpClient";
            this.textBoxCnpClient.Size = new System.Drawing.Size(131, 26);
            this.textBoxCnpClient.TabIndex = 2;
            // 
            // dateTimePickerClient
            // 
            this.dateTimePickerClient.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerClient.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerClient.Location = new System.Drawing.Point(300, 171);
            this.dateTimePickerClient.Name = "dateTimePickerClient";
            this.dateTimePickerClient.Size = new System.Drawing.Size(131, 26);
            this.dateTimePickerClient.TabIndex = 3;
            // 
            // labelNumeClient
            // 
            this.labelNumeClient.AutoSize = true;
            this.labelNumeClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumeClient.Location = new System.Drawing.Point(53, 44);
            this.labelNumeClient.Name = "labelNumeClient";
            this.labelNumeClient.Size = new System.Drawing.Size(48, 18);
            this.labelNumeClient.TabIndex = 4;
            this.labelNumeClient.Text = "Nume";
            // 
            // labelPrenumeClient
            // 
            this.labelPrenumeClient.AutoSize = true;
            this.labelPrenumeClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrenumeClient.Location = new System.Drawing.Point(297, 44);
            this.labelPrenumeClient.Name = "labelPrenumeClient";
            this.labelPrenumeClient.Size = new System.Drawing.Size(68, 18);
            this.labelPrenumeClient.TabIndex = 5;
            this.labelPrenumeClient.Text = "Prenume";
            // 
            // labelCnpClient
            // 
            this.labelCnpClient.AutoSize = true;
            this.labelCnpClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCnpClient.Location = new System.Drawing.Point(53, 148);
            this.labelCnpClient.Name = "labelCnpClient";
            this.labelCnpClient.Size = new System.Drawing.Size(40, 18);
            this.labelCnpClient.TabIndex = 6;
            this.labelCnpClient.Text = "CNP";
            // 
            // labelDataClient
            // 
            this.labelDataClient.AutoSize = true;
            this.labelDataClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataClient.Location = new System.Drawing.Point(297, 148);
            this.labelDataClient.Name = "labelDataClient";
            this.labelDataClient.Size = new System.Drawing.Size(93, 18);
            this.labelDataClient.TabIndex = 7;
            this.labelDataClient.Text = "Data Nasterii";
            // 
            // buttonAdaugaClient
            // 
            this.buttonAdaugaClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdaugaClient.Location = new System.Drawing.Point(187, 239);
            this.buttonAdaugaClient.Name = "buttonAdaugaClient";
            this.buttonAdaugaClient.Size = new System.Drawing.Size(110, 33);
            this.buttonAdaugaClient.TabIndex = 8;
            this.buttonAdaugaClient.Text = "Adauga Client";
            this.buttonAdaugaClient.UseVisualStyleBackColor = true;
            this.buttonAdaugaClient.Click += new System.EventHandler(this.buttonAdaugaClient_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 333);
            this.Controls.Add(this.buttonAdaugaClient);
            this.Controls.Add(this.labelDataClient);
            this.Controls.Add(this.labelCnpClient);
            this.Controls.Add(this.labelPrenumeClient);
            this.Controls.Add(this.labelNumeClient);
            this.Controls.Add(this.dateTimePickerClient);
            this.Controls.Add(this.textBoxCnpClient);
            this.Controls.Add(this.textBoxPrenumeClient);
            this.Controls.Add(this.textBoxNumeClient);
            this.Name = "Form2";
            this.Text = "Adauga Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumeClient;
        private System.Windows.Forms.TextBox textBoxPrenumeClient;
        private System.Windows.Forms.TextBox textBoxCnpClient;
        private System.Windows.Forms.DateTimePicker dateTimePickerClient;
        private System.Windows.Forms.Label labelNumeClient;
        private System.Windows.Forms.Label labelPrenumeClient;
        private System.Windows.Forms.Label labelCnpClient;
        private System.Windows.Forms.Label labelDataClient;
        private System.Windows.Forms.Button buttonAdaugaClient;
    }
}