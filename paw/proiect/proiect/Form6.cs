﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace proiect
{
    public partial class Form6 : Form
    {
        string connectionString;
        List<Client> listac;
        public Form6(List<Client> lparam)
        {
            InitializeComponent();
            connectionString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = clienti.accdb";
            listac = lparam;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OleDbConnection conexiune = new OleDbConnection(connectionString);
            try
            {
                listView1.Items.Clear();
                listac.Clear();
                conexiune.Open();
                OleDbCommand comanda = new OleDbCommand();
                comanda.Connection = conexiune;
                comanda.CommandText = "SELECT * FROM clienti";

                OleDbDataReader reader = comanda.ExecuteReader();
                while (reader.Read())
                {
                    ListViewItem lvi = new ListViewItem(reader["ID"].ToString());
                    lvi.SubItems.Add(reader["Nume"].ToString());
                    lvi.SubItems.Add(reader["Prenume"].ToString());
                    lvi.SubItems.Add(reader["dataNasterii"].ToString());
                    lvi.SubItems.Add(reader["Cnp"].ToString());
                    listView1.Items.Add(lvi);

                    Client c = new Client();
                    c.nume = reader["Nume"].ToString();
                    c.prenume = reader["Prenume"].ToString();
                    c.dataNasterii = DateTime.ParseExact(reader["dataNasterii"].ToString(), "dd/MM/yyyy", null);
                    c.cnp = Convert.ToInt32(reader["Cnp"].ToString());

                    listac.Add(c);
                }


            }
            catch(OleDbException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexiune.Close();
            }
        }
    }
}
