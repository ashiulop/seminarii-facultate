﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect
{
    public partial class FormRezervari : Form
    {
        List<Rezervare> listaRezervari = new List<Rezervare>();
        List<string> listaAparate = new List<string>();
        List<linie> listaLinii = new List<linie>();
        List<Client> listaClienti = new List<Client>();

        int numarlinii = 1;
        int distantaDintreLinii = 33;

        bool numeOK;
        bool prenumeOK;
        bool oraOK;
        bool aparatOK;

        public FormRezervari()
        {
            InitializeComponent();

            linie l0 = new linie();
            l0.aparat = comboBoxAparat1;
            l0.butonSterge = buttonSterge1;
            l0.labelLinie = labelAparat1;

            listaLinii.Add(l0);

            dateTimePickerData.Format = DateTimePickerFormat.Custom;
            dateTimePickerData.CustomFormat = "dd/MM/yyyy";
            dateTimePickerData.MinDate = DateTime.Today;

            listaRezervari.Add(new Rezervare("Popescu", "Dorel", DateTime.Today.ToString("dd/MM/yyyy"), "12:00"));
            listaRezervari.Add(new Rezervare("Georgescu", "Ionel", DateTime.Today.ToString("dd/MM/yyyy"), "14:30"));
            listaRezervari.Add(new Rezervare("Ionescu", "Marcel", DateTime.Today.ToString("dd/MM/yyyy"), "14:30"));

            listaClienti.Add(new Client("Popescu", "Dorel", DateTime.ParseExact("16/05/1993", "dd/MM/yyyy", CultureInfo.InvariantCulture), 29412));
            listaClienti.Add(new Client("Georgescu", "Ionel", DateTime.ParseExact("04/11/1989", "dd/MM/yyyy", CultureInfo.InvariantCulture), 28232));
            listaClienti.Add(new Client("Ionescu", "Marcel", DateTime.ParseExact("17/12/1998", "dd/MM/yyyy", CultureInfo.InvariantCulture), 25193));

            comboBoxOra.Items.AddRange(new object[]
            {
                "12:00",
                "12:30",
                "13:00",
                "13:30",
                "14:00",
                "14:30",
                "15:00",
                "15:30",
                "16:00",
                "16:30",
                "17:00",
                "17:30",
                "18:00",
                "18:30"
            });
            comboBoxAparat1.Items.AddRange(new object[]
            {
                "Banda alergare",
                "Bicicleta",
                "Haltere orizontal",
                "Haltere inclinat sus",
                "Haltere inclinat jos",
                "Gantere",
                "Aparat extensii spate"
            });

            foreach (Rezervare rez in listaRezervari)
            {
                ListViewItem lvi = new ListViewItem(rez.nume);
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.prenume));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.data.ToString()));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.ora));

                lvi.Tag = rez;

                listViewRezervari.Items.Add(lvi);

            }
        }

        private void buttonRezervare_Click(object sender, EventArgs e)
        {
            
            Rezervare r = new Rezervare();


            //for (int i = 0; i < numarlinii; i++)
            //{
            //    listaAparate.Add(comboBoxAparat1.Text);
            //}

            listaAparate.Clear();

            foreach(linie l in listaLinii)
            {
                listaAparate.Add(((ComboBox)l.aparat).Text);
            }

            r.nume = textBoxNume.Text;
            r.prenume = textBoxPrenume.Text;
            r.data = dateTimePickerData.Value.ToString("dd/MM/yyyy");
            r.ora = comboBoxOra.Text;

            foreach (string a in listaAparate)
            {
                r.listaAparate.Add(a);
            }

            

            if(numeOK && prenumeOK && oraOK && aparatOK)
            {
                if (clientInregistrat(r)) {
                    listaRezervari.Add(r);

                    ListViewItem lvi = new ListViewItem(r.nume);
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, r.prenume));
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, r.data.ToString()));
                    lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, r.ora));

                    lvi.Tag = r;

                    listViewRezervari.Items.Add(lvi);

                    MessageBox.Show("Rezervare efectuata cu succes!");
                }
                else
                {
                    MessageBox.Show("Clientul nu este inregistrat!");
                }
                
            }
            else
            {
                MessageBox.Show("Completati toate campurile!");
            }
            
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            ComboBox cb = new ComboBox();
            Button b = new Button();
            Label label = new Label();

            cb.Location = new Point(comboBoxAparat1.Location.X, comboBoxAparat1.Location.Y + numarlinii * distantaDintreLinii);
            cb.Name = "comboBoxAparat" + (numarlinii + 1).ToString();
            cb.Size = comboBoxAparat1.Size;
            cb.Font = comboBoxAparat1.Font;
            cb.TabIndex = comboBoxAparat1.TabIndex + 2 * numarlinii;
            cb.Items.AddRange(new object[]
            {
                "Banda alergare",
                "Bicicleta",
                "Haltere orizontal",
                "Haltere inclinat sus",
                "Haltere inclinat jos",
                "Gantere",
                "Aparat extensii spate"
            });
            cb.Validating += new CancelEventHandler(comboBoxAparat1_Validating);

            b.Location = new Point(buttonSterge1.Location.X, buttonSterge1.Location.Y + numarlinii * distantaDintreLinii);
            b.Name = "buttonSterge" + (numarlinii + 1).ToString();
            b.Size = buttonSterge1.Size;
            b.Font = buttonSterge1.Font;
            b.Text = buttonSterge1.Text;
            b.Click += new EventHandler(buttonSterge1_Click);

            label.Location = new Point(labelAparat1.Location.X, labelAparat1.Location.Y + numarlinii * distantaDintreLinii);
            label.Name = "labelAparat" + (numarlinii + 1).ToString();
            label.Font = labelAparat1.Font;
            label.Text = labelAparat1.Text;


            Controls.Add(cb);
            Controls.Add(b);
            Controls.Add(label);

            linie l = new linie();
            l.aparat = cb;
            l.butonSterge = b;
            l.labelLinie = label;
            listaLinii.Add(l);

            buttonAdauga.Location = new Point(buttonAdauga.Location.X, comboBoxAparat1.Location.Y + numarlinii * distantaDintreLinii);
            numarlinii++;

        }

        private void buttonSterge1_Click(object sender, EventArgs e)
        {
            Button bCurent = (Button)sender;
            linie linieCurenta = (linie)listaLinii.Find(m => ((linie)m).butonSterge == bCurent);
            int indexCurent = listaLinii.IndexOf(linieCurenta);

            for (int i = indexCurent + 1; i < listaLinii.Count; i++)
            {
                ((ComboBox)((linie)listaLinii[i]).aparat).Location = new Point(
                    ((ComboBox)((linie)listaLinii[i]).aparat).Location.X,
                    ((ComboBox)((linie)listaLinii[i]).aparat).Location.Y - distantaDintreLinii
                    );
                ((Button)((linie)listaLinii[i]).butonSterge).Location = new Point(
                    ((Button)((linie)listaLinii[i]).butonSterge).Location.X,
                    ((Button)((linie)listaLinii[i]).butonSterge).Location.Y - distantaDintreLinii
                    );
                ((Label)((linie)listaLinii[i]).labelLinie).Location = new Point(
                    ((Label)((linie)listaLinii[i]).labelLinie).Location.X,
                    ((Label)((linie)listaLinii[i]).labelLinie).Location.Y - distantaDintreLinii
                    );

               
            }
            Controls.Remove((ComboBox)linieCurenta.aparat);
            Controls.Remove((Button)linieCurenta.butonSterge);
            Controls.Remove((Label)linieCurenta.labelLinie);

            listaLinii.Remove(linieCurenta);
            numarlinii--;

            buttonAdauga.Location = new Point(buttonAdauga.Location.X, buttonAdauga.Location.Y - distantaDintreLinii);
        }

        private void textBoxNume_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            if(tbLocal.Text == "")
            {
                errorProvider1.SetError(tbLocal, "Introduceti numele!");
                numeOK = false;
            }
            else
            {
                errorProvider1.SetError(tbLocal, "");
                numeOK = true;
            }
        }

        private void textBoxPrenume_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            if (tbLocal.Text == "")
            {
                errorProvider1.SetError(tbLocal, "Introduceti prenumele!");
                prenumeOK = false;
            }
            else
            {
                errorProvider1.SetError(tbLocal, "");
                prenumeOK = true;
            }
        }

        private void comboBoxOra_Validating(object sender, CancelEventArgs e)
        {
            ComboBox cbLocal = (ComboBox)sender;
            if (cbLocal.SelectedIndex < 0)
            {
                errorProvider1.SetError(cbLocal, "Selectati ora!");
                oraOK = false;
            }
            else
            {
                errorProvider1.SetError(cbLocal, "");
                oraOK = true;
            }
        }

        private void comboBoxAparat1_Validating(object sender, CancelEventArgs e)
        {
            ComboBox cbLocal = (ComboBox)sender;
            if (cbLocal.SelectedIndex < 0)
            {
                errorProvider1.SetError(cbLocal, "Selectati aparatul!");
                aparatOK = false;
            }
            else
            {
                errorProvider1.SetError(cbLocal, "");
                aparatOK = true;
            }
        }

        private void buttonClient_Click(object sender, EventArgs e)
        {
            Client r = new Client();
            r.nume = textBoxNume.Text;
            r.prenume = textBoxPrenume.Text;
            Form2 client = new Form2(r);
            client.ShowDialog();
            if (client.DialogResult == DialogResult.OK)
            {
                listaClienti.Add(r);
                //foreach (Client c in listaClienti)
                //{
                //    MessageBox.Show(c.ToString());
                //}
            }

        }


        private void stergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = listaRezervari.Count - 1; i >= 0; i--)
                if (listaRezervari.ElementAt(i) == listViewRezervari.SelectedItems[0].Tag)
                    listaRezervari.RemoveAt(i);

            listViewRezervari.SelectedItems[0].Remove();
            
            
        }

        private void listViewRezervari_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewRezervari.SelectedItems.Count > 0)
            {
                contextMenuStrip1.Items[0].Enabled = true;
                contextMenuStrip1.Items[1].Enabled = true;
            }
            else
            {
                contextMenuStrip1.Items[0].Enabled = false;
                contextMenuStrip1.Items[1].Enabled = false;
            }
        }

        private void modificaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rezervare cform1 = (Rezervare)listViewRezervari.SelectedItems[0].Tag;
            string nume = cform1.nume;
            string prenume = cform1.prenume;
            Form3 f3 = new Form3(cform1);
            f3.ShowDialog();
            if(f3.DialogResult == DialogResult.OK)
            {
                ListViewItem lvi = listViewRezervari.SelectedItems[0];
                lvi.Text = cform1.nume;
                lvi.SubItems[1].Text = cform1.prenume;
                lvi.SubItems[2].Text = cform1.data;
                lvi.SubItems[3].Text = cform1.ora;

                foreach(Rezervare r in listaRezervari)
                {
                    if(r.nume == nume && r.prenume == prenume)
                    {
                        r.nume = cform1.nume;
                        r.prenume = cform1.prenume;
                        r.data = cform1.data;
                        r.ora = cform1.ora;
                    }
                }
            }
        }

        private bool clientInregistrat(Rezervare r)
        {
            foreach(Client c in listaClienti)
            {
                if (c.nume == r.nume && c.prenume == r.prenume)
                    return true;

            }
            return false;
        }

        private void incarcaInFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream fis = new FileStream("../../rezervari.dat", FileMode.Create);
            BinaryFormatter f = new BinaryFormatter();
            f.Serialize(fis, listaRezervari);
            fis.Close();
            MessageBox.Show("Salvare efectuata cu succes!");
        }

        private void incarcaDinFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream fis = new FileStream("../../rezervari.dat", FileMode.Open);
            BinaryFormatter f = new BinaryFormatter();
            listaRezervari = (List<Rezervare>)f.Deserialize(fis);

            listViewRezervari.Items.Clear();
            foreach (Rezervare rez in listaRezervari)
            {
                ListViewItem lvi = new ListViewItem(rez.nume);
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.prenume));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.data.ToString()));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, rez.ora));

                lvi.Tag = rez;

                listViewRezervari.Items.Add(lvi);

            }
            MessageBox.Show("Incarcare efectuata cu succes!");
            fis.Close();
        }

        private void salveazaClientiInFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream fis = new FileStream("../../clienti.dat", FileMode.Create);
            BinaryFormatter f = new BinaryFormatter();
            f.Serialize(fis, listaClienti);
            fis.Close();
            MessageBox.Show("Salvare efectuata cu succes!");
        }

        private void incarcaClientiDinFisierBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream fis = new FileStream("../../clienti.dat", FileMode.Open);
            BinaryFormatter f = new BinaryFormatter();
            listaClienti = (List<Client>)f.Deserialize(fis);
            MessageBox.Show("Incarcare efectuata cu succes!");
            fis.Close();
        }

        private void buttonVeziClienti_Click(object sender, EventArgs e)
        {
            Form4 cForm4 = new Form4(listaClienti);
            cForm4.Show();

        }

        private void buttonStatistici_Click(object sender, EventArgs e)
        {
            Form5 f5 = new Form5(listaRezervari);
            f5.Show();
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            e.PageSettings.PaperSize = new PaperSize("A4", 850, 1100);
            float pageWidth = e.PageSettings.PrintableArea.Width;
            float pageHeight = e.PageSettings.PrintableArea.Height;

            int offset = 0;
            g.DrawString("Rezervari: ", buttonAdauga.Font, new SolidBrush(Color.Black), e.MarginBounds.X, e.MarginBounds.Y);
            int dim = 70, nr = 1, curr = 0;
            for(int i = curr; i < listaRezervari.Count; i++)
            { 
                g.DrawString(listaRezervari.ElementAt(i).ToString(), Font, new SolidBrush(Color.Black), e.MarginBounds.X, e.MarginBounds.Y + nr * dim);
                nr++;
                offset += dim;

                if (offset >= pageHeight)
                {
                    e.HasMorePages = true;
                    offset = 0;
                    curr = i;
                    return;
                    
                }
                else
                {
                    e.HasMorePages = false;
                }

            }
        }

        private void buttonBd_Click(object sender, EventArgs e)
        {
            Form6 f6 = new Form6(listaClienti);
            f6.Show();
        }
    }

    public class linie
    {
        public object aparat;
        public object butonSterge;
        public object labelLinie;
    }
}
