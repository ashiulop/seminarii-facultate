﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect
{
    public partial class Form3 : Form
    {
        Rezervare r;
        public Form3(Rezervare rParam)
        {
            InitializeComponent();
            r = rParam;

            textBoxNume.Text = r.nume;
            textBoxPrenume.Text = r.prenume;
            dateTimePickerData.Value = DateTime.ParseExact(r.data, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            comboBoxOra.Text = r.ora;
            comboBoxOra.Items.AddRange(new object[]
            {
                "12:00",
                "12:30",
                "13:00",
                "13:30",
                "14:00",
                "14:30",
                "15:00",
                "15:30",
                "16:00",
                "16:30",
                "17:00",
                "17:30",
                "18:00",
                "18:30"
            });
        }

        private void buttonModifica_Click(object sender, EventArgs e)
        {
            r.nume = textBoxNume.Text;
            r.prenume = textBoxPrenume.Text;
            r.data = dateTimePickerData.Value.ToString("dd/MM/yyyy");
            r.ora = comboBoxOra.Text;
            DialogResult = DialogResult.OK;

            if(r.nume == "" && r.prenume == "" && r.ora == "")
            {
                MessageBox.Show("Eroare!");
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
