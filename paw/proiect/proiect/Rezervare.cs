﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proiect
{
   [Serializable]
    public class Rezervare
    {
        public string nume;
        public string prenume;
        public string data;
        public string ora;
        public List<string> listaAparate = new List<string>();

        public Rezervare()
        {

        }

        public Rezervare(string nume, string prenume, string data, string ora)
        {
            this.nume = nume;
            this.prenume = prenume;
            this.data = data;
            this.ora = ora;
            this.listaAparate.Add("banda alergare");
            this.listaAparate.Add("gantere");
        }

        public override string ToString()
        {
            string result = "Client: " + this.nume + " " + this.prenume + "\n";
            result += "    Data: " + this.data + "\n";
            result += "    Ora: " + this.ora + "\n";
            result += "    Aparate: ";

            foreach(string s in this.listaAparate)
            {
                if (s == this.listaAparate.ElementAt(this.listaAparate.Count - 1))
                    result += s + "\n\n";
                else
                    result += s + ", ";
            }

            return result;
        }
    }
}
