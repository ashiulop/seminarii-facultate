﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect
{

    public partial class Form5 : Form
    {
        List<Rezervare> listaLocal;
        string[] x;
        int[] y = new int[] { 0, 0, 0, 0, 0, 0, 0 };

        int nrobs = 7;
        public Form5(List<Rezervare> param)
        {
            InitializeComponent();
            listaLocal = param;

            x = new string[]
            {
                "12:00 - 13:00",
                "13:00 - 14:00",
                "14:00 - 15:00",
                "15:00 - 16:00",
                "16:00 - 17:00",
                "17:00 - 18:00",
                "18:00 - 19:00",
            };

            foreach(Rezervare r in listaLocal)
            {
                if (r.ora == "12:00" || r.ora == "12:30")
                    y[0]++;
                if (r.ora == "13:00" || r.ora == "13:30")
                    y[1]++;
                if (r.ora == "14:00" || r.ora == "14:30")
                    y[2]++;
                if (r.ora == "15:00" || r.ora == "15:30")
                    y[3]++;
                if (r.ora == "16:00" || r.ora == "16:30")
                    y[4]++;
                if (r.ora == "17:00" || r.ora == "17:30")
                    y[5]++;
                if (r.ora == "18:00" || r.ora == "18:30")
                    y[6]++;

            }
            this.ResizeRedraw = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Rectangle zonaClient = e.ClipRectangle;
            Brush fundal = new SolidBrush(Color.White);

            g.FillRectangle(fundal, zonaClient);

            zonaClient.X += 20;
            zonaClient.Y += 20;
            zonaClient.Height -= 40;
            zonaClient.Width -= 40;

            int i, vl = zonaClient.Left, vb = zonaClient.Bottom, vr = zonaClient.Right, vt = zonaClient.Top;

            Pen creionRosu = new Pen(Color.Red, 2);
            g.DrawRectangle(creionRosu, zonaClient);

            int latime, distanta;
            float raport_distanta_latime = 0.2f, max;

            SolidBrush[] pensule = new SolidBrush[]
            {
                new SolidBrush(Color.Plum),
                new SolidBrush(Color.SeaGreen),
                new SolidBrush(Color.SlateGray),
                new SolidBrush(Color.Turquoise),
                new SolidBrush(Color.LightPink),
                new SolidBrush(Color.Purple),
                new SolidBrush(Color.Turquoise)
            };

            Pen[] creioane = new Pen[]
            {
                new Pen(Color.Coral),
                new Pen(Color.Goldenrod),
                new Pen(Color.Linen),
                new Pen(Color.PowderBlue),
                new Pen(Color.Navy),
                new Pen(Color.LawnGreen),
                new Pen(Color.RoyalBlue)
            };

            SolidBrush pnsCrt;
            Pen penCrt;

            latime = (vr - vl) / (int)((nrobs + 1) * raport_distanta_latime + nrobs);
            distanta = (int)(latime * raport_distanta_latime);

            for (max = y[0], i = 1; i < nrobs; i++)
            {
                if (max < y[i])
                {
                    max = y[i];
                }
            }

            penCrt = creioane[0];
            pnsCrt = pensule[3];

            g.DrawLine(penCrt, vl, vt, vl, vb);
            g.DrawLine(penCrt, vl, vb, vr, vb);

            penCrt = creioane[1];
            for (i = 0; i < nrobs; i++)
            {
                pnsCrt = pensule[i % 7];
                PointF pnt = new PointF(vl + distanta + (latime + distanta) * i, vb - y[i] * (vb - vt) / max);
                SizeF sz = new SizeF(latime, y[i] * (vb - vt) / max);

                g.FillRectangle(pnsCrt, new RectangleF(pnt, sz));
                string denumire = x[i].ToString();
                g.DrawString(denumire, Font, pnsCrt, vl + distanta + latime / 10 + i * (latime + distanta), vb + 5);
            }
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            panel1.Invalidate();
        }
    }
}
