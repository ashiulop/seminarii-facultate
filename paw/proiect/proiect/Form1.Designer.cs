﻿namespace proiect
{
    partial class FormRezervari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRezervari));
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.labelNume = new System.Windows.Forms.Label();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.labelPrenume = new System.Windows.Forms.Label();
            this.labelData = new System.Windows.Forms.Label();
            this.dateTimePickerData = new System.Windows.Forms.DateTimePicker();
            this.labelOra = new System.Windows.Forms.Label();
            this.comboBoxOra = new System.Windows.Forms.ComboBox();
            this.listViewRezervari = new System.Windows.Forms.ListView();
            this.columnHeaderNume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPrenume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderOra = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modificaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonRezervare = new System.Windows.Forms.Button();
            this.labelAparat1 = new System.Windows.Forms.Label();
            this.comboBoxAparat1 = new System.Windows.Forms.ComboBox();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.buttonSterge1 = new System.Windows.Forms.Button();
            this.buttonClient = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fisiereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaInFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaDinFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaClientiInFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaClientiDinFisierBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelLvRezervari = new System.Windows.Forms.Label();
            this.buttonVeziClienti = new System.Windows.Forms.Button();
            this.buttonStatistici = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.buttonBd = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNume
            // 
            this.textBoxNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNume.Location = new System.Drawing.Point(161, 46);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(117, 26);
            this.textBoxNume.TabIndex = 0;
            this.textBoxNume.TabStop = false;
            this.textBoxNume.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxNume_Validating);
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNume.Location = new System.Drawing.Point(62, 50);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(48, 18);
            this.labelNume.TabIndex = 1;
            this.labelNume.Text = "Nume";
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrenume.Location = new System.Drawing.Point(161, 92);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(117, 26);
            this.textBoxPrenume.TabIndex = 2;
            this.textBoxPrenume.TabStop = false;
            this.textBoxPrenume.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxPrenume_Validating);
            // 
            // labelPrenume
            // 
            this.labelPrenume.AutoSize = true;
            this.labelPrenume.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelPrenume.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrenume.Location = new System.Drawing.Point(62, 96);
            this.labelPrenume.Name = "labelPrenume";
            this.labelPrenume.Size = new System.Drawing.Size(68, 18);
            this.labelPrenume.TabIndex = 3;
            this.labelPrenume.Text = "Prenume";
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelData.Location = new System.Drawing.Point(62, 142);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(39, 18);
            this.labelData.TabIndex = 4;
            this.labelData.Text = "Data";
            // 
            // dateTimePickerData
            // 
            this.dateTimePickerData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerData.Location = new System.Drawing.Point(161, 136);
            this.dateTimePickerData.Name = "dateTimePickerData";
            this.dateTimePickerData.Size = new System.Drawing.Size(117, 26);
            this.dateTimePickerData.TabIndex = 5;
            this.dateTimePickerData.TabStop = false;
            this.dateTimePickerData.Value = new System.DateTime(2020, 4, 22, 12, 54, 2, 0);
            // 
            // labelOra
            // 
            this.labelOra.AutoSize = true;
            this.labelOra.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOra.Location = new System.Drawing.Point(62, 184);
            this.labelOra.Name = "labelOra";
            this.labelOra.Size = new System.Drawing.Size(33, 18);
            this.labelOra.TabIndex = 7;
            this.labelOra.Text = "Ora";
            // 
            // comboBoxOra
            // 
            this.comboBoxOra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOra.FormattingEnabled = true;
            this.comboBoxOra.Location = new System.Drawing.Point(161, 180);
            this.comboBoxOra.Name = "comboBoxOra";
            this.comboBoxOra.Size = new System.Drawing.Size(117, 28);
            this.comboBoxOra.TabIndex = 8;
            this.comboBoxOra.TabStop = false;
            this.comboBoxOra.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxOra_Validating);
            // 
            // listViewRezervari
            // 
            this.listViewRezervari.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderNume,
            this.columnHeaderPrenume,
            this.columnHeaderData,
            this.columnHeaderOra});
            this.listViewRezervari.ContextMenuStrip = this.contextMenuStrip1;
            this.listViewRezervari.HideSelection = false;
            this.listViewRezervari.Location = new System.Drawing.Point(655, 71);
            this.listViewRezervari.Name = "listViewRezervari";
            this.listViewRezervari.Size = new System.Drawing.Size(399, 237);
            this.listViewRezervari.TabIndex = 9;
            this.listViewRezervari.UseCompatibleStateImageBehavior = false;
            this.listViewRezervari.View = System.Windows.Forms.View.Details;
            this.listViewRezervari.SelectedIndexChanged += new System.EventHandler(this.listViewRezervari_SelectedIndexChanged);
            // 
            // columnHeaderNume
            // 
            this.columnHeaderNume.Text = "Nume";
            this.columnHeaderNume.Width = 97;
            // 
            // columnHeaderPrenume
            // 
            this.columnHeaderPrenume.Text = "Prenume";
            this.columnHeaderPrenume.Width = 93;
            // 
            // columnHeaderData
            // 
            this.columnHeaderData.Text = "Data";
            this.columnHeaderData.Width = 110;
            // 
            // columnHeaderOra
            // 
            this.columnHeaderOra.Text = "Ora";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificaToolStripMenuItem,
            this.stergeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(122, 48);
            // 
            // modificaToolStripMenuItem
            // 
            this.modificaToolStripMenuItem.Enabled = false;
            this.modificaToolStripMenuItem.Name = "modificaToolStripMenuItem";
            this.modificaToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.modificaToolStripMenuItem.Text = "Modifica";
            this.modificaToolStripMenuItem.Click += new System.EventHandler(this.modificaToolStripMenuItem_Click);
            // 
            // stergeToolStripMenuItem
            // 
            this.stergeToolStripMenuItem.Enabled = false;
            this.stergeToolStripMenuItem.Name = "stergeToolStripMenuItem";
            this.stergeToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.stergeToolStripMenuItem.Text = "Sterge";
            this.stergeToolStripMenuItem.Click += new System.EventHandler(this.stergeToolStripMenuItem_Click);
            // 
            // buttonRezervare
            // 
            this.buttonRezervare.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRezervare.Location = new System.Drawing.Point(90, 237);
            this.buttonRezervare.Name = "buttonRezervare";
            this.buttonRezervare.Size = new System.Drawing.Size(149, 30);
            this.buttonRezervare.TabIndex = 10;
            this.buttonRezervare.Text = "Adauga rezervare";
            this.buttonRezervare.UseVisualStyleBackColor = true;
            this.buttonRezervare.Click += new System.EventHandler(this.buttonRezervare_Click);
            // 
            // labelAparat1
            // 
            this.labelAparat1.AutoSize = true;
            this.labelAparat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAparat1.Location = new System.Drawing.Point(335, 49);
            this.labelAparat1.Name = "labelAparat1";
            this.labelAparat1.Size = new System.Drawing.Size(50, 18);
            this.labelAparat1.TabIndex = 11;
            this.labelAparat1.Text = "Aparat";
            // 
            // comboBoxAparat1
            // 
            this.comboBoxAparat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAparat1.FormattingEnabled = true;
            this.comboBoxAparat1.Location = new System.Drawing.Point(405, 44);
            this.comboBoxAparat1.Name = "comboBoxAparat1";
            this.comboBoxAparat1.Size = new System.Drawing.Size(121, 28);
            this.comboBoxAparat1.TabIndex = 12;
            this.comboBoxAparat1.TabStop = false;
            this.comboBoxAparat1.Validating += new System.ComponentModel.CancelEventHandler(this.comboBoxAparat1_Validating);
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdauga.Location = new System.Drawing.Point(583, 44);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(28, 28);
            this.buttonAdauga.TabIndex = 13;
            this.buttonAdauga.Text = "+";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // buttonSterge1
            // 
            this.buttonSterge1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSterge1.Location = new System.Drawing.Point(550, 44);
            this.buttonSterge1.Name = "buttonSterge1";
            this.buttonSterge1.Size = new System.Drawing.Size(28, 28);
            this.buttonSterge1.TabIndex = 14;
            this.buttonSterge1.Text = "X";
            this.buttonSterge1.UseVisualStyleBackColor = true;
            this.buttonSterge1.Click += new System.EventHandler(this.buttonSterge1_Click);
            // 
            // buttonClient
            // 
            this.buttonClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClient.Location = new System.Drawing.Point(90, 289);
            this.buttonClient.Name = "buttonClient";
            this.buttonClient.Size = new System.Drawing.Size(149, 30);
            this.buttonClient.TabIndex = 15;
            this.buttonClient.Text = "Adauga client";
            this.buttonClient.UseVisualStyleBackColor = true;
            this.buttonClient.Click += new System.EventHandler(this.buttonClient_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fisiereToolStripMenuItem,
            this.printToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1072, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fisiereToolStripMenuItem
            // 
            this.fisiereToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.incarcaInFisierBinarToolStripMenuItem,
            this.incarcaDinFisierBinarToolStripMenuItem,
            this.salveazaClientiInFisierBinarToolStripMenuItem,
            this.incarcaClientiDinFisierBinarToolStripMenuItem});
            this.fisiereToolStripMenuItem.Name = "fisiereToolStripMenuItem";
            this.fisiereToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.fisiereToolStripMenuItem.Text = "&Fisiere";
            // 
            // incarcaInFisierBinarToolStripMenuItem
            // 
            this.incarcaInFisierBinarToolStripMenuItem.Name = "incarcaInFisierBinarToolStripMenuItem";
            this.incarcaInFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.incarcaInFisierBinarToolStripMenuItem.Text = "Salveaza rezervari in fisier binar";
            this.incarcaInFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.incarcaInFisierBinarToolStripMenuItem_Click);
            // 
            // incarcaDinFisierBinarToolStripMenuItem
            // 
            this.incarcaDinFisierBinarToolStripMenuItem.Name = "incarcaDinFisierBinarToolStripMenuItem";
            this.incarcaDinFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.incarcaDinFisierBinarToolStripMenuItem.Text = "Incarca rezervari din fisier binar";
            this.incarcaDinFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.incarcaDinFisierBinarToolStripMenuItem_Click);
            // 
            // salveazaClientiInFisierBinarToolStripMenuItem
            // 
            this.salveazaClientiInFisierBinarToolStripMenuItem.Name = "salveazaClientiInFisierBinarToolStripMenuItem";
            this.salveazaClientiInFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.salveazaClientiInFisierBinarToolStripMenuItem.Text = "Salveaza clienti in fisier binar";
            this.salveazaClientiInFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.salveazaClientiInFisierBinarToolStripMenuItem_Click);
            // 
            // incarcaClientiDinFisierBinarToolStripMenuItem
            // 
            this.incarcaClientiDinFisierBinarToolStripMenuItem.Name = "incarcaClientiDinFisierBinarToolStripMenuItem";
            this.incarcaClientiDinFisierBinarToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.incarcaClientiDinFisierBinarToolStripMenuItem.Text = "Incarca clienti din fisier binar";
            this.incarcaClientiDinFisierBinarToolStripMenuItem.Click += new System.EventHandler(this.incarcaClientiDinFisierBinarToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.printToolStripMenuItem.Text = "&Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // labelLvRezervari
            // 
            this.labelLvRezervari.AutoSize = true;
            this.labelLvRezervari.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLvRezervari.Location = new System.Drawing.Point(651, 48);
            this.labelLvRezervari.Name = "labelLvRezervari";
            this.labelLvRezervari.Size = new System.Drawing.Size(76, 20);
            this.labelLvRezervari.TabIndex = 17;
            this.labelLvRezervari.Text = "Rezervari";
            // 
            // buttonVeziClienti
            // 
            this.buttonVeziClienti.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVeziClienti.Location = new System.Drawing.Point(90, 342);
            this.buttonVeziClienti.Name = "buttonVeziClienti";
            this.buttonVeziClienti.Size = new System.Drawing.Size(149, 30);
            this.buttonVeziClienti.TabIndex = 18;
            this.buttonVeziClienti.Text = "Vizualizeaza Clientii";
            this.buttonVeziClienti.UseVisualStyleBackColor = true;
            this.buttonVeziClienti.Click += new System.EventHandler(this.buttonVeziClienti_Click);
            // 
            // buttonStatistici
            // 
            this.buttonStatistici.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStatistici.Location = new System.Drawing.Point(655, 342);
            this.buttonStatistici.Name = "buttonStatistici";
            this.buttonStatistici.Size = new System.Drawing.Size(174, 30);
            this.buttonStatistici.TabIndex = 19;
            this.buttonStatistici.Text = "Vizualizeaza statisticile";
            this.buttonStatistici.UseVisualStyleBackColor = true;
            this.buttonStatistici.Click += new System.EventHandler(this.buttonStatistici_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // buttonBd
            // 
            this.buttonBd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBd.Location = new System.Drawing.Point(906, 342);
            this.buttonBd.Name = "buttonBd";
            this.buttonBd.Size = new System.Drawing.Size(148, 30);
            this.buttonBd.TabIndex = 20;
            this.buttonBd.Text = "Acceseaza BD";
            this.buttonBd.UseVisualStyleBackColor = true;
            this.buttonBd.Click += new System.EventHandler(this.buttonBd_Click);
            // 
            // FormRezervari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 450);
            this.Controls.Add(this.buttonBd);
            this.Controls.Add(this.buttonStatistici);
            this.Controls.Add(this.buttonVeziClienti);
            this.Controls.Add(this.labelLvRezervari);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.buttonClient);
            this.Controls.Add(this.buttonSterge1);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.comboBoxAparat1);
            this.Controls.Add(this.labelAparat1);
            this.Controls.Add(this.buttonRezervare);
            this.Controls.Add(this.listViewRezervari);
            this.Controls.Add(this.comboBoxOra);
            this.Controls.Add(this.labelOra);
            this.Controls.Add(this.dateTimePickerData);
            this.Controls.Add(this.labelData);
            this.Controls.Add(this.labelPrenume);
            this.Controls.Add(this.textBoxPrenume);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.textBoxNume);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormRezervari";
            this.Text = "Rezervari";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.Label labelPrenume;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.DateTimePicker dateTimePickerData;
        private System.Windows.Forms.Label labelOra;
        private System.Windows.Forms.ComboBox comboBoxOra;
        private System.Windows.Forms.ListView listViewRezervari;
        private System.Windows.Forms.Button buttonRezervare;
        private System.Windows.Forms.Label labelAparat1;
        private System.Windows.Forms.ComboBox comboBoxAparat1;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.Button buttonSterge1;
        private System.Windows.Forms.Button buttonClient;
        private System.Windows.Forms.ColumnHeader columnHeaderNume;
        private System.Windows.Forms.ColumnHeader columnHeaderPrenume;
        private System.Windows.Forms.ColumnHeader columnHeaderData;
        private System.Windows.Forms.ColumnHeader columnHeaderOra;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modificaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stergeToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fisiereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaInFisierBinarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaDinFisierBinarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salveazaClientiInFisierBinarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaClientiDinFisierBinarToolStripMenuItem;
        private System.Windows.Forms.Label labelLvRezervari;
        private System.Windows.Forms.Button buttonVeziClienti;
        private System.Windows.Forms.Button buttonStatistici;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button buttonBd;
    }
}

