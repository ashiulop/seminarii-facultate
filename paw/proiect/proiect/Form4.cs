﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect
{
    public partial class Form4 : Form
    {
        List<Client> listaClienti;
        public Form4(List<Client> listaParam)
        {
            InitializeComponent();
            listaClienti = listaParam;

            foreach (Client c in listaClienti)
            {
                ListViewItem lvi = new ListViewItem(c.nume);
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, c.prenume));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, c.dataNasterii.ToString("dd/MM/yyyy")));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, c.cnp.ToString()));

                lvi.Tag = c;

                listViewClienti.Items.Add(lvi);

            }
        }
    }
}
