﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proiect
{   
    [Serializable]
    public class Client
    {
        public string nume;
        public string prenume;
        public DateTime dataNasterii;
        public int cnp;

        public Client()
        {

        }

        public Client(string nume, string prenume, DateTime dataNasterii, int cnp)
        {
            this.nume = nume;
            this.prenume = prenume;
            this.dataNasterii = dataNasterii;
            this.cnp = cnp;
        }
        public override string ToString()
        {
            return nume + " " + prenume + " " + Convert.ToInt32(cnp) + " " + dataNasterii.ToString();
        }
    }
    
}
