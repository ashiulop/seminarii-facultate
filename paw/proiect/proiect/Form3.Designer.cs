﻿namespace proiect
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxOra = new System.Windows.Forms.ComboBox();
            this.labelOra = new System.Windows.Forms.Label();
            this.dateTimePickerData = new System.Windows.Forms.DateTimePicker();
            this.labelData = new System.Windows.Forms.Label();
            this.labelPrenume = new System.Windows.Forms.Label();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.labelNume = new System.Windows.Forms.Label();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.buttonModifica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxOra
            // 
            this.comboBoxOra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOra.FormattingEnabled = true;
            this.comboBoxOra.Location = new System.Drawing.Point(171, 159);
            this.comboBoxOra.Name = "comboBoxOra";
            this.comboBoxOra.Size = new System.Drawing.Size(117, 28);
            this.comboBoxOra.TabIndex = 16;
            // 
            // labelOra
            // 
            this.labelOra.AutoSize = true;
            this.labelOra.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOra.Location = new System.Drawing.Point(72, 163);
            this.labelOra.Name = "labelOra";
            this.labelOra.Size = new System.Drawing.Size(33, 18);
            this.labelOra.TabIndex = 15;
            this.labelOra.Text = "Ora";
            // 
            // dateTimePickerData
            // 
            this.dateTimePickerData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerData.Location = new System.Drawing.Point(171, 115);
            this.dateTimePickerData.Name = "dateTimePickerData";
            this.dateTimePickerData.Size = new System.Drawing.Size(117, 26);
            this.dateTimePickerData.TabIndex = 14;
            this.dateTimePickerData.Value = new System.DateTime(2020, 4, 22, 12, 54, 2, 0);
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelData.Location = new System.Drawing.Point(72, 121);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(39, 18);
            this.labelData.TabIndex = 13;
            this.labelData.Text = "Data";
            // 
            // labelPrenume
            // 
            this.labelPrenume.AutoSize = true;
            this.labelPrenume.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelPrenume.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrenume.Location = new System.Drawing.Point(72, 75);
            this.labelPrenume.Name = "labelPrenume";
            this.labelPrenume.Size = new System.Drawing.Size(68, 18);
            this.labelPrenume.TabIndex = 12;
            this.labelPrenume.Text = "Prenume";
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrenume.Location = new System.Drawing.Point(171, 71);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(117, 26);
            this.textBoxPrenume.TabIndex = 11;
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNume.Location = new System.Drawing.Point(72, 29);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(48, 18);
            this.labelNume.TabIndex = 10;
            this.labelNume.Text = "Nume";
            // 
            // textBoxNume
            // 
            this.textBoxNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNume.Location = new System.Drawing.Point(171, 25);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(117, 26);
            this.textBoxNume.TabIndex = 9;
            // 
            // buttonModifica
            // 
            this.buttonModifica.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonModifica.Location = new System.Drawing.Point(117, 213);
            this.buttonModifica.Name = "buttonModifica";
            this.buttonModifica.Size = new System.Drawing.Size(104, 32);
            this.buttonModifica.TabIndex = 17;
            this.buttonModifica.Text = "Modifica";
            this.buttonModifica.UseVisualStyleBackColor = true;
            this.buttonModifica.Click += new System.EventHandler(this.buttonModifica_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 287);
            this.Controls.Add(this.buttonModifica);
            this.Controls.Add(this.comboBoxOra);
            this.Controls.Add(this.labelOra);
            this.Controls.Add(this.dateTimePickerData);
            this.Controls.Add(this.labelData);
            this.Controls.Add(this.labelPrenume);
            this.Controls.Add(this.textBoxPrenume);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.textBoxNume);
            this.Name = "Form3";
            this.Text = "Modifica Rezervare";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxOra;
        private System.Windows.Forms.Label labelOra;
        private System.Windows.Forms.DateTimePicker dateTimePickerData;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Label labelPrenume;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.Button buttonModifica;
    }
}