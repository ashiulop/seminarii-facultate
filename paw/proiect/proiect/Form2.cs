﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proiect
{
    public partial class Form2 : Form
    {
        Client client;
        public Form2(Client rParametru)
        {
            InitializeComponent();
            dateTimePickerClient.Format = DateTimePickerFormat.Custom;
            dateTimePickerClient.CustomFormat = "dd/MM/yyyy";
            client = rParametru;

            textBoxNumeClient.Text = client.nume;
            textBoxPrenumeClient.Text = client.prenume;
        }

        private void buttonAdaugaClient_Click(object sender, EventArgs e)
        {
            client.nume = textBoxNumeClient.Text;
            client.prenume = textBoxPrenumeClient.Text;
            client.dataNasterii = dateTimePickerClient.Value.Date;
            DialogResult = DialogResult.OK;
            try
            {
                client.cnp = Convert.ToInt32(textBoxCnpClient.Text);
            }
            catch
            {
                MessageBox.Show("Eroare!");
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
