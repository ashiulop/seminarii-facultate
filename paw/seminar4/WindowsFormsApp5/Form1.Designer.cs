﻿namespace WindowsFormsApp5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.textBoxPrenume = new System.Windows.Forms.TextBox();
            this.textBoxVarsta = new System.Windows.Forms.TextBox();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.listBoxPersoane = new System.Windows.Forms.ListBox();
            this.listViewPersoane = new System.Windows.Forms.ListView();
            this.columnHeaderNume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPrenume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderVarsta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAdaugaFormular = new System.Windows.Forms.Button();
            this.buttonVeziPersoane = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nume";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prenume";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "varsta";
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(126, 51);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(100, 20);
            this.textBoxNume.TabIndex = 3;
            // 
            // textBoxPrenume
            // 
            this.textBoxPrenume.Location = new System.Drawing.Point(126, 87);
            this.textBoxPrenume.Name = "textBoxPrenume";
            this.textBoxPrenume.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrenume.TabIndex = 4;
            // 
            // textBoxVarsta
            // 
            this.textBoxVarsta.Location = new System.Drawing.Point(126, 123);
            this.textBoxVarsta.Name = "textBoxVarsta";
            this.textBoxVarsta.Size = new System.Drawing.Size(100, 20);
            this.textBoxVarsta.TabIndex = 5;
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.Location = new System.Drawing.Point(126, 186);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(75, 23);
            this.buttonAdauga.TabIndex = 6;
            this.buttonAdauga.Text = "Adauga";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // listBoxPersoane
            // 
            this.listBoxPersoane.FormattingEnabled = true;
            this.listBoxPersoane.Location = new System.Drawing.Point(333, 51);
            this.listBoxPersoane.Name = "listBoxPersoane";
            this.listBoxPersoane.Size = new System.Drawing.Size(173, 121);
            this.listBoxPersoane.TabIndex = 7;
            // 
            // listViewPersoane
            // 
            this.listViewPersoane.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderNume,
            this.columnHeaderPrenume,
            this.columnHeaderVarsta});
            this.listViewPersoane.HideSelection = false;
            this.listViewPersoane.Location = new System.Drawing.Point(333, 219);
            this.listViewPersoane.Name = "listViewPersoane";
            this.listViewPersoane.Size = new System.Drawing.Size(173, 141);
            this.listViewPersoane.TabIndex = 8;
            this.listViewPersoane.UseCompatibleStateImageBehavior = false;
            this.listViewPersoane.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderNume
            // 
            this.columnHeaderNume.Text = "Nume";
            // 
            // columnHeaderPrenume
            // 
            this.columnHeaderPrenume.Text = "Prenume";
            // 
            // columnHeaderVarsta
            // 
            this.columnHeaderVarsta.Text = "Varsta";
            // 
            // buttonAdaugaFormular
            // 
            this.buttonAdaugaFormular.Location = new System.Drawing.Point(126, 244);
            this.buttonAdaugaFormular.Name = "buttonAdaugaFormular";
            this.buttonAdaugaFormular.Size = new System.Drawing.Size(138, 23);
            this.buttonAdaugaFormular.TabIndex = 9;
            this.buttonAdaugaFormular.Text = "Adauga prin Formular";
            this.buttonAdaugaFormular.UseVisualStyleBackColor = true;
            this.buttonAdaugaFormular.Click += new System.EventHandler(this.buttonAdaugaFormular_Click);
            // 
            // buttonVeziPersoane
            // 
            this.buttonVeziPersoane.Location = new System.Drawing.Point(541, 186);
            this.buttonVeziPersoane.Name = "buttonVeziPersoane";
            this.buttonVeziPersoane.Size = new System.Drawing.Size(97, 23);
            this.buttonVeziPersoane.TabIndex = 10;
            this.buttonVeziPersoane.Text = "Vezi Persoane";
            this.buttonVeziPersoane.UseVisualStyleBackColor = true;
            this.buttonVeziPersoane.Click += new System.EventHandler(this.buttonVeziPersoane_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 450);
            this.Controls.Add(this.buttonVeziPersoane);
            this.Controls.Add(this.buttonAdaugaFormular);
            this.Controls.Add(this.listViewPersoane);
            this.Controls.Add(this.listBoxPersoane);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.textBoxVarsta);
            this.Controls.Add(this.textBoxPrenume);
            this.Controls.Add(this.textBoxNume);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.TextBox textBoxPrenume;
        private System.Windows.Forms.TextBox textBoxVarsta;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.ListBox listBoxPersoane;
        private System.Windows.Forms.ListView listViewPersoane;
        private System.Windows.Forms.ColumnHeader columnHeaderNume;
        private System.Windows.Forms.ColumnHeader columnHeaderPrenume;
        private System.Windows.Forms.ColumnHeader columnHeaderVarsta;
        private System.Windows.Forms.Button buttonAdaugaFormular;
        private System.Windows.Forms.Button buttonVeziPersoane;
    }
}

