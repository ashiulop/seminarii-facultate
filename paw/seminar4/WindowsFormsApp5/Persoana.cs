﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp5
{
    public class Persoana
    {
        public string Nume;
        public string Prenume;
        public int Varsta;

        public Persoana(string n, string p, int v)
        {
            Nume = n;
            Prenume = p;
            Varsta = v;
        }

        public override string ToString()
        {
            return Nume + " " + Prenume + " " + Varsta;
        }
    }
}
