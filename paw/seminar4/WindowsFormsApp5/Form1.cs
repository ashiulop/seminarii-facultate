﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        private  List<Persoana> listaPersoane;
        public Form1()
        {
            InitializeComponent();
            listaPersoane = new List<Persoana>();
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            Persoana p = new Persoana("", "", 0);
            p.Nume = textBoxNume.Text;
            p.Prenume = textBoxPrenume.Text;
            bool adaugaok = true;
            try
            {
                p.Varsta = Convert.ToInt32(textBoxVarsta.Text);
            }
            catch
            {
                adaugaok = false;
                MessageBox.Show("Conversia nu a reusit", "Mesaj", MessageBoxButtons.OK);
            }
            if (adaugaok)
            {
                AdaugainControale(p);
            }
      

        }

        private void AdaugainControale(Persoana p)
        {
            listBoxPersoane.Items.Add(p.ToString());
            listaPersoane.Add(p);

            ListViewItem lvi = new ListViewItem(p.Nume);
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Prenume));
            lvi.SubItems.Add
                    (new ListViewItem.ListViewSubItem(lvi, p.Varsta.ToString()));

            lvi.Tag = p;

            listViewPersoane.Items.Add(lvi);
        }

        private void buttonAdaugaFormular_Click(object sender, EventArgs e)
        {
            Persoana pForm1 = new Persoana("", "", 0);
            Form2 f2 = new Form2(pForm1);
            f2.ShowDialog();
            if(f2.DialogResult == DialogResult.OK)
            {
                AdaugainControale(pForm1);
            }
        }

        private void buttonVeziPersoane_Click(object sender, EventArgs e)
        {
            Form3 f3 = new Form3(listaPersoane);
            f3.ShowDialog();
        }
    }
}
