﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form2 : Form
    {
        Persoana pForm2;
        public Form2(Persoana pParametru)
        {
            InitializeComponent();

            pForm2 = pParametru;
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            pForm2.Nume = textBoxNume.Text;
            pForm2.Prenume = textBoxPrenume.Text;
            try
            {
                pForm2.Varsta = Convert.ToInt32(textBoxVarsta.Text);
            }
            catch
            {
                MessageBox.Show("Eroare conversie");
                this.DialogResult = DialogResult.Cancel;
            }
        }
    }
}
