﻿namespace WindowsFormsApp5
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxPersoane = new System.Windows.Forms.ListBox();
            this.buttonRenunta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxPersoane
            // 
            this.listBoxPersoane.FormattingEnabled = true;
            this.listBoxPersoane.Location = new System.Drawing.Point(104, 102);
            this.listBoxPersoane.Name = "listBoxPersoane";
            this.listBoxPersoane.Size = new System.Drawing.Size(166, 134);
            this.listBoxPersoane.TabIndex = 0;
            // 
            // buttonRenunta
            // 
            this.buttonRenunta.Location = new System.Drawing.Point(359, 157);
            this.buttonRenunta.Name = "buttonRenunta";
            this.buttonRenunta.Size = new System.Drawing.Size(85, 24);
            this.buttonRenunta.TabIndex = 1;
            this.buttonRenunta.Text = "Renunta";
            this.buttonRenunta.UseVisualStyleBackColor = true;
            this.buttonRenunta.Click += new System.EventHandler(this.buttonRenunta_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 341);
            this.Controls.Add(this.buttonRenunta);
            this.Controls.Add(this.listBoxPersoane);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxPersoane;
        private System.Windows.Forms.Button buttonRenunta;
    }
}