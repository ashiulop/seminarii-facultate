﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form3 : Form
    {
        public Form3(List<Persoana> persoaneForm3)
        {
            InitializeComponent();

            foreach(Persoana p in persoaneForm3)
            {
                listBoxPersoane.Items.Add(p.ToString());
            }
        }

        private void buttonRenunta_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
