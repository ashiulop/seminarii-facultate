﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form2 : Form
    {
        Persoana pForm2;
        public Form2(Persoana pParametru)
        {
            pForm2 = pParametru;
            InitializeComponent();

            if(pForm2.Nume.Length > 0)
            {
                textBoxCNP.Text = pForm2.cnp;
                textBoxNume.Text = pForm2.Nume;
                textBoxPrenume.Text = pForm2.Prenume;
                textBoxVarsta.Text = pForm2.Varsta.ToString();

                buttonOk.Text = "Modifica";
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            pForm2.cnp = textBoxCNP.Text;
            pForm2.Nume = textBoxNume.Text;
            pForm2.Prenume = textBoxPrenume.Text;
            DialogResult = DialogResult.OK;
            try
            {
                pForm2.Varsta = Convert.ToInt32(textBoxVarsta.Text);
            }
            catch
            {
                MessageBox.Show("Eroare");
                DialogResult = DialogResult.Cancel;

            }
        }
    }
}
