﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        private  List<Persoana> listaPersoane;
        public Form1()
        {
            InitializeComponent();
            listaPersoane = new List<Persoana>();

            Persoana p1 = new Persoana("1234", "Ion", "Popescu", 10);
            AdaugainControale(p1);

            p1 = new Persoana("12342", "Ionel", "Georgescu", 20);
            AdaugainControale(p1);

        }

        private void AdaugainControale(Persoana p)
        {
            listaPersoane.Add(p);
            ListViewItem lvi = new ListViewItem(p.cnp);
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Nume));
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Prenume));
            lvi.SubItems.Add
                    (new ListViewItem.ListViewSubItem(lvi, p.Varsta.ToString()));
            lvi.Tag = p;
            listViewPersoane.Items.Add(lvi);
        }

        private void buttonAdaugaFormular_Click(object sender, EventArgs e)
        {
            Persoana pForm1 = new Persoana("","","",0);
            Form2 f2 = new Form2(pForm1);
            f2.ShowDialog();

            if (f2.DialogResult == DialogResult.OK)
            {
                AdaugainControale(pForm1);
            }       

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Persoana pForm1 = new Persoana("","", "", 0);
            Form2 f2 = new Form2(pForm1);
            f2.ShowDialog();

            if (f2.DialogResult == DialogResult.OK)
            {
                AdaugainControale(pForm1);
            }
            // creati form3
            // se instantiaza la apasarea unui buton din form 1
            // afisati in listbox din form 3 lista de persoane din memorie din Form 1


        }

        private void listViewPersoane_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listViewPersoane.SelectedItems.Count > 0)
            {
                contextMenuStrip1.Items[1].Enabled = true;
                contextMenuStrip1.Items[2].Enabled = true;
            }
            else
            {
                contextMenuStrip1.Items[1].Enabled = false;
                contextMenuStrip1.Items[2].Enabled = false;
            }
        }

        private void adaugaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonAdaugaFormular_Click(sender, e);
        }

        private void modificaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Persoana pForm1 = (Persoana)listViewPersoane.SelectedItems[0].Tag;
            Form2 f2 = new Form2(pForm1);
            f2.ShowDialog();

            if(f2.DialogResult == DialogResult.OK)
            {
                ListViewItem lvi = listViewPersoane.SelectedItems[0];
                lvi.Text = pForm1.cnp;
                lvi.SubItems[1].Text = pForm1.Nume;
                lvi.SubItems[2].Text = pForm1.Prenume;
                lvi.SubItems[3].Text = pForm1.Varsta.ToString();
            }
        }

        private void stergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listViewPersoane.SelectedItems[0].Remove();
        }

        private void buttonInserare_Click(object sender, EventArgs e)
        {
            if(listViewPersoane.SelectedItems.Count > 0 && treeView1.SelectedNode != null)
            {
                Persoana plocal = (Persoana)listViewPersoane.SelectedItems[0].Tag;
                TreeNode t = new TreeNode(plocal.ToString());
                t.Tag = plocal;

                treeView1.SelectedNode.Nodes.Add(t);
            }
        }

        private void stergeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            treeView1.SelectedNode.Remove();

            if(treeView1.SelectedNode == null)
            {
                contextMenuStrip2.Items[0].Enabled = false;
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if(treeView1.SelectedNode != null)
            {
                contextMenuStrip2.Items[0].Enabled = true;
            }
        }
    }
}
