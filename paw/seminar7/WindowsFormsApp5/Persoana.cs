﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp5
{
    public class Persoana
    {
        public string Nume;
        public string Prenume;
        public int Varsta;
        public string cnp;

        public Persoana(string c,string n, string p, int v)
        {
            cnp = c;
            Nume = n;
            Prenume = p;
            Varsta = v;
        }

        public override string ToString()
        {
            return Nume + " " + Prenume + " " + Varsta;
        }
    }
}
