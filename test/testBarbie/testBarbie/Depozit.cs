﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testBarbie
{
    public class Depozit
    {
        private int cod;
        private List<Produs> produse;

        public Depozit()
        {
            this.cod = 0;
            this.produse = null;
        }

        public Depozit(int cod, List<Produs> produse)
        {
            this.cod = cod;
            this.produse = new List<Produs>(produse);
        }

        public int Cod
        {
            get
            {
                return this.cod;
            }
            set
            {
                if (value > 0 && value < 999)
                    this.cod = value;
            }
        }

        public List<Produs> Produse
        {
            get
            {
                return this.produse;
            }
            set
            {
                if (value != null)
                    this.produse = value;
            }
        }

        public static Depozit operator+(Depozit d, Produs p)
        {
            if (p != null)
                d.produse.Add(p);
            return d;
        }

        public override string ToString()
        {
            string rezultat = "";
            rezultat = rezultat + "Cod depozit: " + this.cod;

            if (this.produse != null)
            {
                rezultat = rezultat + "\nProduse: \n";
                foreach (Produs p in produse)
                    rezultat = rezultat + p.ToString() + "\n";
            }
            else
                rezultat = rezultat + " Nu exista produse in depozit!";
            return rezultat;
        }
    }
}
