﻿namespace testBarbie
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxCod = new System.Windows.Forms.TextBox();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.textBoxPret = new System.Windows.Forms.TextBox();
            this.labelCod = new System.Windows.Forms.Label();
            this.labelNume = new System.Windows.Forms.Label();
            this.labelPret = new System.Windows.Forms.Label();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.listViewProduse = new System.Windows.Forms.ListView();
            this.columnHeaderCod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderNume = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPret = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxCod
            // 
            this.textBoxCod.Location = new System.Drawing.Point(131, 39);
            this.textBoxCod.Name = "textBoxCod";
            this.textBoxCod.Size = new System.Drawing.Size(100, 20);
            this.textBoxCod.TabIndex = 0;
            this.textBoxCod.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxCod_Validating);
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(131, 80);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(100, 20);
            this.textBoxNume.TabIndex = 1;
            this.textBoxNume.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxNume_Validating);
            // 
            // textBoxPret
            // 
            this.textBoxPret.Location = new System.Drawing.Point(131, 120);
            this.textBoxPret.Name = "textBoxPret";
            this.textBoxPret.Size = new System.Drawing.Size(100, 20);
            this.textBoxPret.TabIndex = 2;
            this.textBoxPret.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxPret_Validating);
            // 
            // labelCod
            // 
            this.labelCod.AutoSize = true;
            this.labelCod.Location = new System.Drawing.Point(66, 42);
            this.labelCod.Name = "labelCod";
            this.labelCod.Size = new System.Drawing.Size(26, 13);
            this.labelCod.TabIndex = 3;
            this.labelCod.Text = "Cod";
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(66, 83);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(35, 13);
            this.labelNume.TabIndex = 4;
            this.labelNume.Text = "Nume";
            // 
            // labelPret
            // 
            this.labelPret.AutoSize = true;
            this.labelPret.Location = new System.Drawing.Point(66, 123);
            this.labelPret.Name = "labelPret";
            this.labelPret.Size = new System.Drawing.Size(26, 13);
            this.labelPret.TabIndex = 5;
            this.labelPret.Text = "Pret";
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.Location = new System.Drawing.Point(93, 170);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(109, 23);
            this.buttonAdauga.TabIndex = 6;
            this.buttonAdauga.Text = "Adauga produs";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // listViewProduse
            // 
            this.listViewProduse.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCod,
            this.columnHeaderNume,
            this.columnHeaderPret});
            this.listViewProduse.HideSelection = false;
            this.listViewProduse.Location = new System.Drawing.Point(429, 39);
            this.listViewProduse.Name = "listViewProduse";
            this.listViewProduse.Size = new System.Drawing.Size(248, 154);
            this.listViewProduse.TabIndex = 7;
            this.listViewProduse.UseCompatibleStateImageBehavior = false;
            this.listViewProduse.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderCod
            // 
            this.columnHeaderCod.Text = "Cod";
            // 
            // columnHeaderNume
            // 
            this.columnHeaderNume.Text = "Nume";
            // 
            // columnHeaderPret
            // 
            this.columnHeaderPret.Text = "Pret";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listViewProduse);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.labelPret);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.labelCod);
            this.Controls.Add(this.textBoxPret);
            this.Controls.Add(this.textBoxNume);
            this.Controls.Add(this.textBoxCod);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCod;
        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.TextBox textBoxPret;
        private System.Windows.Forms.Label labelCod;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.Label labelPret;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ListView listViewProduse;
        private System.Windows.Forms.ColumnHeader columnHeaderCod;
        private System.Windows.Forms.ColumnHeader columnHeaderNume;
        private System.Windows.Forms.ColumnHeader columnHeaderPret;
    }
}