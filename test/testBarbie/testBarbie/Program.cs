﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testBarbie
{
    class Program
    {
        static void Main(string[] args)
        {
            Produs p1 = new Produs(2, "Apa", 3.5f);
            Produs p2 = new Produs(3, "Paine", 2f);
            Produs p3 = new Produs(5, "Cartofi", 1.5f);

            List<Produs> lista = new List<Produs>();
            lista.Add(p1);
            lista.Add(p2);
            lista.Add(p3);

            Depozit d1 = new Depozit(1, lista);
            Console.WriteLine(d1);
            //Console.Read();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(d1));
        }
    }
}
