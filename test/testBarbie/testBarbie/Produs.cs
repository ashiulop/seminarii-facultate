﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testBarbie
{
    public class Produs
    {
        private int cod;
        private string nume;
        private float pret;

        public Produs()
        {
            this.cod = 0;
            this.nume = "Necunoscut";
            this.pret = 0.0f;
        }

        public Produs(int cod, string nume, float pret)
        {
            this.cod = cod;
            this.nume = nume;
            this.pret = pret;
        }

        public int Cod
        {
            get
            {
                return this.cod;
            }
            set
            {
                if (value > 0 && value < 999)
                    this.cod = value;
            }
        }

        public string Nume
        {
            get
            {
                return this.nume;
            }
            set
            {
                if (value.Length > 0)
                    this.nume = value;
            }
        }

        public float Pret
        {
            get
            {
                return this.pret;
            }
            set
            {
                if(value > 0 && value < 10000)
                {
                    this.pret = value;
                }
            }
        }

        public override string ToString()
        {
            return "Cod: " + this.cod + ", Nume: " + this.nume + ", Pret: " + this.pret;
        }
    }
}
