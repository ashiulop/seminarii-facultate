﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testBarbie
{
    public partial class Form1 : Form
    {
        public Depozit depozit;
        public Form1(Depozit d)
        {
            InitializeComponent();
            this.depozit = d;

            foreach(Produs p in this.depozit.Produse)
            {
                ListViewItem lvi = new ListViewItem(p.Cod.ToString());
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Nume));
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Pret.ToString()));
                lvi.Tag = p;

                listViewProduse.Items.Add(lvi);
            }
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            Produs p = new Produs();
            try
            {
                p.Cod = Convert.ToInt32(textBoxCod.Text);
            }
            catch
            {
                MessageBox.Show("Cod introdus gresit!");
            }
                

            p.Nume = textBoxNume.Text;
            try {
                p.Pret = float.Parse(textBoxPret.Text);
            }
            catch
            {
                MessageBox.Show("Pret introdus gresit!");
            }
            finally
            {
                textBoxCod.Text = "";
                textBoxNume.Text = "";
                textBoxPret.Text = "";
            }


            this.depozit = this.depozit + p;

            ListViewItem lvi = new ListViewItem(p.Cod.ToString());
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Nume));
            lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, p.Pret.ToString()));
            lvi.Tag = p;

            listViewProduse.Items.Add(lvi);

        }

        private void textBoxCod_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            try
            {
                int cod = Convert.ToInt32(textBoxCod.Text);
                errorProvider1.SetError(tbLocal, "");
            }
            catch
            {
                errorProvider1.SetError(tbLocal, "Codul trebuie sa fie un numar!");
            }
        }

        private void textBoxNume_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            if (tbLocal.Text.Length == 0)
                errorProvider1.SetError(tbLocal, "Numele nu trebuie sa fie gol!");
            else
                errorProvider1.SetError(tbLocal, "");
        }

        private void textBoxPret_Validating(object sender, CancelEventArgs e)
        {
            TextBox tbLocal = (TextBox)sender;
            try
            {
                float pret = float.Parse(textBoxPret.Text);
                errorProvider1.SetError(tbLocal, "");
            }
            catch
            {
                errorProvider1.SetError(tbLocal, "Pretul trebuie sa fie un numar!");
            }
        }
    }
}
