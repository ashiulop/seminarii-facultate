﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrupaStudenti
{
    class Student
    {
        private static int contor = 0;
        private int cod ;
        private string nume;
        private float medie;

        public Student()
        {
            this.cod = ++Student.contor;
            this.nume = "Anonim";
            this.medie = 0.0f;
        }

        public Student(string nume, float medie)
        {
            this.cod = ++Student.contor;
            this.nume = nume;
            this.medie = medie;
        }

        public string Nume
        {
            get
            {
                return this.nume;
            }
            set
            {
                if (value.Length > 0)
                    this.nume = value;
            }
        }

        public int Cod
        {
            get
            {
                return this.cod;
            }
        }

        public float Medie
        {
            get
            {
                return this.medie;
            }
            set
            {
                if (value > 0 && value <= 10)
                    this.medie = value;
            }
        }
        public override string ToString()
        {
            return "Cod: " + this.cod + ", Nume: " + this.nume + ", Medie: " + this.medie;
        }
    }
}
