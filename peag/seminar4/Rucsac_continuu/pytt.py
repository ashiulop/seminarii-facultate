import numpy as np
from FunctiiMutatieIndivizi import m_neuniforma
import matplotlib.pyplot as grafic

def fob(s, n, c, v, cmax):
    val = 0
    cost = 0
    for i in range(n):
        val += s[i] * v[i]
        cost += s[i] * c[i]
    return val, cost <= cmax

def deseneaza(pop, popM, dim, n):
    x =[i for i in range(dim)]
    y = [pop[i][n] for i in range(dim)]
    grafic.plot(x, y, "ks", Markersize = 12)
    ym = [popM[i][n] for i in range(dim)]
    grafic.plot(x, ym, "rs", Markersize = 10)
    grafic.show()

def gen(fc, fv, cmax, dim):
    c = np.genfromtxt(fc)
    v = np.genfromtxt(fv)
    n = len(c)
    pop = []
    for i in range(dim):
        fezabil = False
        while not fezabil:
            s = np.random.uniform(0, 1, n)
            val, fezabil = fob(s, n, c, v, cmax)
        s = list(s)
        s = s + [val]
        pop = pop + [s]
    return pop, dim, n, v, c

def mutatie_pop(pop, dim, n, c, v, cmax, pm, sigma):
    popM = pop.copy()
    for i in range(dim):
        x = pop[i][:n].copy()
        for j in range(n):
            r = np.random.uniform(0,1)
            if r <= pm:
                x[j] = m_neuniforma(x[j], sigma, 0, 1)
        val, ok = fob(x, n, c, v, cmax)
        if ok:
            x = x + [val]
            popM[i] = x
    deseneaza(pop, popM, dim, n)
    return popM
