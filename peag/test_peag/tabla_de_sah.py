import numpy as np
import matplotlib.pyplot as grafic

def f_obiectiv(x,n):
    c = n*(n-1)/2
    for i in range(n):
        for j in range(i+1,n):
            if abs(i-j)%2 != abs(x[i]-x[j])%2:
                c=c-1
    return c


def gen(n,dim):
    pop=np.zeros((dim,n+1),dtype=int)
    for i in range(dim):
        pop[i,:n]=np.random.permutation(n)
        pop[i,n]=f_obiectiv(pop[i,:n],n)
    return pop

def OCX(x,y,n,p1,p2):
    c2=[x[i] for i in range(p1,p2+1)]
    z1=[y[i] for i in range(p2,n) if y[i] not in c2]
    z2=[y[i] for i in range(p2) if y[i] not in c2]
    z=np.append(z1,z2)
    c3=[z[i] for i in range(n-p2-1)]
    c1=[z[i] for i in range(n-p2-1,len(z))]
    c=np.append(c1,c2)
    c=np.append(c,c3)
    return c

def crossover_OCX(x,y,n):
    poz=np.random.randint(0,n,2)
    while poz[0]==poz[1]:
        poz=np.random.randint(0,n,2)
    p1=np.min(poz)
    p2=np.max(poz)
    c1=OCX(x,y,n,p1,p2)
    c2=OCX(y,x,n,p1,p2)
    return c1,c2

def crossover_populatie(pop,dim,n,pc):
    po=pop.copy()
    for i in range(0,dim-1,2):
        x = pop[i]
        y = pop[i+1]
        r = np.random.uniform(0,1)
        if r<=pc:
            c1,c2 = crossover_OCX(x,y,n)
            val1=f_obiectiv(c1, n)
            val2= f_obiectiv(c2, n)
            po[i][:n]=c1.copy()
            po[i][n]=val1
            po[i+1][:n]=c2.copy()
            po[i+1][n]=val2
    return po


def m_perm_inserare(x,n):
    poz = np.random.randint(0, n, 2)
    while poz[0] == poz[1]:
        poz = np.random.randint(0, n, 2)
    p1 = np.min(poz)
    p2 = np.max(poz)
    y=x.copy()
    y[p1+1]=x[p2]
    if p1<n-2:
        y[p1+2:n]=np.array([x[i] for i in range(p1+1,n) if i != p2])
    return y

def mutatie_populatie(pop,dim,n,pm):
    mpop=pop.copy()
    for i in range(dim):
        r=np.random.uniform(0,1)
        if r<=pm:
            x=m_perm_inserare(mpop[i,:n],n)
            mpop[i,:n]=x.copy()
            mpop[i,n]=f_obiectiv(x,n)
    return mpop


def arata(sol,v):

    n=len(sol)
    t=len(v)
    minim=min(v)
    t1="Evoluția calității (cel mai bun individ din fiecare generație).\nRezultatul "
    t2="Cea mai bună așezare a pieselor găsită.\nRezultatul "
    t4="este optim"
    if minim>0:
        t3="nu "
    else:
        t3=""
    titlu1=f'{t1}{t3}{t4}'
    titlu2=t2+t3+t4

    fig1=grafic.figure()
    x=[i for i in range(t)]
    grafic.plot(x,v,'ro-')
    grafic.ylabel("Calitate")
    grafic.xlabel("Generația")
    grafic.title(titlu1)

    fig=grafic.figure()
    ax=fig.gca()
    x=[i+0.5 for i in range(n)]
    y=[sol[i]+0.5 for i in range(n)]
    grafic.plot(x,y,'r*',markersize=10)
    grafic.xticks(range(n+1))
    grafic.yticks(range(n+1))
    grafic.grid(True,which='both',color='k', linestyle='-', linewidth=1)
    ax.set_aspect('equal')
    grafic.title(titlu2)
    fig.show()

def fps(qual,dim):
    fps=np.zeros(dim)
    suma=np.sum(qual)
    for i in range (dim):
        fps[i] = qual[i]/suma
    qfps=fps.copy()
    for i in range(1, dim):
        qfps[i]=qfps[i-1]+fps[i]
    return qfps

def sigmafps(qual, dim):
    med=np.mean(qual)
    var=np.std(qual)
    newq=[max(0,qual[i]-(med-2*var)) for i in range(dim)]
    if np.sum(newq)==0:
        qfps=fps(qual,dim)
    else:
        qfps=fps(newq,dim)
    return qfps

def ruleta(pop,qual,dim,n):
    spop=pop.copy()
    squal=np.zeros(dim)
    qfps=sigmafps(qual,dim)
    for it in range(dim):
        r=np.random.uniform(0,1)
        poz=np.where(qfps>=r)
        isel=poz[0][0]
        spop[it][:]=pop[isel][:]
        squal[it]=qual[isel]
    return spop, squal

def elitism(pop_c,qual_c,pop_mo,qual_mo,dim):
    pop=np.copy(pop_mo)
    qual=np.copy(qual_mo)
    max_c=np.max(qual_c)
    max_mo=np.max(qual_mo)
    if max_c>max_mo:
        p1=np.where(qual_c==max_c)
        imax=p1[0][0]
        ir=np.random.randint(dim)
        pop[ir]=pop_c[imax].copy()
        qual[ir]=max_c
    return pop,qual

def GA(n,dim,NMAX,pc,pm):
    pop=gen(n,dim)
    it=0
    gata=False
    maxim=np.max(pop[:,n])
    istoric_v=[n*(n-1)/2-maxim]
    while it<NMAX and not gata and maxim<n*(n-1)/2 :
        spop,sval=ruleta(pop[:,:n],pop[:,n],dim,n)
        pop_s=np.zeros([dim,n+1])
        pop_s[:,:n]=spop.copy()
        pop_s[:,n]=sval.copy()
        pop_o=crossover_populatie(pop_s,dim,n,pc)
        pop_mo=mutatie_populatie(pop_o,dim,n,pm)
        newpop,newval=elitism(pop[:,:n],pop[:,n],pop_mo[:,:n],pop_mo[:,n],dim)
        minim=np.min(newval)
        maxim=np.max(newval)
        if maxim==minim:
            gata=True
        else:
            it=it+1
        istoric_v.append(n*(n-1)/2-np.max(newval))
        pop[:,:n]=newpop.copy()
        pop[:,n]=newval.copy()
    i_sol=np.where(pop[:,n]==maxim)
    sol=pop[i_sol[0][0]][:n]
    val=n*(n-1)/2-maxim
    arata(sol,istoric_v)
    return sol, val


#exemplu de apel:
#import tabla_de_sah as ts
#sol,val=ts.GA(12,50,250,0.8,0.1)