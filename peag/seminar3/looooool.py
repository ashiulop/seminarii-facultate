import numpy as np

def fitness(s, n, c):
    val = c[s[0], s[n-1]]
    for i in range(n-1):
        val = val + c[s[i], s[i+1]]
    return 100/val

def gen(numeF, dim):
    cost = np.genfromtxt(numeF)
    n = len(cost)
    pop = np.zeros([dim, n], dtype="int")
    fit = np.zeros(dim)
    for i in range(dim):
        s = np.random.permutation(n)
        v = fitness(s, n, cost)
        pop[i] = s
        fit[i] = v
    return [pop, fit]
