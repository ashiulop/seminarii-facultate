import matplotlib.pyplot as grafic
import numpy as np

#preia date float dintr-un fisier text ce contine o matrice patratica scrisa pe linii
# in care fiecare linie valorile sunt separate cu un singur spatiu
def citeste(fis):
   a=np.genfromtxt(fis)
   return a


#f. obiectiv
def foTSP(p,c,n):
    val = 0
    for i in range(n - 1):
        val = val + c[p[i]][p[i+1]]
    val = val+c[p[0]][p[n-1]]
    return 100/val

# figurarea populatiei prin punctele (indice individ, calitate) - pentru a vedea variabilitatea in populatie
def reprezinta_pop(val, dim, n):
    x = [i for i in range(dim)]
    y = [val[i] for i in range(dim)]
    grafic.plot(x, y, "gs-", markersize=12)

#genereaza populatia initiala
#I:
# fc - numele fisierului costurilor
# dim - numarul de indivizi din populatie
#E: lista cu 2 componente [pop,val] - populatia initiala si vectorul valorilor
def gen(fc,dim):
    #citeste datele din fisierul nxn al costurilor
    c=citeste(fc)
    #n=dimensiunea problemei
    n = len(c)
    #defineste o variabila ndarray dimx(n+1) cu toate elementele 0
    pop=np.zeros((dim,n),dtype=int)
    val=np.zeros(dim,dtype=float)
    for i in range(dim):
        #genereaza candidatul permutare cu n elemente
        pop[i] = np.random.permutation(n)
        # evalueaza candidat
        val[i] = foTSP(pop[i,:n],c,n)
    #[pop,val]=lista L cu primul element populatia, al doilea element vectorul valorilor
    #ca referire, pop=L[0], val=L[1]
    reprezinta_pop(val,dim,n)
    return [pop, val]

#Apel
#import generare_init as gi
#[p,v]=gi.gen("costuri.txt",30)

