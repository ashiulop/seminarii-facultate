#Calcul suma elemente pare vector

#Citire de la tastatura:
# x = int(input("Nr elemente vector: "))
# print("Introduceti elementele vectorului: ")
# v = []
#
# for i in range(x):
#     v.append(int(input()))

v = [1, 4, 6, 6, 2, 8, 3, 1]
sum = 0

for i in v:
    if i % 2 == 0:
        sum += i

print("Suma = " + str(sum))

#Numar elemente nule matrice

#Citire de la tastatura:
# x = int(input("nr linii: "))
# y = int(input("nr coloane: "))
# m = []
# print("Introduceti numerele din matrice: ")
# for i in range(x):
#     a = []
#     for j in range(y):
#         a.append(int(input()))
#     m.append(a)

m = [[1, 0, 2, 0], [0, 0, 3, 9], [2, 2, 5, 0]]
nr = 0

for i in m:
    for j in i:
        if j == 0:
            nr += 1

print("Numar elemente nenule din matrice = " + str(nr))

#Sortare descrescatoare linii matrice

for i in m:
    i.sort(reverse=True)
    print(i)

#CMMDC

a = int(input("a = "))
b = int(input("b = "))


while b:
    r = a % b
    a = b
    b = r

print("CMMDC = " + str(a))

#Fie n nr natural. Calculati suma cifrelor. Inversati numarul. Verificati palindrom

numar = int(input("Introduceti un numar: "))
copie = numar

sum2 = 0

while copie:
    sum2 += copie % 10
    copie //= 10

print("Suma cifrelor numarului " + str(numar) + " este " + str(sum2))

copie = numar
invers = 0

while copie:
    invers = invers * 10 + copie % 10
    copie //= 10

print("Inversul numarului " + str(numar) + " este " + str(invers))

if invers == numar:
    print("Numarul " + str(numar) + " este palindrom")
else:
    print("Numarul " + str(numar) + " nu este palindrom")

a = input()