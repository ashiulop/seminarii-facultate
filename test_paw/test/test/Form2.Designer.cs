﻿namespace test
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxNume = new System.Windows.Forms.TextBox();
            this.textBoxSuprafata = new System.Windows.Forms.TextBox();
            this.textBoxPret = new System.Windows.Forms.TextBox();
            this.labelNume = new System.Windows.Forms.Label();
            this.labelSuprafata = new System.Windows.Forms.Label();
            this.labelPret = new System.Windows.Forms.Label();
            this.buttonAdauga = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNume
            // 
            this.textBoxNume.Location = new System.Drawing.Point(169, 39);
            this.textBoxNume.Name = "textBoxNume";
            this.textBoxNume.Size = new System.Drawing.Size(100, 20);
            this.textBoxNume.TabIndex = 0;
            this.textBoxNume.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxNume_Validating);
            // 
            // textBoxSuprafata
            // 
            this.textBoxSuprafata.Location = new System.Drawing.Point(169, 89);
            this.textBoxSuprafata.Name = "textBoxSuprafata";
            this.textBoxSuprafata.Size = new System.Drawing.Size(100, 20);
            this.textBoxSuprafata.TabIndex = 1;
            this.textBoxSuprafata.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxSuprafata_Validating);
            // 
            // textBoxPret
            // 
            this.textBoxPret.Location = new System.Drawing.Point(169, 144);
            this.textBoxPret.Name = "textBoxPret";
            this.textBoxPret.Size = new System.Drawing.Size(100, 20);
            this.textBoxPret.TabIndex = 2;
            this.textBoxPret.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxPret_Validating);
            // 
            // labelNume
            // 
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(71, 42);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(82, 13);
            this.labelNume.TabIndex = 3;
            this.labelNume.Text = "Nume proprietar";
            // 
            // labelSuprafata
            // 
            this.labelSuprafata.AutoSize = true;
            this.labelSuprafata.Location = new System.Drawing.Point(71, 92);
            this.labelSuprafata.Name = "labelSuprafata";
            this.labelSuprafata.Size = new System.Drawing.Size(53, 13);
            this.labelSuprafata.TabIndex = 4;
            this.labelSuprafata.Text = "Suprafata";
            // 
            // labelPret
            // 
            this.labelPret.AutoSize = true;
            this.labelPret.Location = new System.Drawing.Point(71, 147);
            this.labelPret.Name = "labelPret";
            this.labelPret.Size = new System.Drawing.Size(26, 13);
            this.labelPret.TabIndex = 5;
            this.labelPret.Text = "Pret";
            // 
            // buttonAdauga
            // 
            this.buttonAdauga.Location = new System.Drawing.Point(109, 198);
            this.buttonAdauga.Name = "buttonAdauga";
            this.buttonAdauga.Size = new System.Drawing.Size(100, 23);
            this.buttonAdauga.TabIndex = 6;
            this.buttonAdauga.Text = "Adauga Teren";
            this.buttonAdauga.UseVisualStyleBackColor = true;
            this.buttonAdauga.Click += new System.EventHandler(this.buttonAdauga_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 269);
            this.Controls.Add(this.buttonAdauga);
            this.Controls.Add(this.labelPret);
            this.Controls.Add(this.labelSuprafata);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.textBoxPret);
            this.Controls.Add(this.textBoxSuprafata);
            this.Controls.Add(this.textBoxNume);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNume;
        private System.Windows.Forms.TextBox textBoxSuprafata;
        private System.Windows.Forms.TextBox textBoxPret;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.Label labelSuprafata;
        private System.Windows.Forms.Label labelPret;
        private System.Windows.Forms.Button buttonAdauga;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}