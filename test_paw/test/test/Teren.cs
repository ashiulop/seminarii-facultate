﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Teren
    {
        private string proprietar;
        private float suprafata;
        private int pret;

        public Teren()
        {
            this.proprietar = "necunoscut";
            this.suprafata = 0.0f;
            this.pret = 0;
        }

        public Teren(string proprietar, float suprafata, int pret)
        {
            this.proprietar = proprietar;
            this.suprafata = suprafata;
            this.pret = pret;
        }

        public string Proprietar
        {
            get
            {
                return this.proprietar;
            }
            set
            {
                if (value.Length > 0)
                    this.proprietar = value;
            }
        }

        public float Suprafata
        {
            get
            {
                return this.suprafata;
            }
            set
            {
                if(value > 0f)
                {
                    this.suprafata = value;
                }
            }
        }

        public int Pret
        {
            get
            {
                return this.pret;
            }
            set
            {
                if (value > 0)
                    this.pret = value;
            }
        }

        public override string ToString()
        {
            return "Nume proprietar: " + this.proprietar + ", suprafata: " + this.suprafata + "mp, pret: " + this.pret;
        }
    }
}
