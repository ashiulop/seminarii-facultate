﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form2 : Form
    {
        public Localitate local;
        public Form2(Localitate param)
        {
            InitializeComponent();
            local = param;
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            Teren tLocal = new Teren();
            tLocal.Proprietar = textBoxNume.Text;
            bool ok = true;
            try
            {
                tLocal.Suprafata = float.Parse(textBoxSuprafata.Text);
            }
            catch
            {
                MessageBox.Show("Introduceti un numar valid ca suprafata!");
                ok = false;
            }

            try
            {
                tLocal.Pret = Convert.ToInt32(textBoxPret.Text);
            }
            catch
            {
                MessageBox.Show("Introduceti un numar valid ca pret!");
                ok = false;
            }

            if (ok)
            {
                this.local = this.local + tLocal;
                MessageBox.Show("Teren adaugat cu succes!");
            }
            else
                MessageBox.Show("Eroare la incarcarea terenului!");
           

        }

        private void textBoxNume_Validating(object sender, CancelEventArgs e)
        {
            TextBox tblocal = (TextBox)sender;
            if (tblocal.Text == "")
                errorProvider1.SetError(tblocal, "Introduceti numele");
            else
                errorProvider1.SetError(tblocal, "");
        }

        private void textBoxSuprafata_Validating(object sender, CancelEventArgs e)
        {
            TextBox tblocal = (TextBox)sender;
            try
            {
                float.Parse(tblocal.Text);
                errorProvider1.SetError(tblocal, "");
            }
            catch
            {
                errorProvider1.SetError(tblocal, "Suprafata trebuie sa fie un numar!");
            }
            
        }

        private void textBoxPret_Validating(object sender, CancelEventArgs e)
        {
            TextBox tblocal = (TextBox)sender;
            if (tblocal.Text == "")
                errorProvider1.SetError(tblocal, "Introduceti pretul");
            else
                errorProvider1.SetError(tblocal, "");
        }
    }
}
