﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Localitate
    {
        private string nume;
        private List<Teren> terenuri;

        public Localitate()
        {
            this.nume = "Necunoscut";
            this.terenuri = null;
        }

        public Localitate(string nume, List<Teren> terenuri)
        {
            this.nume = nume;
            this.terenuri = new List<Teren>(terenuri);
        }

        public string Nume
        {
            get
            {
                return this.nume;
            }
            set
            {
                if (value.Length > 0)
                    this.nume = value;
            }
        }

        public List<Teren> Terenuri
        {
            get
            {
                return this.terenuri;
            }
            set
            {
                if (value != null)
                    this.terenuri = value;
            }
        }

        public static Localitate operator+(Localitate l, Teren t)
        {
            if (t != null)
                l.terenuri.Add(t);
            return l;
        }

        public override string ToString()
        {
            string rezultat = "";
            rezultat = rezultat + "Nume localitate: " + this.nume;

            if (this.terenuri != null)
            {
                rezultat = rezultat + "\nTerenuri: \n";
                foreach (Teren t in terenuri)
                    rezultat = rezultat + t.ToString() + "\n";
            }
            else
                rezultat = rezultat + " Nu exista Terenuri in localitate!";
            return rezultat;
        }
    }
}
