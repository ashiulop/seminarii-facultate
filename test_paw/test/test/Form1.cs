﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form1 : Form
    {
        Localitate local;
        public Form1(Localitate param)
        {
            InitializeComponent();
            this.local = param;

            foreach(Teren t in local.Terenuri)
            {
                treeView1.Nodes[0].Text = local.Nume;
                TreeNode tn = new TreeNode(t.ToString());
                tn.Tag = t;

                treeView1.Nodes[0].Nodes.Add(tn);
            }
        }

        private void buttonAdauga_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2(local);
            f2.ShowDialog();
            this.local = f2.local;

            if (treeView1.SelectedNode != null)
            {
                treeView1.SelectedNode.Nodes.Clear();
                foreach (Teren t in local.Terenuri)
                {
                    treeView1.SelectedNode.Text = local.Nume;
                    TreeNode tn = new TreeNode(t.ToString());
                    tn.Tag = t;

                    treeView1.SelectedNode.Nodes.Add(tn);
                }
            }
            else
            {
                treeView1.Nodes[0].Nodes.Clear();
                foreach (Teren t in local.Terenuri)
                {
                    treeView1.Nodes[0].Text = local.Nume;
                    TreeNode tn = new TreeNode(t.ToString());
                    tn.Tag = t;

                    treeView1.Nodes[0].Nodes.Add(tn);
                }
            }
        }
    }
}
