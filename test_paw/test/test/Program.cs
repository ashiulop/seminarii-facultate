﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            Teren t1 = new Teren("Popescu", 35f, 200);
            Teren t2 = new Teren("Ionescu", 20.5f, 150);
            Teren t3 = new Teren("Georgescu", 40f, 500);

            List<Teren> lista = new List<Teren>() { t1, t2, t3 };

            Localitate l1 = new Localitate("Bucuresti", lista);
            Console.Write(l1);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(l1));
        }
    }
}
