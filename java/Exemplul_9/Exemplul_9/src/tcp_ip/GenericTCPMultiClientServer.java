package tcp_ip;

import java.io.IOException;
import java.net.ServerSocket;

public class GenericTCPMultiClientServer {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Utilizare: java GenericTCPMultiClientServer <port>");
            System.exit(-1);
        }

        int port = Integer.valueOf(args[0]);

        try (ServerSocket serverSocket = new ServerSocket(port);) {
            System.out.println("Asteapta conexiuni client la portul " +
                    serverSocket.getLocalPort() + "...");

            while (true) {
                //Accepta conexiune de la un nou client intr-un thread distinct
                new ServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Conexiune esuata pe portul " + port);
            System.exit(-2);
        }
    }
}
