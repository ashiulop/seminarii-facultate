package tcp_ip;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;


public class GenericTCPClient {
    public static void main(String[] args) {
        // "localhost" este numele asociat adresei de IP 127.0.0.1
        if (args.length != 2) {
            System.err.println("Utilizare: java GenericTCPClient " +
                    "<nume server> <port>");
            System.exit(-1);
        }

        String numeServer = args[0];
        int port = Integer.valueOf(args[1]);

        try {
            System.out.println("Conectare la " + numeServer +
                    " port " + String.valueOf(port));
            Socket client = new Socket(numeServer, port);

            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            BufferedReader commandLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Scrie 'exit' pentru a inchide clientul!");

            // Repeta pana cand este furnizat mesajul 'exit'
            while (true) {
                String mesaj = commandLine.readLine();
                if (mesaj.equalsIgnoreCase("exit")) {
                    in.close();
                    out.close();
                    client.close();
                    System.exit(0);
                } else {
//                    out.writeUTF(mesaj + client.getLocalSocketAddress());
                    out.writeUTF(mesaj);
                }

                System.out.println(in.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
