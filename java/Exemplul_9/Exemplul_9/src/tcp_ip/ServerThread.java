package tcp_ip;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;


public class ServerThread extends Thread {
    private Socket client = null;
    private static int nrClienti = 0;

    public ServerThread(Socket socket) {
        super("ServerThread");
        this.client = socket;
        this.setName(this.getName() + "-" + ++nrClienti);
    }

    @Override
    public void run() {
        try (DataInputStream in = new DataInputStream(client.getInputStream());
             DataOutputStream out = new DataOutputStream(client.getOutputStream())){

            System.out.println("Conexiune acceptata de la client " +
                    client.getRemoteSocketAddress() + "; " + this.getName());

            String mesajClient = null;

            while (true) {
                mesajClient = in.readUTF();

                System.out.println(mesajClient + " (" + this.getName() + ")");
                //Aici poate urma o procesare realizata de server
                sleep(500);

                out.writeUTF("ECHO server: " + mesajClient + "(" + this.getName() + ")");
            }

        } catch (EOFException eof) {
            System.out.println("Deconectare de la client " +
                    "(" + this.getName() + ")");
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
