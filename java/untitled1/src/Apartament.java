import java.util.ArrayList;
import java.util.List;

class Camera{
    private float suprafata;
    private String denumire;

    public Camera(float suprafata, String denumire) {
        this.suprafata = suprafata;
        this.denumire = denumire;
    }

    public float getSuprafata() {
        return suprafata;
    }

    public void setSuprafata(float suprafata) {
        this.suprafata = suprafata;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    @Override
    public String toString() {
        return this.denumire + ", " + this.suprafata;
    }
}

public class Apartament {
    private String proprietar;
    private float chirie;
    private List<Camera> camere;

    public Apartament(String proprietar, float chirie, List<Camera> camere) {
        this.proprietar = proprietar;
        this.chirie = chirie;
        this.camere = camere;
    }

    public String getProprietar() {
        return proprietar;
    }

    public void setProprietar(String proprietar) {
        this.proprietar = proprietar;
    }

    public float getChirie() {
        return chirie;
    }

    public void setChirie(float chirie) {
        this.chirie = chirie;
    }

    public List<Camera> getCamere() {
        return camere;
    }

    public void setCamere(List<Camera> camere) {
        this.camere = camere;
    }

    @Override
    public String toString() {
        String value =  this.proprietar + ", " + this.chirie + ", ";
        for(Camera c : camere){
            value += c.toString() + ", ";
        }
        return value;
    }

    public static void main(String[] args) {
        Camera dormitor1 = new Camera(15, "dormitor");
        Camera dormitor2 = new Camera(12, "dormitor");
        Camera sufragerie = new Camera(17, "sufragerie");
        Camera bucatarie1 = new Camera(12, "bucatarie");
        Camera bucatarie2 = new Camera(15, "bucatarie");
        Camera hol = new Camera(20, "hol");

        Apartament a1 = new Apartament("Ionescu", 250, new ArrayList<Camera>(){{add(dormitor1); add(bucatarie1);}});
        Apartament a2 = new Apartament("Popescu", 350, new ArrayList<Camera>(){{add(dormitor1); add(dormitor2); add(bucatarie1);}});
        Apartament a3 = new Apartament("Georgescu", 1000, new ArrayList<Camera>(){{add(dormitor1); add(sufragerie); add(hol); add(bucatarie1);}});
        Apartament a4 = new Apartament("Marinescu", 750, new ArrayList<Camera>(){{add(dormitor1); add(hol); add(bucatarie1);}});

        List<Apartament> apartamente = new ArrayList<>(){{
            add(a1);
            add(a2);
            add(a3);
            add(a4);
        }};

        for (Apartament a : apartamente){
            float suprafata = 0;
            for (Camera c : a.getCamere()){
                suprafata += c.getSuprafata();
            }
            System.out.println(a.toString()+ ", suprafata: " + suprafata + "mp");
        }
    }
}
