package banking_tcp;

import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ServerThread extends Thread{
    private Socket client = null;
    private static int nrClienti = 0;
    private ArrayList<String> listaMesajeClient = new ArrayList<>();

    public ServerThread(Socket socket) {
        super("ServerThread");
        this.client = socket;
        this.setName(this.getName() + "-" + ++nrClienti);
    }

    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

    @Override
    public void run() {
        try (DataInputStream in = new DataInputStream(client.getInputStream());
             DataOutputStream out = new DataOutputStream(client.getOutputStream())){

            System.out.println("Conexiune acceptata de la client " +
                    client.getRemoteSocketAddress() + "; " + this.getName());
            listaMesajeClient.add("Conexiune acceptata de la client " +
                    client.getRemoteSocketAddress() + "; " + this.getName());

            String mesajClient = null;
            boolean clientOK = false;
            Client clientLocal = null;

            while (true) {
                mesajClient = in.readUTF();

                //System.out.println(mesajClient + " (" + this.getName() + ")");
                System.out.println("(" + LocalDateTime.now() + ")" + this.getName() + ": " + mesajClient);
                listaMesajeClient.add("(" + LocalDateTime.now() + ")" + this.getName() + ": " + mesajClient);

                if(!clientOK){
                    for(Client c : TCPMultiClientServer.listaClienti){
                        if(mesajClient.equalsIgnoreCase(c.getNume())){
                            clientLocal = c;
                            clientOK = true;
                            String info = "(" + LocalDateTime.now() + ")" + this.getName();
                            this.setName(clientLocal.getNume());
                            info += " -> " +this.getName();
                            System.out.println(info);
                        }
                    }

                    if(!clientOK)
                        out.writeUTF("Clientul nu este inregistrat! Incercati din nou...");
                    else
                        out.writeUTF("Client acceptat! Scrieti 'help' pentru lista de comenzi disponibile...");
                }
                else {
                    if (mesajClient.equalsIgnoreCase("help")) {
                        String helpMessage = "Lista de comenzi disponibila: \n";
                        helpMessage += "'sold': Afiseaza soldul clientului curent \n";
                        helpMessage += "'iban': Afiseaza Ibanul clientului curent \n";
                        helpMessage += "'extrage': Extrage o suma de bani din cont \n";
                        helpMessage += "'adauga': Adauga o suma de bani in cont \n";
                        helpMessage += "'exit': Inchide clientul \n";
                        out.writeUTF(helpMessage);
                    }
                    else if(mesajClient.equalsIgnoreCase("sold")){
                        String outMessage = "Informatii despre contul clientului " + clientLocal.getNume();
                        outMessage += "\nSold: " + clientLocal.getCont().getSold() + " lei";
                        out.writeUTF(outMessage);
                    }
                    else if(mesajClient.equalsIgnoreCase("iban")){
                        String outMessage = "Informatii despre contul clientului " + clientLocal.getNume();
                        outMessage += "\nIban: " + clientLocal.getCont().getIban();
                        out.writeUTF(outMessage);
                    }
                    else if(mesajClient.equalsIgnoreCase("extrage")){
                        out.writeUTF("Introduceti suma pe care vreti sa o extrageti: ");
                       // sleep(500);

                        while(!mesajClient.equalsIgnoreCase("back")){
                            mesajClient = in.readUTF();
                            if(isNumeric(mesajClient)){
                                int suma = Integer.parseInt(mesajClient);
                                if(suma > clientLocal.getCont().getSold())
                                    out.writeUTF("Suma introdusa este mai mare decat soldul contului dumneavoastra! " +
                                            "Incercati din nou sau scrieti 'back' pentru a va intoarce la meniul principal");
                                else{
                                    clientLocal.getCont().setSold(clientLocal.getCont().getSold() - suma);
                                    out.writeUTF("Ati extras cu succes " + suma + " lei! " +
                                            "Introduceti o noua suma de extras sau scrieti 'back' pentru a va intoarce " +
                                            "la meniul principal");
                                }
                            }
                            else if(!isNumeric(mesajClient) && !mesajClient.equalsIgnoreCase("back")){
                                out.writeUTF("Textul introdus nu este un numar valid! Incercati din nou...");
                            }
                            else if(mesajClient.equalsIgnoreCase("back"))
                                out.writeUTF("V-ati intors in meniul principal!");
                        }
                    }
                    else if(mesajClient.equalsIgnoreCase("adauga")){
                        out.writeUTF("Introduceti suma pe care vreti sa o adaugati in cont: ");
                        // sleep(500);

                        while(!mesajClient.equalsIgnoreCase("back")){
                            mesajClient = in.readUTF();
                            if(isNumeric(mesajClient)){
                                int suma = Integer.parseInt(mesajClient);
                                clientLocal.getCont().setSold(clientLocal.getCont().getSold() + suma);
                                out.writeUTF("Ati introdus cu succes " + suma + " lei in contul dumneavoastra! " +
                                        "Introduceti o noua suma de extras sau scrieti 'back' pentru a va intoarce " +
                                        "la meniul principal");
                            }
                            else if(!isNumeric(mesajClient) && !mesajClient.equalsIgnoreCase("back")){
                                out.writeUTF("Textul introdus nu este un numar valid! Incercati din nou...");
                            }
                            else if(mesajClient.equalsIgnoreCase("back"))
                                out.writeUTF("V-ati intors in meniul principal!");
                        }
                    }
                    else
                        out.writeUTF("Comanda necunoscuta! Scrieti 'help' pentru lista de comenzi disponibile...");
                    sleep(500);
                }

            }

        } catch (EOFException eof) {
            System.out.println("Deconectare de la client " +
                    "(" + this.getName() + ")");
            listaMesajeClient.add("Deconectare de la client " +
                    "(" + this.getName() + ")");
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter fw = new FileWriter("src\\fisiere\\logs.txt", true);
            for (String mesaj : listaMesajeClient){
                fw.write(mesaj + "\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
