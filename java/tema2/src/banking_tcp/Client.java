package banking_tcp;

public class Client {
    private String nume;
    private ContBancar cont;

    public Client(String nume, ContBancar cont) {
        this.nume = nume;
        this.cont = cont;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public ContBancar getCont() {
        return cont;
    }

    public void setCont(ContBancar cont) {
        this.cont = cont;
    }
}
