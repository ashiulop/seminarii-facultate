package banking_tcp;

import java.io.*;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Scanner;

public class TCPMultiClientServer {
    public static ArrayList<Client> listaClienti = new ArrayList<Client>();

    static{
        String file_path = "src\\fisiere\\clienti.txt";
        try {
            Scanner s = new Scanner(new BufferedReader(new FileReader(file_path)));
            while (s.hasNextLine()){
                String linie = s.nextLine();
                for(int i = 0; i < linie.lines().count(); i++){
                    String[] client =linie.split(" ");
                    listaClienti.add(new Client(client[0], new ContBancar(client[1], Integer.parseInt(client[2]))));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Utilizare: java GenericTCPMultiClientServer <port>");
            System.exit(-1);
        }

        int port = Integer.valueOf(args[0]);

        try (ServerSocket serverSocket = new ServerSocket(port);) {
            System.out.println("Asteapta conexiuni client la portul " +
                    serverSocket.getLocalPort() + "...");

            while (true) {
                //Accepta conexiune de la un nou client intr-un thread distinct
                new ServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Conexiune esuata pe portul " + port);
            System.exit(-2);
        }
    }
}
