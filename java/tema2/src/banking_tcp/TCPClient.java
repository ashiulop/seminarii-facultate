package banking_tcp;

import java.io.*;
import java.net.Socket;

public class TCPClient {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Utilizare: java GenericTCPClient " +
                    "<nume server> <port>");
            System.exit(-1);
        }

        String numeServer = args[0];
        int port = Integer.valueOf(args[1]);

        try {
            System.out.println("Conectare la " + numeServer +
                    " port " + String.valueOf(port));
            Socket clientSocket = new Socket(numeServer, port);

            OutputStream outToServer = clientSocket.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            InputStream inFromServer = clientSocket.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            BufferedReader commandLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti numele pe care este inregistrat contul. Scrieti 'exit' pentru a inchide clientul!");

            while (true) {
                String mesaj = commandLine.readLine();
                if (mesaj.equalsIgnoreCase("exit")) {
                    in.close();
                    out.close();
                    clientSocket.close();
                    System.exit(0);
                }
                else {
//                    out.writeUTF(mesaj + client.getLocalSocketAddress());
                    out.writeUTF(mesaj);
                }




                System.out.println(in.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
