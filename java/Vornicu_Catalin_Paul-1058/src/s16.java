public class s16{
    static volatile int contor = 0;
    private synchronized static void inc(){contor++;}

    public static void main(String[] args) throws Exception{
        class FirExecutie extends Thread{
            @Override
            public void run() {
                for(int i = 0 ; i < 10000; i++){
                    inc();
                }
            }
        }
        Thread fir1 = new FirExecutie();
        Thread fir2 = new FirExecutie();
        fir1.start();
        fir2.start();
        fir1.join();
        fir2.join();
        System.out.println(contor);

    }
}