//Clasa RezervareCinema are ca atribute titlul filmului (String), data filmului (Date) și două masive de valori întregi ce
// reprezintă locurile rezervate (un masiv pentru rând și unul pentru loc în cadrul rândului).
// Clasa implementează interfața Cloneable și o metodă add(int rand,int loc) prin care se
// adaugă un loc.
 //       1. Să se implementeze clasa RezervareCinema conform cerințelor specificate
 //       2. Sa se construiască o colecție de cel puțin 3 obiecte de tip de RezervareCinema și să se salveze
//       aceasta colecție într-un fișier text rezervari.csv, câte un obiect pe linie

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class RezervareCinema implements Cloneable{
    private String titlu;
    private Date data;
    private int[] randuri;
    private int[] locuri;
    private int k = 0;

    public RezervareCinema(String titlu, Date data, int n) {
        this.titlu = titlu;
        this.data = data;
        this.randuri = new int[n];
        this.locuri = new int[n];
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int[] getRanduri() {
        return randuri;
    }

    public void setRanduri(int[] randuri) {
        this.randuri = randuri;
    }

    public int[] getLocuri() {
        return locuri;
    }

    public void setLocuri(int[] locuri) {
        this.locuri = locuri;
    }

    public int getK() {
        return k;
    }

    public void add(int rand, int loc){
        this.randuri[k] = rand;
        this.locuri[k] = loc;
        k++;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return (RezervareCinema)super.clone();
    }
}

public class Main {
    public static void main(String[] args) {
        List<RezervareCinema> listaRezervari= new ArrayList<>(){
            {
                add(new RezervareCinema("Film1", new Date("02.06.2020"), 2));
                add(new RezervareCinema("Film2", new Date("02.06.2020"), 3));
                add(new RezervareCinema("Film3", new Date("02.06.2020"), 1));
            }
        };

        for(RezervareCinema r: listaRezervari){
            for(int i = 0; i < r.getK(); i++){
                r.add(i * r.getK(), i * r.getK() + 4);
            }
        }

        try {
            FileWriter fw = new FileWriter("rezervari.csv", false);
            PrintWriter pw = new PrintWriter(fw);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}
