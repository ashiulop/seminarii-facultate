package punct;

public class Punct2D extends Object implements Cloneable {
    private double x;
    private double y;
    private String eticheta;

    static final double X_ORIGINE = 0.0;
    static final double Y_ORIGINE = 0.0;
    static final String ETICHETA = "ORIGINE";

    public Punct2D(double x, double y, String eticheta) {
        this.x = x;
        this.y = y;
        this.eticheta = eticheta;
    }

    public Punct2D() {
        this.x = X_ORIGINE;
        this.y = Y_ORIGINE;
        this.eticheta = ETICHETA;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getEticheta() {
        return eticheta;
    }

    public void setEticheta(String eticheta) {
        this.eticheta = eticheta;
    }

    @Override
    public String toString() {
//        return "Punct2D{" +
//                "x=" + x +
//                ", y=" + y +
//                ", eticheta='" + eticheta + '\'' +
//                '}';
        return this.x + "|" + this.y + "|" + this.eticheta;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
