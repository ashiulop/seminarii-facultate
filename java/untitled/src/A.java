interface IVorbeste {
    void Spune(String text);
}

class Persoana implements IVorbeste{
    protected String nume;
    public Persoana(String n){
        this.nume = n;
    }
    public void Spune(String text){
        System.out.println("Persoana " + this.nume + " - " + text);
    }
}

class Student extends Persoana implements IVorbeste{
    private int nrMatricol;
    public Student(int nrMat, String numep){
        super(numep);
        this.nrMatricol = nrMat;
    }
    public void Spune(String text){
        System.out.println("Student " + this.nume + " - " + text);
    }
}

public class A{
    public static void main(String[] args) {
        Student s  = new Student(2, "Gigel");
        Persoana p = new Persoana("Popescu");
        IVorbeste iv = s;
        iv.Spune("Text1");
        iv = p;
        iv.Spune("Text2");
    }
}