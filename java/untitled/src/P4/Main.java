package P4;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Profesor> profesori = readFromCSV("profesori.csv");

        for(Profesor p : profesori){
            System.out.println(p.toString());
        }
    }

    private static List<Profesor> readFromCSV(String fileName) {
        List<Profesor> profesori = new ArrayList<>();
        Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        // using try with resource, Java 7 feature to close resources
        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {

            // read the first line from the text file
            String line = br.readLine();

            // loop until all lines are read
            while (line != null) {

                Profesor p = new Profesor();
                String[] linie = line.split(",");
                int id = Integer.parseInt(linie[0]);
                p.setId(id);
                p.setNume(linie[1]);
                p.setGrad(linie[2]);
                p.setDepartament(linie[3]);

                line = br.readLine();
                linie = line.split(",");

                String[] discipline = new String[linie.length/2];
                int[] ore = new int[linie.length/2];
                int nr1 = 0, nr2 = 0;
                for(int i = 0; i < linie.length; i++){

                    if(i % 2 == 0)
                        discipline[nr1++] = linie[i];
                    else
                        ore[nr2++] = Integer.parseInt(linie[i]);
                }
                p.setDiscipline(discipline);
                p.setOre(ore);

                profesori.add(p);

                line = br.readLine();
            }


        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return profesori;
    }


}
