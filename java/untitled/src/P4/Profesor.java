package P4;

import java.util.Arrays;

public class Profesor {
    private int id;
    private String nume;
    private String grad;
    private String departament;
    private String[] discipline;
    private int[] ore;

    public Profesor() {
        this.id = 0;
        this.nume = null;
        this.grad = null;
        this.departament = null;
        this.discipline = null;
        this.ore = null;
    }

    public Profesor(int id, String nume, String grad, String departament, String[] discipline, int[] ore) {
        this.id = id;
        this.nume = nume;
        this.grad = grad;
        this.departament = departament;
        this.discipline = discipline;
        this.ore = ore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getDepartament() {
        return departament;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public String[] getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String[] discipline) {
        this.discipline = discipline;
    }

    public int[] getOre() {
        return ore;
    }

    public void setOre(int[] ore) {
        this.ore = ore;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", grad='" + grad + '\'' +
                ", departament='" + departament + '\'' +
                ", discipline=" + Arrays.toString(discipline) +
                ", ore=" + Arrays.toString(ore) +
                '}';
    }
}
