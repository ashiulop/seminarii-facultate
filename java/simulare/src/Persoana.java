// Fie clasa Persoana cu argumentele specificate mai jos. 
// 1. Să se scrie o funcție statica care afișează o listă de obiecte persoana. Să se testeze din programul principal cu o listă
// de 3 obiecte persoana
// 2. Să se scrie o funcție statică salvează(String caleFisier, List<Persoana> persoana) pentru salvarea unei liste
// de persoane într-un fișier text. Să se apeleze funcția din programul principal.

import java.util.ArrayList;
import java.util.List;

class Persoana {
    private int cod;
    private String nume;

// de continuat codul


    public Persoana(int cod, String nume) {
        this.cod = cod;
        this.nume = nume;
    }

    static void afisare(ArrayList<Persoana> lista){
        for(Persoana p : lista){
            System.out.println(p.toString());
        }
    }

    @Override
    public String toString() {
        return "Nume: " + this.nume + ", Cod: " + this.cod;
    }

    public static void main(String[] args) {
        Persoana p1 = new Persoana(123, "Ion");
        Persoana p2 = new Persoana(456, "Vasile");
        Persoana p3 = new Persoana(789, "Marcel");

        ArrayList<Persoana> lista = new ArrayList<Persoana>();
        lista.add(p1);
        lista.add(p2);
        lista.add(p3);
        Persoana.afisare(lista);
    }
}