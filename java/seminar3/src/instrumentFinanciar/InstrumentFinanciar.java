package instrumentFinanciar;

import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Scanner;

public class InstrumentFinanciar {
    public static void main(String[] args){
        Instrument instr1 = new Instrument("ALR", "Alro Slatina", 0.76, Status.TRANZACTIONABIL);
        Instrument instr2 = new Instrument("TGN", "Transgaz", 270.8, Status.TRANZACTIONABIL);
        Instrument instr3 = new Instrument("BVB", "Bursa de Valori Bucuresti", 25.56, Status.NETRANZACTIONABIL);

        ArrayList<Instrument> lista = new ArrayList<Instrument>();

        lista.add(instr1);
        lista.add(instr2);
        lista.add(instr3);

        FileWriter outFile = null;
        BufferedWriter writer = null;

        try {
            outFile = new FileWriter("instrumenteFinanciare.csv", false);
            writer = new BufferedWriter(outFile);

            for (Instrument i:lista) {
                System.out.println(i.toString());
                writer.write(i.toString());
                writer.newLine();  //outFile.write("\r\n");
            }

            writer.close();
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileReader inFile = null;
        BufferedReader reader = null;

        ArrayDeque<Instrument> coada = new ArrayDeque<Instrument>();

        try {
            inFile = new FileReader("instrumenteFinanciare.csv");
            reader = new BufferedReader(inFile);

            Scanner scanFile = new Scanner(reader);

            while (scanFile.hasNext()){
                String linie = scanFile.nextLine();
                Scanner scanLinie = new Scanner(linie);
                scanLinie.useDelimiter(",");

                Instrument local = new Instrument();
                local.setSimbol(scanLinie.next());
                local.setNume(scanLinie.next());
                local.setPret(scanLinie.nextDouble());

                local.setStare(Status.valueOf(scanLinie.next()));

                coada.offerLast(local);
            }

            reader.close();
            inFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (!coada.isEmpty()){
            Instrument local = coada.pollFirst();
            System.out.println(local.toString());
        }

//        for (Instrument i:lista) {
//            System.out.println(i.toString());
//        }
    }
}
