package instrumentFinanciar;

public enum Status {
    TRANZACTIONABIL("TRANZACTIONABIL"),
    NETRANZACTIONABIL("NETRANZACTIONABIL"),
    NECUNOSCUT("NECUNOSCUT");

    String denumire;

    Status(String denumire){
        this.denumire = denumire;
    }
}
