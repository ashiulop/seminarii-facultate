package Tema;

public interface Descarcare {
    public default int DescarcaContainer(PortContainer pc, Macara m){
        int[] containere = pc.getNrContainere();

        switch (m.getTipContainer()){
            case Mic_10mc:
                if(containere[0] - 1 < 0)
                    return -1;
                containere[0] -= 1;
                pc.setNrContainere(containere);
                return containere[0];
            case Mediu_25mc:
                if(containere[1] - 1 < 0)
                    return -1;
                containere[1] -= 1;
                pc.setNrContainere(containere);
                return containere[1];
            case Mare_50mc:
                if(containere[2] - 1 < 0)
                    return -1;
                containere[2] -= 1;
                pc.setNrContainere(containere);
                return containere[2];
            case Jumbo_100mc:
                if(containere[3] - 1 < 0)
                    return -1;
                containere[3] -= 1;
                pc.setNrContainere(containere);
                return containere[3];
        }
        return -1;
    }
}
