package Tema;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        PortContainer pc1 = new PortContainer("PortContainer1", new int[]{5, 2, 5, 3});
        PortContainer pc2 = new PortContainer("PortContainer2", new int[]{7, 12, 3, 2});
        PortContainer pc3 = new PortContainer("PortContainer3", new int[]{10, 5, 9, 4});

        PortContainer[] flota = new PortContainer[]{pc1, pc2, pc3};


//        Tema.PortContainer pc4 = new Tema.PortContainer("PC4", new Tema.Containere[]{Tema.Containere.Jumbo_100mc, Tema.Containere.Mediu_25mc, Tema.Containere.Mic_10mc, Tema.Containere.Mare_50mc}, new int[]{9, 3, 2, 4});

        try {
            PortContainer pcClona = (PortContainer) pc1.clone();
            pcClona.setNrContainere(new int[]{4, 5, 1, 6});
            pcClona.setEticheta("Tema.PortContainer Clona");

            System.out.println(pc1.getEticheta());
            System.out.println(pcClona.getEticheta());


        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        FileWriter outFile = null;
        BufferedWriter writer = null;
        String SEPARATOR = ",";
        try {

            outFile = new FileWriter("flota.csv", false);
            writer = new BufferedWriter(outFile);
            for (PortContainer pc : flota) {
                StringBuffer linie = new StringBuffer();
                linie.append(pc.getEticheta());
                linie.append(SEPARATOR);
                for (Containere c : pc.getTipContainer()) {
                    linie.append(c);
                    linie.append(SEPARATOR);
                }
                for (int i : pc.getNrContainere()) {
                    linie.append(i);
                    linie.append(SEPARATOR);
                }
                linie.deleteCharAt(linie.length() - 1);
                writer.write(linie.toString());
                writer.newLine();
            }
            writer.close();
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileReader inFile = null;
        BufferedReader reader = null;

        ArrayList<PortContainer> lista = new ArrayList<PortContainer>();

        try {
            inFile = new FileReader("flota.csv");
            reader = new BufferedReader(inFile);

            Scanner scanFile = new Scanner(reader);

            while (scanFile.hasNext()) {
                String linie = scanFile.nextLine();
                Scanner scanLinie = new Scanner(linie);
                scanLinie.useDelimiter(SEPARATOR);

                PortContainer local = new PortContainer();
                local.setEticheta(scanLinie.next());
                Containere[] contLocale = new Containere[4];
                for (int i = 0; i < 4; i++) {
                    contLocale[i] = Containere.valueOf(scanLinie.next());
                }
                local.setTipContainer(contLocale);

                int[] nrContLocale = new int[4];
                for (int i = 0; i < 4; i++) {
                    nrContLocale[i] = Integer.parseInt(scanLinie.next());
                }
                local.setNrContainere(nrContLocale);

                lista.add(local);
            }
            reader.close();
            inFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (PortContainer pc : lista){
            System.out.println(pc.toString());
        }

        Macara m1 = new Macara(Containere.Mic_10mc, 1000);
        Macara m2 = new Macara(Containere.Mediu_25mc, 1500);
        Macara m3 = new Macara(Containere.Mare_50mc, 2000);
        Macara m4 = new Macara(Containere.Jumbo_100mc, 3000);

        Macara[] macarale = new Macara[]{m1, m2, m3, m4};

        Thread[] operatori = new OperatorPortContainer[4];
        for(int i = 0 ; i < 4; i++){
            operatori[i] = new OperatorPortContainer(pc1, macarale[i]);
            operatori[i].start();
        }
    }
}
