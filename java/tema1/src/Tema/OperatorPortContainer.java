package Tema;

public class OperatorPortContainer extends Thread implements Descarcare{
    private PortContainer nava;
    private Macara robot;

    public OperatorPortContainer(PortContainer nava, Macara robot) {
        this.nava = nava;
        this.robot = robot;
    }

    public synchronized void operarePortContainer(){
        int containereRamase = DescarcaContainer(this.nava, this.robot);
        if(containereRamase != -1){
            try {
                wait(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Au mai ramas " + ((containereRamase != -1)? containereRamase : 0) + " containere de tipul " + this.robot.getTipContainer() +
                " de pe nava " + this.nava.getEticheta());

        notifyAll();
    }

    @Override
    public void run() {
        while (this.nava.getCapacitate() > 0){
            try {
                sleep(this.robot.getTimpManipulare());
                operarePortContainer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
