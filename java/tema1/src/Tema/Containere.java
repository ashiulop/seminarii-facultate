package Tema;

public enum Containere {
    Mic_10mc("Mic_10mc"),
    Mediu_25mc("Mediu_25mc"),
    Mare_50mc("Mare_50mc"),
    Jumbo_100mc("Jumbo_100mc");

    String denumire;

    Containere(String denumire) {
        this.denumire = denumire;
    }
}
