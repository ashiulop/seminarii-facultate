package Tema;

public class Macara{
    private Containere tipContainer;
    private int timpManipulare;

    public Macara(Containere tipContainer, int timpManipulare) {
        this.tipContainer = tipContainer;
        this.timpManipulare = timpManipulare;
    }

    public Containere getTipContainer() {
        return tipContainer;
    }

    public void setTipContainer(Containere tipContainer) {
        this.tipContainer = tipContainer;
    }

    public int getTimpManipulare() {
        return timpManipulare;
    }

    public void setTimpManipulare(int timpManipulare) {
        this.timpManipulare = timpManipulare;
    }



}
