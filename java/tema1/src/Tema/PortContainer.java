package Tema;

public class PortContainer implements Cloneable, Numarabil{
    private String eticheta;
    private Containere[] tipContainer = {Containere.Mic_10mc, Containere.Mediu_25mc, Containere.Mare_50mc, Containere.Jumbo_100mc};
    private int[] nrContainere;

    public PortContainer() {
        this.eticheta = "Tema.PortContainer gol";
        this.nrContainere = new int[] {0, 0, 0, 0};
    }

    public PortContainer(String eticheta, Containere[] tipContainer, int[] nrContainere) {
        this.eticheta = eticheta;
        this.nrContainere = new int[4];
        int index = 0;
        for(Containere c : tipContainer){
            switch (c){
                case Mic_10mc:
                    this.nrContainere[0] = nrContainere[index];
                    break;
                case Mediu_25mc:
                    this.nrContainere[1] = nrContainere[index];
                    break;
                case Mare_50mc:
                    this.nrContainere[2] = nrContainere[index];
                    break;
                case Jumbo_100mc:
                    this.nrContainere[3] = nrContainere[index];
                    break;
            }
            index++;
        }
    }

    public PortContainer(String eticheta, int[] nrContainere) {
        this.eticheta = eticheta;
        this.nrContainere = nrContainere;
    }

    public String getEticheta() {
        return eticheta;
    }

    public void setEticheta(String eticheta) {
        this.eticheta = eticheta;
    }

    public int[] getNrContainere() {
        return nrContainere;
    }

    public void setNrContainere(int[] nrContainere) {
        this.nrContainere = nrContainere;
    }

    @Override
    public String toString() {
        return "PortContainerul " + this.eticheta + " are in componenta sa " +
                this.nrContainere[0] + " containere de tip " + this.tipContainer[0] + ", " +
                this.nrContainere[1] + " containere de tip " + this.tipContainer[1] + ", " +
                this.nrContainere[2] + " containere de tip " + this.tipContainer[2] + ", " +
                this.nrContainere[3] + " containere de tip " + this.tipContainer[3];
    }

    @Override
    public int getCapacitate() {
        return this.nrContainere[0] * 10 + this.nrContainere[1] * 25 + this.nrContainere[2] * 50 + this.nrContainere[3] * 100;
    }

    public Containere[] getTipContainer() {
        return tipContainer;
    }

    public void setTipContainer(Containere[] tipContainer) {
        this.tipContainer = tipContainer;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        PortContainer clona = (PortContainer)super.clone();
        clona.setNrContainere((int[])clona.getNrContainere().clone());
        clona.setEticheta(new String(clona.getEticheta()));
        return clona;

    }
}
